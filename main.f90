program PIC_GK
!
  use mp
  use constants, only: pi, zi
  implicit none
  !
  ! MANY VARIABLES ARE GLOBAL BUT SOME EFFORT HAS BEEN MADE TO PASS MORE EXPLICITLY
  !
  ! MAKEFILE for GS2 has proper linking and make commands for GSP.
  ! ON Franklin, make sure to module load fftw/2.1.5
  !
  ! On Dawson, one should link this program with the command
  !
  ! On MRF
  ! ifort pic.f90 -L/u/home2/nfl/FFTW/lib -lrfftw -lfftw
  !
  ! On MRF / MPI-version
  ! mpif90 -r8 -c mp_new.f90
  ! mpif90 -O3 -o pic pic.f90 mp_new.o -L/usr/local/lib -lrfftw -lfftw
  !                                            
  ! ifort -r8 -c mp.f90
  ! ifort -O3 -o pic mp.o pic_collision_pitch_new.f90 -L/u/home2/nfl/FFTW/lib -lrfftw -lfftw 
  ! On bassi
  ! xlf90_r pic_kyle_5808.f90 -o pic_kyle_5808.x -bmaxdata:0x80000000 -bmaxstack:0x80000000 -L/usr/common/usg/fftw/2.1.5/include 
  ! -L/usr/common/usg/fftw/2.1.5/lib -ldfftw -ldrfftw
  ! On jaguar
  ! ftn -O3 -o pic.x pic_collision_pitch_new.f90 mp.o  $FFTW_POST_LINK_OPTS -ldrfftw -ldfftw 
  ! h5pfc -r8 -target=linux -O3 -fastsse -c mp.f90
  ! h5pfc -r8 -target=linux -O3 -fastsse -o pic_colls.f90 mp.o $FFTW_POST_LINK_OPTS -ldrfftw -ldfftw
  !
  !--------------------constants--------------------------------------
  !  integer, parameter :: pr = 8
  !  complex, parameter :: zi = (0.0,1.0)
  !  real(kind=pr), parameter :: pi = 3.1415926535897931
  !-------------------------------------------------------------------
  !
  !-------------- --program variables---------------------------------
  integer :: ikx, iky, ikz,iz, ikvperp, step
  integer :: i, j, k, l, idx, is,mk
  !  
  real :: Volume_scale
  real :: delta_x, delta_y, delta_z
  !  
  real, dimension(:), allocatable :: x_grid, y_grid, z_grid
  !  
  real, dimension (:),   allocatable :: kx, ky, kz
  real, dimension (:,:), allocatable :: k2_perp, k2_perp_shaped
  ! 
  complex, dimension (:,:,:), allocatable :: phi
  complex, dimension (:,:,:), allocatable :: Apar
  !  
  real, dimension(:,:,:), allocatable :: p   ! particle array
  !  
  !  real :: J0_init
  real, dimension(:,:,:,:,:), allocatable :: J0, J1
  real, dimension(:,:,:), allocatable :: J0_coll
  real, dimension(:,:), allocatable:: vpar_coll, vperp_coll
  real, dimension(:), allocatable:: F0_coll
  !  
  real :: scale_x, scale_y, scale_z
  !
  ! Indices for the particle arrays
  integer, target :: x  = 1,  y = 2
  integer, target :: xm = 5, ym = 6
  integer, parameter :: z  = 3,  wgt  = 4
  integer, parameter :: zmid = 7,  wgtm = 8
  integer, parameter :: vx = 9, vy = 10, vz = 11, Ez = 12
  integer, parameter :: wd = 12  ! NOTE: wdot and Epar are different &
  integer, parameter:: vzmid = 13, En = 14
  integer:: coll_corr, nu_D, delta_nu

  !items which share the same memory because we do not need both at the same time.
  integer, pointer :: xmid, ymid
  !
  integer :: spectrum_k2_unit, spectrum_kx_unit, spectrum_ky_unit
  integer :: phi_k_unit, energy_unit, spectrum_kz_unit
  integer :: diagnostics_1_unit, diagnostics_2_unit, diagnostics_3_unit, diagnostics_4_unit
  integer :: diagnostics_traj_unit, diagnostics_vtraj_unit, diag_traj_mom_unit
  integer :: restart_unit, vspace_coll_diagnostics_unit
  integer :: diagnostics_heatflux, diagnostics_particleflux, diagnostics_weights, diagnostics_density
  integer :: diagnostics_ihflux_unit, diagnostics_ipflux_unit, diagnostics_iweights2_unit
  integer :: particles_unit, phik2_unit, j0_unit, nz_diagnostics_unit,trajgw_unit, spectrumnew_unit, conserve_unit, phiErrLog_unit, phiErrVSt_unit, wgt_unit
  integer :: particle_diagnostics_unit, z_trajectory_diagnostics_unit, phiplot_unit, apar_unit, phi2_unit, phikz_unit, aparkz_unit, phikx_unit, numerical_denom_unit
  !
  character(len=40) :: phi_2_str, rho_2_str, rho_x_str, log_phi_2_str
  character(len=40) :: spectrum_k2_str, spectrum_kx_str, spectrum_ky_str 
  character(len=40) :: phi_k_str, phi_x_str, phi_x_in_str, energy_str, spectrum_kz_str
  character(len=40) :: diagnostics_1_str, diagnostics_2_str, diagnostics_3_str, diagnostics_4_str
  character(len=40) :: diagnostics_traj_str, diagnostics_vtraj_str, diag_traj_mom_str
  character(len=40) :: vz_str, restart_str, vel_diagnostics_str
  character(len=40) :: diagnostics_hflux_str, diagnostics_pflux_str, diagnostics_weights_str, diagnostics_density_str
  character(len=40) :: diagnostics_ihflux_str, diagnostics_ipflux_str, diagnostics_iweights2_str
  character(len=40) :: particles_str, phik2_str, j0_str, nz_diagnostics_str,trajgw_str,spectrumnew_str, conserve_str, phiErrLog_str, phiErrVSt_str, wgt_str
  character(len=40) :: particle_diagnostics_str, z_trajectory_diagnostics_str, phiplot_str, apar_str, phi2_str, phikz_str, aparkz_str, phikx_str, numerical_denom_str
  character(len=1) :: strnum1
  character(len=2) :: strnum2
  character(len=3) :: strnum3
  character(len=4) :: strnum4
  !
  integer, dimension (3) :: Msize
  !  
  real :: time
  !  
  integer :: Number_Chi, Number_E, Number_Chi_total
  integer, dimension(:,:), allocatable :: Energy_int, Chi_int
  !
  character (len=10) :: zdate, ztime, zzone
  integer, dimension(8) :: ival
  real :: time_start, time_end
  !
  logical :: first_nonlinear = .TRUE.
  logical :: exit = .false.
  !
  ! for collision operator
  real, dimension(:,:), allocatable :: nu_D_grid,nu_Dei_grid,nu_s_grid,Delta_nu_grid,nu_E_grid,nu_par_grid
  real, dimension(:,:,:), allocatable :: AA_xi, CC_xi, AA_En, CC_En
  real, dimension(:,:,:), allocatable:: BB_xi, BB_En
  real, dimension(:,:,:), allocatable:: v0y0fac_lorentz, v0y0fac_en_diff
  real, dimension(:,:,:), allocatable:: v1y0fac_lorentz, v1y0fac_en_diff
  real, dimension(:,:,:), allocatable:: v2y2fac_lorentz, v2y2fac_en_diff
  real, dimension(:,:,:), allocatable:: v0_lorentz, v0_en_diff
  real, dimension(:,:,:), allocatable:: v1_lorentz, v1_en_diff
  real, dimension(:,:,:), allocatable:: v2_lorentz, v2_en_diff
  integer, dimension(:,:), allocatable:: v_int
  logical:: pitch_angle_coll_on = .false. , energy_diff_coll_on = .false., conserve_coll_sm_on=.false., conserve_coll_mc_on = .false.
  !
  ! for multiple species 
  real :: fac_velocity 
  real, dimension(:), allocatable  :: mass, temperature, density, &
       Zs, L_T, L_N, NU_coll, nprime, tprime, vts, epar_fac  ! IMPORTANT
  !
  !------------------------input parameters---------------------------
  integer :: in_unit=60
  character(len=40) :: runname
  character(len=40) :: in_str
  !
  ! number of spatial gridpoints (Nx,Ny; must be divisible by 4) and box length (Lx,Ly)
  integer       :: Nx=32, Ny=32, Nz=32
  real :: Lx=64.0, Ly=64.0, Lz=2.*pi   
  !
  ! velocity grid
  integer :: Nvz 
  integer :: NE = 8
  real :: Emax = 4.0
  !
  real, dimension(:),allocatable:: sumPhi, sumPhiGrowthRate
  integer:: NpartOut = 0, NtimeOut =0, iOut = 0, NpartTraj=0
  real, dimension(:), allocatable:: B0_grid, mu_grid, dmu, xi_grid, v_grid
  real, dimension(:), allocatable::  Egrid,PhiErr, mcNormSumMu
  integer, dimension(:), allocatable:: NE_mu,numAtZ
  real:: velz2, velperp2,E_temp,B0local,z_temp,B0min,dJ0,mu_temp,B0max,B0mag,sumNorm,eps,zmid_temp,mugradB,sgnvz,vz2,zstar,tstar,znew,dv, dxi
  !logical:: proc0 = .TRUE., spectrum_phi_new_on = .FALSE.
  !integer:: nproc = 1
  !integer:: iproc = 1
  integer:: ntimesthrough = 0
  integer:: sumi,Nmu,NJ0,Nxi,imu,B0mode,B0_unit,B0out_unit,Nxi_coll,Nv_coll,spectrum_new_write
  real, dimension(:,:), allocatable:: v0_SM, v1_SM
  real, dimension(:,:),allocatable:: mcNorm
  integer, dimension(:,:), allocatable:: Nbounce, Np_mu_z
  character(len=40) :: B0file, B0out_str
  real, dimension(:,:,:), allocatable:: phiDenom, AparDenom
  real:: alpha_ewa = 0.1, max_gamma = 0.0
  integer:: init_kx = -9999, init_ky = -9999, init_kz = -9999
  integer:: exclude_kx = -9999, exclude_ky = -9999, exclude_kz = -9999
  logical:: use_sobol = .false.
  integer:: Nparticle_traj, ispec_traj=1
  integer:: Nrun_phi_diag, part_write_step = 0
  real:: tempnum1, tempnum2,vmaxtest,analytictest
  logical:: coll_ei_on = .true.
  logical:: h_convert_im = .true.
  logical:: h_convert_mc = .true.
  complex, dimension(:,:,:,:), allocatable:: I_corr_Uperp_k, I_corr_Q_k
  integer::Ngridtest
  real,dimension(:,:), allocatable:: ftest
  logical:: wgt_diagnostics_on = .false.
  real:: vy_max, vx_max, vz_max, delta_t_max,maxtest
  real:: Tend = -1.0
  real:: CFL_fac = -1.0
  integer:: nout_phiplot = -1
  logical:: phiplot_real = .TRUE.
  real,dimension(:),allocatable:: curvdrift_max 
  real:: max_test = 0.0
  real:: delta_t_next_x, delta_t_next_y, delta_t_next, delta_t_corrected
  logical:: reject_timestep
  real:: dtrelax_fac = 1.0
  logical:: gyrodiff_on = .true.
  integer:: init_wgt_mode_r = 0, init_wgt_mode_v = 0, nrelax_req = 1, nout_wgt = 1
  integer:: imu_conserve = -1, idx_conserve, k0_method = 0,Nparam_mandatory = 14, wgt_adjust, zturn, wgt_df, nout_apar = 1, navg_spectrum = 100
  real:: init_v0 = -9999.0,init_xi0 = -9999.0,init_v_width = 0.1, T_marker = 1.0, beta = -1.0
  logical:: gaussquad_on = .false., drift_kinetic = .false.,superparticle_conserve = .false., superparticle_fail_cont = .true.,make_poisson_well_posed = .true.
  logical:: Apar_on = .false., use_actual_df = .false., numerical_denom = .false., use_mask = .false., phiplot_z_on = .false., one_k_mode = .false., no_streaming = .false.
  logical:: phi2_diagnostics_on = .false., phikz_diagnostics_on = .false., aparkz_diagnostics_on = .false., phikx_diagnostics_on = .false.
  logical:: gnrm_on = .true., apar_diagnostics_on = .false., shape_fcn_on = .false., local_slope = .false., terminate = .false.  , cfl_check = .false., numerical_denom_diagnostics_on = .false.
  real,dimension(:), allocatable:: k_shape_x, k_shape_y, k_shape_z,k_shape_x2, k_shape_y2, k_shape_z2
  
  ! number of particles 
  integer       :: Ncell=1  ! Number of particles at some vperp, vpar in original coding.
  integer       :: Npart  ! Total = Ncell * Nvperp * Nvz; number of total particles per processor
  !
  ! delta_t is the time step
  real :: delta_t = 0.01
  real :: delta_t_in = 0.01
  real :: delta_t_min = 0.01
  real :: flux_diff_tol = 0.03
  integer :: flux_tol_step = 100
  logical :: flux_tol_on = .false.
  real :: dtmodbegin = 9999999.
  real :: dtmodend = 0.
  real :: time_stop = -1.0
  real:: vy_test
  !
  ! number of time steps
  integer :: step_max = -1
  !
  ! scale factor for initial weights
  real :: init_wgt_mag = 1.
  !
  ! tau describes the ratio of T_{i} to T_{e} ... see Normalization.pdf
  real :: tau = 1.
  !
  ! charge is the number of charges carried by one ion
  integer :: charge = 1
  !
  ! number of species to be used
  integer :: num_species = 1
  !
  ! A length scale in parallel direction
  ! Ln is normalized to A and describes the density gradient scale length &
  !in the x-direction n(x) = n_{0} exp(-x/Ln)
  real :: Ln = 1.
  ! LT is normalized to A and describes the ion temperature gradient scale & 
  !length in the x-direction T(x) = T_{0} exp(-x/LT)
  real :: LT = 1.
  !
  ! Filter Phi with exp( -(kperp*a_perp)**2 - (kz*a_par)**2)
  real :: a_perp = 0., a_par = 0.
  !
  ! for trajectory diagnostics
  integer :: Ndisp          = 5000
  integer :: traj_dia_step     = 5
  integer :: num_traj       = 1
  logical :: tracers        = .TRUE.
  !
  ! flag for choosing J0 approximation
  logical :: full_J0 = .true.
  !
  ! flags managing to turn on the diagnostics and within the diagnostics certain parts
  logical :: spectrum_diagnostics_on=.FALSE.
  logical :: phi_diagnostics_on=.FALSE.
  logical :: energy_diagnostics_on=.FALSE.
  !
  logical :: diagnostics_new_on=.FALSE.
  logical :: diagnostics_1_on  =.FALSE.
  logical :: diagnostics_2_on  =.FALSE.
  logical :: diagnostics_3_on  =.FALSE.
  logical :: diagnostics_4_on  =.FALSE.
  logical :: diagnostics_traj_on =.FALSE.
  logical :: diagnostics_iflux_on = .FALSE.
  logical :: conserve_diagnostics_on = .FALSE.
  logical :: halfstep_coll_on = .TRUE.
  logical :: nz_diagnostics_on = .FALSE.
  logical :: particle_diagnostics_on = .FALSE.
  logical :: z_trajectory_diagnostics_on = .FALSE.
  logical :: spectrum_diagnostics_old = .FALSE.
  logical :: phiplot_on = .FALSE.
    
  !
  logical :: save_for_restart =.FALSE.
  logical :: init_restart     =.FALSE.
  logical :: init_randomphase = .FALSE.
  real    :: phase_amp        = 1.
  logical :: scramble_wgt     =.FALSE.
  logical :: scramble_ypos    =.FALSE.
  !
  integer :: nsave = 1000
  integer :: nwrite = 10
  integer :: nout_spectrum = 10
  !
  integer :: phi_diagnostics_time = 10
  integer :: phi_diag_mode  = 3
  integer :: phi_diag_func = 3
  !
  real :: time_growth_off =   1.e10  ! time when linear growth gets turned OFF (tprime=nprime=0.0) 
  real :: time_collision_on = 1.e10  ! time when collision gets turned ON
  real :: nonlinear_phase   = 1.e10  ! time when run becomes nonlinear and time_step is reduced
  !
  ! to turn on the update of the particle positions in x and y &
  ! if positions are updated the run is nonlinear
  logical :: nonlinear_on =.TRUE.
  !
  ! to activate the gyrokinetic equation for updating weights
  logical :: gk_on = .TRUE.
  !
  ! to include parallel nonlinearity -- need to revisit normalizations if this is true?
  logical :: ParkerLee = .FALSE.
  logical :: finishing = .false.
  !
  ! parameters for the collision operator
  integer :: N_Chi = 4 
  real :: gamma = 0.1    ! "diffusion-parameter" added 04/15/08, see eq 5.21 of IBB thesis
  logical :: collision_on = .FALSE.
  integer :: collision_time = 10 ! this is measured in timesteps
  !  real :: nu_ii = 0.05 
  !
  ! parameters for velocity_space diagnostics
  integer :: is_dia = 1
  integer :: v_dia_x = 1
  integer :: v_dia_y = 1
  integer :: v_dia_z = 1
  integer :: vspace_coll_diagnostics_time = 50
  logical :: vspace_coll_diagnostics_on = .FALSE.
  !
  ! parameters for Krook operator
  real :: nu_krook = 0.01
  logical :: krook_operator_on = .FALSE. 
  !
  ! parameters for curvature/z-pinch
  logical :: curvature_on =.FALSE. 
  real :: Rinv = 1.
  !
  ! parameters for multiple species
  real :: mass_1 = 1., mass_2, mass_3
  real :: temperature_1 = 1., temperature_2, temperature_3
  real :: density_1 = 1., density_2, density_3
  real :: charge_1 = 1., charge_2, charge_3
  real :: LT_1, LT_2, LT_3
  real :: LN_1, LN_2, LN_3
  real :: NU_coll_1 = 0.05, NU_coll_2 = 0.05, NU_coll_3 = 0.05
  !
  !-----------------Begin executable code-------------------------
  !
  ! Initialize the mpi variables
  call init_mp
  !
  if (proc0) then
     call date_and_time (zdate, ztime, zzone, ival)
     time_start = ival(5)*3600.+ival(6)*60.+ival(7)+ival(8)/1000.
     write(*,*) "Code starts running at :", time_start
  end if
  !
  call read_inputs
  !
  ! The initial timestep in set by the user
  delta_t = delta_t_in
  time = 0.0
 !
  if (proc0) call setup_IO
  call broadcast_diagnostic_files()


  call setup_restart
  !
  call allocate_round1
  call init_B0
  call init_grid_round1

  call allocate_round2
  call init_grid_round2

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Option to use the restart files in the local directory ./restartdir/
  if(init_restart) then     
     open(restart_unit,file=restart_str,status='old',form='unformatted')
     read(restart_unit) time
     read(restart_unit) delta_t
     do j=1,num_species
        do i=1,Npart
           read(restart_unit) p(x,i,j),p(y,i,j),p(z,i,j),p(wgt,i,j),p(En,i,j),p(vx,i,j),p(vy,i,j),p(vz,i,j),p(zturn,i,j), p(wgt_adjust,i,j)
        end do
     end do
     close(restart_unit)
  else
     call init_particles
     call init_velocity
     if (B0mode .NE. 0) call init_bounce
     call init_wgt
  end if

  ! Scramble not active.
  !if(scramble_ypos) then
  !   call scramble
  !end if
  !if(scramble_wgt) then
  !   call scramble
  !end if
  !
  ! this check is necessary for restarted runs
  !if (nonlinear_on .AND. time > nonlinear_phase .AND. first_nonlinear) then
  !   delta_t = delta_t * 0.2
  !   phi_diagnostics_time = phi_diagnostics_time * 5 
  !   nout_spectrum = nout_spectrum * 5 
  !   !if(pitch_angle_collision_on .AND. time > time_collision_on) then 
  !   !   collision_time = collision_time
  !   !else
  !   !   collision_time = collision_time * 5
  !   !end if
  !   vspace_coll_diagnostics_time = vspace_coll_diagnostics_time * 5 
  !   ! have to make this part better, right now fixed manually - IBB ???
  !   first_nonlinear = .FALSE.
  !end if

  if(proc0) write(*,*) "nproc = ", nproc

  call init_J0 () 
  if (conserve_coll_mc_on) call init_J1()
  !
  if (collision_on ) call init_collision()

  p(vx,:,:) = 0.0
  p(vy,:,:) = 0.0
  vx_max = 0.0
  vy_max = 0.0
  vz_max = 0.0
 
  curvdrift_max = 0.0
  if (curvature_on) then
     do is = 1,num_species
        fac_velocity = temperature(is)*Rinv/abs(Zs(is))
        idx = 0
        do imu = 1,Nmu 
           NE = NE_mu(imu)
           do j = 1,NE
              idx = idx + 1
              maxtest = fac_velocity*(2.0*p(En,idx,is)-B0min*mu_grid(imu))/B0min
              curvdrift_max(is) = -sign(1.0,Zs(is))*max(curvdrift_max(is), maxtest)
           end do
        end do
     end do
  end if

  call max_allreduce(curvdrift_max)

  ! Perpendicular CFL logic: if user does not specify CFL_fac (or specifies it negative), 
  !   code will not check CFL condition at all
  ! Otherwise, it will only check it if there is any perpendicular movement at all: 
  !   either through nonlinearity or curvature/gradB drifts
  if ((nonlinear_on .OR. curvature_on) .AND. (CFL_fac .GT. 0.0) ) cfl_check = .true.  

  delta_t_max = delta_t_in
  do is = 1,num_species
     if ( (CFL_fac .GT. 0.0) .AND. (Nz .GT. 1) ) then
        vz_max = sqrt(maxval( 2.0*(p(En,:,is) - mu_grid(1)*B0min)))*vts(is)
        call max_allreduce(vz_max)
        delta_t_max = min(delta_t_max,CFL_fac*delta_z / vz_max)
     end if
  end do 

  if (delta_t .GT. delta_t_max) then
     if (proc0) write(*,*) "delta_t rescaled due to CFL violation in z-direction"
     delta_t = delta_t_max
  end if

  call broadcast(delta_t)

  delta_t_next = delta_t

  if (proc0) write(*,*) "delta_t = ", delta_t

  ! Filter not active 
  !if (a_perp > 0. .AND. proc0) then
  !   write(*,*) 'Gaussian filter active in kperp'
  !end if

  if (phi_diagnostics_on) then
     p(x,:,:) = 0.0
     p(y,:,:) = 0.0
     p(z,:,:) = 0.0
     if (phi_diag_mode .EQ. 2) call phi_diag_repeat(Nrun_phi_diag)
  end if

  step = 0

  if (Apar_on) then
     call calculate_phiApar(p(wgt,:,:), p(x,:,:), p(y,:,:), p(z,:,:), p(En,:,:),p(vz,:,:),phi,Apar)
     if (local_slope) then
        call calculate_local_slope_EM_field(phi,Apar,p(x,:,:), p(y,:,:), p(z,:,:),p(vz,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
     else
        call calculate_EM_field(phi,Apar,p(x,:,:), p(y,:,:), p(z,:,:),p(vz,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
     end if
  else
     call calculate_phi(p(wgt,:,:), p(x,:,:), p(y,:,:), p(z,:,:), p(En,:,:),phi)
     if (local_slope) then
        call calculate_local_slope_electric_field(phi,p(x,:,:), p(y,:,:), p(z,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
     else
        call calculate_electric_field(phi,p(x,:,:), p(y,:,:), p(z,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
     end if
  end if


  if (proc0) write(*,*) "time before evolve loop = ", time

  !
  !---------------This is the time advancement loop-------------------
  !
  if (proc0) write(*,*) " total number of particles per processor = ", Npart
  if (proc0) write(*,*) " total number of particles = ", Npart*nproc

  do while (.NOT.terminate)
     step = step + 1
     time = time + delta_t

     call manage_diagnostics()

     reject_timestep = .true.
     do while (reject_timestep) 
        reject_timestep = .false.

       if (proc0)   write(*,*) "step = ",step, " delta_t = ", delta_t, " time = ", time, " vxmax = ", vx_max, " vymax = ", vy_max , sum(cabs(phi(:,:,:))**2)
         
        ! Update particle positions
        do k=1,num_species
           fac_velocity = temperature(k)*Rinv/Zs(k)
           idx = 0
           do i=1,Nmu
              NE = NE_mu(i)
              mu_temp = mu_grid(i)
              do j=1,NE
                 idx = idx+1
                 E_temp = p(En,idx,k)
                 z_temp = p(z,idx,k)
                 znew = modulo(z_temp + p(vz,idx,k) * 0.5 * vts(k)*delta_t , Lz) 
                 if (no_streaming) znew=z_temp
                 vz2 = 2.0*(E_temp - B0(znew)*mu_grid(i))
                 sgnvz = sign(1.0,p(vz,idx,k))

                 ! Check if the particle is predicted to end up beyond its turning point
                 if ( ( vz2 .LE. 0.0 ) .AND. (idx .NE. idx_conserve) ) then
                       zstar = 0.5*Lz + sign(p(zturn,idx,k),z_temp-0.5*Lz)
                    mugradB = mu_grid(i)*gradB0(zstar)
   
                    ! Find time until turning point is reached
                    if ( - 2.0*(z_temp - zstar)/ mugradB   .LE. epsilon(0.0)) then
                        tstar = 0.0
                    else 
                       tstar = sqrt( - 2.0*(z_temp - zstar)/  mugradB  ) / vts(k)
                    end if
                    
                    ! Find new z based on t*
                    znew = modulo( z_temp - 0.5*vts(k)**2*mugradB*(0.5*delta_t - tstar)**2 ,Lz) 
                    vz2 = 2.0*(E_temp - B0(znew)*mu_grid(i))

                    ! Turn the particle around
                    sgnvz = -sign(1.0,z_temp-0.5e0*Lz)
                 end if
   
                 p(zmid,idx,k) = znew
                 p(vzmid,idx,k) = sgnvz*sqrt(vz2)
   
                 if (curvature_on) then
                    p(ymid,idx,k) = modulo(p(y,idx,k) - fac_velocity*(2.0*E_temp-B0(z_temp)*mu_grid(i)) * 0.5 * delta_t/B0(z_temp), Ly)
                 else
                    p(ym,idx,k) = p(y,idx,k)  ! now done with pointers
                 end if
 
              end do
           end do
        end do
        !
        if (nonlinear_on) p(ymid,:,:) = modulo(p(ymid,:,:) &
          + p(vy,:,:) * 0.5 * delta_t, Ly) ! FUNNY CODING IS CORRECT - IBB ???
        ! I'll assume he means ymid instead of y.  KBG 08/11/09
        if (nonlinear_on) p(xmid,:,:) = modulo(p(x,:,:)    + p(vx,:,:) * 0.5 * delta_t, Lx) 
        !
        !  Find weights at mid-point
        call update_weights(p(wgt,:,:),p(wd,:,:), p(vz,:,:), p(z,:,:), p(vx,:,:),p(En,:,:), p(Ez,:,:))
        if (conserve_coll_mc_on .AND. collision_on) then
           call calculate_coll_corr(p(x,:,:),p(y,:,:),p(z,:,:),p(vz,:,:),p(En,:,:),p(nu_D,:,:),p(delta_nu,:,:),p(wgt,:,:),p(coll_corr,:,:))
           p(wd,:,:) = p(wd,:,:) + p(coll_corr,:,:)
        end if
        do k=1,num_species
           do i=1,Npart ! Quick GPU Kernel
              p(wgtm,i,k) = p(wgt,i,k) + p(wd,i,k) * 0.5 * delta_t 
           end do
        end do
        !
        if (collision_on .AND. mod(step,collision_time)==0 .AND. halfstep_coll_on)then
           if (conserve_coll_sm_on) call calculate_phi(p(wgtm,:,:), p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(En,:,:), phi)   
           call collision(0.5*delta_t, p(wgtm,:,:), p(xmid,:,:), p(ymid,:,:), p(zmid,:,:),p(En,:,:),p(vzmid,:,:))
        end if
        !
        if (krook_operator_on) then
           if (proc0 .AND. step == 1) write(*,*) "krooking"
           call krook_operator(p(wgtm,:,:))
        end if
        !
        !--Then take a full time step (moving particles and updating weights)--
        !
           !  Find fields at mid-point
       if (Apar_on) then
          call calculate_phiApar(p(wgtm,:,:), p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(En,:,:),p(vzmid,:,:),phi,Apar)
          if (local_slope) then
             call calculate_local_slope_EM_field(phi,Apar,p(xmid,:,:), p(ymid,:,:), p(zmid,:,:),p(vzmid,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
          else
             call calculate_EM_field(phi,Apar,p(xmid,:,:), p(ymid,:,:), p(zmid,:,:),p(vzmid,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
          end if
       else
          call calculate_phi(p(wgtm,:,:), p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(En,:,:), phi)   
          if (local_slope) then
             call calculate_local_slope_electric_field(phi, p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
          else
             call calculate_electric_field(phi, p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
          end if
       end if


        if ( cfl_check) then 
           vx_max = 0.0
           vy_max = 0.0
           delta_t_corrected = delta_t
           do is = 1,num_species
              fac_velocity = temperature(is)/Zs(is)*Rinv
 
              vx_max = max(maxval(abs(p(vx,:,is))),vx_max) 
              ! Coding is a little convoluted, but makes sense:
              !   First the maximum between (vEy) and (vEy + vcurv) is found, 
              !   then is compared with current vy_max to return the new maximum
              vy_max = max( max( maxval(abs(p(vy,:,is) + curvdrift_max(is))),maxval(abs(p(vy,:,is)))) ,vy_max) 

              call max_allreduce(vx_max)
              call max_allreduce(vy_max)
           end do
  
           !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
           ! If CFL is violated, reject the timestep

           if ( (vx_max - (delta_x/delta_t)) .GT. epsilon(0.0) .AND. (Nx .GT. 1)) then    
              write(*,*) "Rejecting timstep due to CFL violation in x-direction"
              delta_t_corrected = CFL_fac*delta_x / vx_max
              reject_timestep = .true.
           end if
 
           if ( (vy_max - (delta_y/delta_t)) .GT. epsilon(0.0) .AND. (Ny .GT. 1)) then  
              write(*,*) "Rejecting timstep due to CFL violation in y-direction"
              delta_t_corrected = min(CFL_fac*delta_y / vy_max,delta_t_corrected) 
              reject_timestep = .true.
           end if

           ! See if we can relax the timestep next loop
           if (reject_timestep) then
              delta_t = delta_t_corrected
              call broadcast(delta_t)
           else
              ! Relax based on x direction
              delta_t_next_x = dtrelax_fac*CFL_fac*delta_x/vx_max
              ! Make sure increased timestep doesn't violate other CFL conditions currently
              delta_t_next_x = min(delta_t_next_x, CFL_fac*delta_y/vy_max)
              delta_t_next_x = min(delta_t_next_x, delta_t_max)
  
              ! Relax based on x direction
              delta_t_next_y = dtrelax_fac*CFL_fac*delta_y/vy_max
              ! Make sure increased timestep doesn't violate other CFL conditions
              delta_t_next_y = min(delta_t_next_y, CFL_fac*delta_x/vx_max)
              delta_t_next_y = min(delta_t_next_y, delta_t_max)

              ! Now that we have two improved timesteps, neither of which violate any CFL condition, use the largest

              delta_t_next = min(delta_t_next,max(delta_t_next_x,delta_t_next_y))

              if ((delta_t_next .GT. delta_t) .AND. proc0) write(*,*) "delta_t can be relaxed at this timestep."

              ! This can go wrong if there is a time-by-time fight between CFL and relaxation condition
              ! That is, if after relaxing, CFL demands a readjustment, and this happens repeatedly, doubling the number of timesteps
              ! User should specify nrelax_req: the number of consecutive steps a dt relaxation is requested, then use the minimum
              ! The check for this is mod(step,nrelax_req) = 0 below
              ! Generally, the poor performance dtrelax_fac equal or close to 1.0 can be offset by a larger nrelax_req and vice versa
           end if
        end if
 
     end do ! End rejection loop
     !
     do k=1,num_species
        fac_velocity = temperature(k)/Zs(k)*Rinv
        idx = 0
        do i=1,Nmu
           NE = NE_mu(i)
           mu_temp = mu_grid(i)
           do j=1,NE
              idx = idx+1
              E_temp = p(En,idx,k)
              z_temp = p(z,idx,k)
              zmid_temp = p(zmid,idx,k)
              znew = modulo(z_temp + p(vzmid,idx,k) *vts(k)* delta_t , Lz) 
              if (no_streaming) znew=z_temp
              vz2 = 2.0 * (E_temp - B0(znew)*mu_grid(i) )
              sgnvz = sign(1.0,p(vzmid,idx,k))

              ! Check if the particle is predicted to end up at or beyond its turning point
              if ( ( vz2 .LE. 0.0 ) .AND. (idx .NE. idx_conserve) ) then
                 zstar = 0.5e0*Lz + sign(p(zturn,idx,k),z_temp-0.5e0*Lz)
                 mugradB = mu_grid(i)*gradB0(zstar)

                 if (mugradB .EQ. 0.e0) write(*,*) "mugradB = 0!" 

                 ! Find time until turning point is reached
                 if (- 2.0*(z_temp - zstar)/ mugradB .LE. epsilon(0.0)) then
                    tstar = 0.0
                 else 
                    tstar = sqrt( - 2.0*(z_temp - zstar)/ mugradB) / vts(k)
                 end if

                 ! Find new z based on t*
                 znew = modulo( z_temp - 0.5*vts(k)**2*mugradB*(delta_t - tstar)**2 ,Lz) 
                 vz2 = 2.0* (E_temp - B0(znew)*mu_grid(i))

                 ! Turn the particle around
                 sgnvz = -sign(1.0,zstar-0.5e0*Lz)
              end if

              p(z,idx,k) = znew
              p(vz,idx,k) = sgnvz*sqrt(vz2)
              if (curvature_on) then
                 p(y,idx,k) = modulo(p(y,idx,k) - &
                   (fac_velocity*(2.0*E_temp-B0(zmid_temp)*mu_grid(i))) * delta_t/B0(zmid_temp), Ly)
              end if

           end do
        end do
     end do
       !
     if (nonlinear_on) then 
        p(y,:,:) = modulo(p(y,:,:) + p(vy,:,:) * delta_t, Ly) 
        p(x,:,:) = modulo(p(x,:,:) + p(vx,:,:) * delta_t, Lx) 
     end if
        !
     call update_weights(p(wgtm,:,:), p(wd,:,:),p(vzmid,:,:),p(zmid,:,:), p(vx,:,:),p(En,:,:), p(Ez,:,:))
     if (collision_on .AND. conserve_coll_mc_on) then
        call calculate_coll_corr(p(x,:,:),p(y,:,:),p(z,:,:),p(vz,:,:),p(En,:,:),p(nu_D,:,:),p(delta_nu,:,:),p(wgt,:,:),p(coll_corr,:,:))
        p(wd,:,:) = p(wd,:,:) + p(coll_corr,:,:)
     end if
     do k=1,num_species
        do i=1,Npart ! Quick GPU kernel
           p(wgt,i,k) = p(wgt,i,k) + p(wd,i,k) * delta_t  
        end do
     end do
     !
     if (collision_on .AND. mod(step,collision_time)==0) then
        if (conserve_coll_sm_on) call calculate_phi(p(wgtm,:,:), p(xmid,:,:), p(ymid,:,:), p(zmid,:,:), p(En,:,:), phi)   
        call collision(delta_t, p(wgt,:,:), p(x,:,:), p(y,:,:), p(z,:,:),p(En,:,:), p(vz,:,:))
     end if
     !
     if (krook_operator_on) then
        call krook_operator(p(wgt,:,:))
     end if

     !
     if (save_for_restart .and. mod(step,nsave)==0) call restart_save
     !

     ! Check termination conditions:
     ! User stops:
     if (mod(step, 5) == 0) call checkstop(terminate)
     ! Or time/step based condition is met: 
     if ( (step .GE. step_max) .OR. (time .GE. time_stop)) terminate = .true.


     if ( (mod(step,nrelax_req) .EQ. 0) .AND. (delta_t_next .GT. delta_t)) then    
        delta_t = delta_t_next
        delta_t_next = delta_t_max
     end if
  
     call broadcast(delta_t)

     if (Apar_on) then
        call calculate_phiApar(p(wgt,:,:), p(x,:,:), p(y,:,:), p(z,:,:), p(En,:,:),p(vz,:,:),phi,Apar)
        if (local_slope) then
           call calculate_local_slope_EM_field(phi,Apar,p(x,:,:), p(y,:,:), p(z,:,:),p(vz,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
        else
           call calculate_EM_field(phi,Apar,p(x,:,:), p(y,:,:), p(z,:,:),p(vz,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
        end if
     else
        call calculate_phi(p(wgt,:,:), p(x,:,:), p(y,:,:), p(z,:,:), p(En,:,:),phi)
        if (local_slope) then
           call calculate_local_slope_electric_field(phi,p(x,:,:), p(y,:,:), p(z,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
        else
           call calculate_electric_field(phi,p(x,:,:), p(y,:,:), p(z,:,:), p(vx,:,:), p(vy,:,:), p(Ez,:,:))
        end if
     end if

  end do
  !
  !----------------------END time advance loop------------------------
  !
  finishing = .true.
  !
  if (proc0) then
     
     if(diagnostics_new_on) then
        if(diagnostics_1_on) close(diagnostics_1_unit)
        if(diagnostics_2_on) close(diagnostics_2_unit)
        if(diagnostics_3_on) close(diagnostics_3_unit)
        if(diagnostics_4_on) then
           close(diagnostics_4_unit)
           close(diagnostics_particleflux)
           close(diagnostics_heatflux)
           close(diagnostics_weights)
           close(diagnostics_density)
        end if
     end if
     !
     if (diagnostics_traj_on) then
        close(diagnostics_traj_unit)
        close(diagnostics_vtraj_unit)
        close(diag_traj_mom_unit)
     end if
     !
     if (diagnostics_iflux_on) then
        close(diagnostics_ihflux_unit)
        close(diagnostics_ipflux_unit)
        close(diagnostics_iweights2_unit)
     end if
     !     
     if (spectrum_diagnostics_on) then
        close(spectrum_k2_unit)
        close(spectrum_kx_unit)
        close(spectrum_ky_unit)
        close(spectrum_kz_unit)
     end if
     !
     if (vspace_coll_diagnostics_on) then
        close(vspace_coll_diagnostics_unit)
     end if
     !    
     if (phi_diagnostics_on) then
        if (phi_diag_mode .EQ. 3) close(phiErrVSt_unit)
     end if

     if (nz_diagnostics_on) then
        close(nz_diagnostics_unit)
     end if
 
     if (conserve_diagnostics_on) then
!        close(conserve_unit)
     endif

     if (z_trajectory_diagnostics_on) then
        close(z_trajectory_diagnostics_unit)
     end if
  end if
  !
!  if (save_for_restart) call restart_save
  !
  if (proc0) then
     call date_and_time (zdate, ztime, zzone, ival)
     time_end = ival(5)*3600.+ival(6)*60.+ival(7)+ival(8)/1000.
     write(*,*) "Code stops running at :", time_end
     write(*,*) "Total run time :", time_end-time_start
     deallocate(mass,temperature)
     deallocate(density,Zs,L_T,L_N,NU_coll,vts,epar_fac,nprime,Tprime)
  end if
  !
  call finish_mp
  !
contains
  !-------------------------------------------------------------------
  subroutine restart_save
    !
    ! Writing the restart data for each particle.
    !
    use mp, only: iproc,nproc,proc0
    implicit none

       open(restart_unit, file=restart_str,status='replace',form='unformatted') 
       write(restart_unit) time
       write(restart_unit) delta_t

       do j=1,num_species
          do i=1,Npart
             write(restart_unit) p(x,i,j), p(y,i,j), p(z,i,j), p(wgt,i,j),p(En,i,j),p(vx,i,j),p(vy,i,j), p(vz,i,j), p(zturn,i,j), p(wgt_adjust,i,j)
          end do
       end do

    close(restart_unit)
    !
  end subroutine restart_save
  !-------------------------------------------------------------------
  subroutine checkstop(exit)
    !
    ! Exit if a *.stop file is present in the running directory
    !
    use mp, only: proc0, broadcast

    implicit none
    logical, intent (in out) :: exit
    !
    ! If .stop file has appeared, set exit flag
    if (proc0) then
       open (unit=88, file=trim(runname)//".stop", status="old", err=100)
       exit = .true.
       close (unit=88)
100    continue
    end if
    !
 !   call broadcast (exit)
    !
  end subroutine checkstop
  !-------------------------------------------------------------------
  subroutine diagnostics_new(phi_in, phi_old, delta_t, delta_t_in, delta_t_min, flux_tol_on, flux_diff_tol, flux_tol_step, step, finishing)
    !
    ! Controls dia_3, dia_4, flux diagnostics, weights and density diagnostics and phi 
    ! diagnostics.  Also controls the time step according to ambipolarity.
    !
   use mp, only: sum_reduce, nproc
    !
    implicit none
    logical, save :: first_iflux = .true.
    integer :: i, j, k, idx
    integer :: ntot
    !
    real, intent(inout) :: delta_t
    integer, intent(in) :: step, flux_tol_step
    real, intent(in) :: delta_t_in, delta_t_min, flux_diff_tol
    logical, intent(in) :: flux_tol_on
    complex, dimension(0:,0:,0:), intent(in) :: phi_in
    logical, optional :: finishing
    complex, dimension(0:,0:,0:), intent(inout) :: phi_old
    !
    logical :: writemore
    !
    complex  :: omega
    real :: kx_max, ky_max, kz_max, phi2_max
    real :: v_perp!, vperp_weight
    complex, dimension(:,:,:), allocatable :: phi_new
    !
    real :: E_phi ! see definition Denton Kotschenreuther (1.19) f.f.
    !    real :: sum_weights, heat_flux, sum_density
    real, dimension(num_species) :: particle_flux, norm_v, heat_flux, sum_weights, sum_density
    real, dimension(num_species) :: iparticle_flux, iheat_flux, isum_weights
    real :: flux_species_diff = 0.
    allocate(phi_new(0:Nx/2,0:Ny-1,0:Nz-1))
    !   
    if (proc0) E_phi = 0.5*SUM(phi_in*Conjg(phi_in))
    !  
    writemore = .false.
    if (present(finishing)) writemore = finishing
    !
100 format(6(1x,es12.4))
    !
    if (proc0) then
       phi2_max = 1.e-25
       kx_max = 0.
       ky_max = 0.
       kz_max = 0.
       omega = 0.
       !      
       do ikz=0,Nz/2
          do iky=0,Ny/2
             do ikx=0,Nx/2
                if (REAL(phi_in(ikx,iky,ikz)*Conjg(phi_in(ikx,iky,ikz))) > phi2_max) then
                   omega = (zi/(nwrite*delta_t))*log(phi_in(ikx,iky,ikz)/phi_old(ikx,iky,ikz))
                   !phi2_max = phi_1(ikx,iky,ikz)*Conjg(phi_in(ikx,iky,ikz))
                   kx_max = ikx
                   ky_max = iky
                   kz_max = ikz
                end if
             end do
          end do
       end do
       !
       if(diagnostics_3_on) then
          write(diagnostics_3_unit,100) real(omega), aimag(omega), kx_max, ky_max, kz_max
          close(diagnostics_3_unit)
          open(unit=diagnostics_3_unit, file=diagnostics_3_str, status='old', access='append')
       end if
       phi_old        = phi_in
    end if
    !
    !    if (num_species < 2) then 
    !       idx = 0
    !       particle_flux = 0.
    !       heat_flux = 0.
    !       sum_weights = 0.
    !       sum_density = 0.
    !       do i = 1,NE
    !          Nvz = Nxi_NE(i)
    !          do j = 1,Nvz
    !             idx = idx + 1
    !             particle_flux = particle_flux + 
    !             heat_flux = heat_flux + (vperp(i)**2 + p(vz,idx,1)**2)*p(wgt,idx,1)*p(vx,idx,1) ! IMPORTANT 
    !             sum_weights = sum_weights + p(wgt,idx,1)**2
    !             sum_density = sum_density + p(wgt,idx,1)
    !          end do
    !       end do
    !    else
    do k=1,num_species
       idx = 0
       particle_flux(k) = 0.
       norm_v(k) = 0.
       heat_flux(k) = 0.
       sum_weights(k) = 0.
       sum_density(k) = 0.
       iparticle_flux(k) = 0.
       iheat_flux(k) = 0.
       isum_weights(k) = 0.
       do i = 1,Nmu
       
          Nvz = 1  ! <- This is fictional
          !Nvz = Nxi_NE(i)
          do j = 1,Nvz
             idx = idx + 1
             v_perp = sqrt(2.0*mu_grid(i)*B0( p(z,idx,k) ) )
             particle_flux(k) = particle_flux(k) + p(wgt,idx,k)*p(vx,idx,k)*v_perp*exp(-p(En,idx,k))
             norm_v(k) = norm_v(k) + v_perp*exp(-p(En,idx,k))
             heat_flux(k) = heat_flux(k) + p(En,idx,k)*p(wgt,idx,k)*p(vx,idx,k)*v_perp*exp(-p(En,idx,k)) ! IMPORTANT 
             sum_weights(k) = sum_weights(k) + p(wgt,idx,k)**2*v_perp*exp(-p(En,idx,k))
             sum_density(k) = sum_density(k) + p(wgt,idx,k)*v_perp*exp(-p(En,idx,k))
          end do
       end do
    end do
    !
    if (diagnostics_iflux_on) then
       if (first_iflux) then
          open(unit=diagnostics_ihflux_unit, file=diagnostics_ihflux_str, status='replace')            
          open(unit=diagnostics_ipflux_unit, file=diagnostics_ipflux_str, status='replace')           
          open(unit=diagnostics_iweights2_unit, file=diagnostics_iweights2_str, status='replace')
          first_iflux = .FALSE.
       end if
       do k=1,num_species
          iheat_flux(k)     = 0.5*heat_flux(k)/norm_v(k)
          iparticle_flux(k) = particle_flux(k)/norm_v(k)
          isum_weights(k)   = sum_weights(k)/norm_v(k)
          !sum_density(k)   = sum_density(k)/norm_v(k)
       end do
       write(diagnostics_ihflux_unit,100) time, iheat_flux
       close(diagnostics_ihflux_unit)
       open(unit=diagnostics_ihflux_unit, file=diagnostics_ihflux_str, status='old', access='append')
       write(diagnostics_iweights2_unit,100) time, isum_weights
       close(diagnostics_iweights2_unit)
       open(unit=diagnostics_iweights2_unit, file=diagnostics_iweights2_str, status='old', access='append')
       write(diagnostics_ipflux_unit,100) time, iparticle_flux
       close(diagnostics_ipflux_unit)
       open(unit=diagnostics_ipflux_unit, file=diagnostics_ipflux_str, status='old', access='append')  
    end if
    !
!    call sum_reduce (particle_flux, 0)
!    call sum_reduce (norm_v, 0)
!    call sum_reduce (heat_flux, 0)
!    call sum_reduce (sum_weights, 0)
!    call sum_reduce (sum_density, 0)
    !
    ntot = Npart * nproc
    !
    if (proc0) then
       do k=1,num_species
          heat_flux(k)     = 0.5*heat_flux(k)/norm_v(k)
          particle_flux(k) = particle_flux(k)/norm_v(k)
          sum_weights(k)   = sum_weights(k)/norm_v(k)
          sum_density(k)   = sum_density(k)/norm_v(k)
       end do
       !
       if (num_species == 2) then
          write(*,*) "flux_diff_tol = ", flux_diff_tol
          flux_species_diff = (abs(particle_flux(1)-particle_flux(2))+1.)/1.-1.
          write(*,*) "flux_species_diff = ", flux_species_diff
          if(flux_tol_on .AND. mod(step,flux_tol_step)==0) then
             if (flux_species_diff > flux_diff_tol) then
                if (delta_t > delta_t_min) then
                   delta_t = delta_t*0.5
                   write(*,*) "time step halved"
                else
                   delta_t = delta_t
                   write(*,*) "delta_t has reached or become smaller than delta_t_min" 
                end if
             else
                if (delta_t < delta_t_in) then
                   delta_t = delta_t*2.
                   write(*,*) "time step doubled"
                else
                   delta_t = delta_t_in
                   write(*,*) "time step returned to delta_t_in value"
                end if
             end if
          end if
       end if
       !       
       if (num_species < 2) then
          if(diagnostics_4_on) then
             write(diagnostics_4_unit,100) time, sum_weights, heat_flux, E_phi, sum_density
          end if
       else
          if(diagnostics_4_on) then
             write(diagnostics_4_unit,100) time, sum_weights(1), heat_flux(1), E_phi, sum_density(1)
             close(diagnostics_4_unit)
             open(unit=diagnostics_4_unit, file=diagnostics_4_str, status='old', access='append')
             write(diagnostics_heatflux,100) time, heat_flux
             close(diagnostics_heatflux)
             open(unit=diagnostics_heatflux, file=diagnostics_hflux_str, status='old', access='append')
             write(diagnostics_particleflux,100) time, particle_flux
             close(diagnostics_particleflux)
             open(unit=diagnostics_particleflux, file=diagnostics_pflux_str, status='old', access='append')
             write(diagnostics_weights,100) time, sum_weights
             close(diagnostics_weights)
             open(unit=diagnostics_weights, file=diagnostics_weights_str, status='old', access='append')
             write(diagnostics_density,100) time, sum_density
             close(diagnostics_density)
             open(unit=diagnostics_density, file=diagnostics_density_str, status='old', access='append')
          end if
       end if
    end if


  end subroutine diagnostics_new
  !-------------------------------------------------------------------
  subroutine calculate_phi(weight, x_gc, y_gc, z_gc, En_gc,phi)
    use fft_work, only: fftw_forward
    implicit none
    ! Interpolate phi from markers onto the grid, transform to k-space, solve
    ! gyrokinetic Poisson equation.  See I. Broemstrup thesis eq 2.33.
    !
    !
    real, dimension(1:,1:),intent(inout) :: weight
    real, dimension(1:,1:), intent (in) :: x_gc
    real, dimension(1:,1:), intent (in) :: y_gc
    real, dimension(1:,1:), intent (in) :: z_gc
    real, dimension(1:,1:), intent (in) :: En_gc
    complex, dimension(0:,0:,0:):: phi
    !    
!    complex, dimension(:,:,:), allocatable :: phi_z
    real :: g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13, g14, g15, g16
    real, dimension(8) :: gg
    real, dimension(:,:,:), allocatable :: grid, gnrm
    real, dimension(:),allocatable:: F0_num
    complex, dimension(:,:), allocatable :: rho_k
    complex, dimension(:,:), allocatable :: filter    !
    complex :: J0_rho_k, testfac
    !complex, dimension(:,:,:,:), allocatable :: J0_rho_k  
    !
    real  :: fac1, fac2, dfac, test, ErrLow, phi_denom_analytic
    real  :: xgc, ygc, zgc, xl0, xl1, ym0, ym1, zn0, zn1, mgc, mk0,mk1,mk2
    integer :: idx, L0, L1, L2, M0, M1, M2, N0, N1, N2, is,mk
    integer :: ivperp, ivz, icell, J0idx
    integer, dimension(1) :: ig
    integer*8, save :: plan_real2compl_1d
    integer*8, save :: plan_real2compl_2d, plan_inv
    logical, save :: beginning=.true.
    integer, save :: phi_subroutine_count = 0
    logical, save :: first_denom_diag = .true.
    ! 
    complex :: numerator
    real:: J0val,Enorm,halfvz2,vperp,vperpweight,mtest,m_temp,analytic
    real:: B0inv,argNorm,k2,J0arg,J0val1,J0val2,normfac,jacNorm,maxanalytic, Gamma_0
    real:: erf
    !
    allocate (grid(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (gnrm(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (rho_k(0:Nx/2,0:Ny-1))  
    if (numerical_denom) then
       allocate (F0_num(0:Nz-1))  
    end if

    phi = 0.0
    !
    phi_subroutine_count = phi_subroutine_count + 1
    !
    if(beginning) then
       !
       call rfftwnd_f77_create_plan (plan_real2compl_2d,2,Msize(1:2),fftw_forward,0) 

       phiDenom = 0.0

       ! Note that the drift kinetic option is not any more efficient than the default GK mode
       ! If serious drift kinetic work is desired, the code would need a little redesign or more extensive options to gain an efficiency
       if (drift_kinetic) then
          J0 = 1.0
       end if
      
       do ikx = 0,Nx/2
          do iky = 0,Ny-1
             do iz = 0,Nz-1
                if (num_species .EQ. 1) then
                   if (drift_kinetic) then
                      Gamma_0 = 1.0 - k2_perp_shaped(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2
                   else
                      call gamma_n(k2_perp_shaped(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2, Gamma_0)
!                      call gamma0_function(k2_perp(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2, Gamma_0)
                   end if
                   phiDenom(ikx,iky,iz) = (tau + Zs(1)*density(1)*(1.0 - Gamma_0))
                else 
                   do is=1,num_species
                      if (drift_kinetic) then
                         phiDenom(ikx,iky,iz) = k2_perp_shaped(ikx,iky)
                      else
                         call gamma_n(k2_perp_shaped(ikx,iky)*temperature(is)*mass(is)/(Zs(is)*B0_grid(iz))**2, Gamma_0)
                         !call gamma0_function(k2_perp(ikx,iky)*temperature(is)*mass(is)/(Zs(is)*B0_grid(iz))**2, Gamma_0)
                         phiDenom(ikx,iky,iz) = phiDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/temperature(is))*(1.0-Gamma_0)
                      end if
                   end do
                end if
             end do
          end do
       end do
    end if

    ! Assign artificial values for the weights if running diagnostic
    if (phi_diagnostics_on ) then
       if (phi_diag_func .EQ. 1) weight(:,:) = 1.0
       if (phi_diag_func .EQ. 2) weight(:,:) = En_gc(:,:)
       if (phi_diag_func .EQ. 3) weight(:,:) = sin(2.0*pi*abs(p(vz,:,:)))
    end if
 
    ! If numerical denominator is chosen, delete this, will be filled in later
    if (numerical_denom) phiDenom = 0.e0

    ! For each vperp value, interpolate the charge to the x,y,z grid.  Integrates over v_parallel.
    do is=1,num_species
       idx = 0
       do i = 1,Nmu
          gnrm = 0.e0
          grid = 0.e0
          if (numerical_denom) F0_num(0:Nz-1) = 0.e0
          NE = NE_mu(i)

          ! For each discrete mu, deposit the particles onto a 3D grid,
          ! Monte Carlo integrating in the process
          do j = 1,NE
             idx = idx+1

             L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1                 
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny) ; N2 = mod(N1,Nz)

             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)

             !            
             g1 = (xl1 - xgc) * (ym1 - ygc) * (zn1 - zgc)
             g2 = (xgc - xl0) * (ym1 - ygc) * (zn1 - zgc)
             !            
             g3 = (xl1 - xgc) * (ygc - ym0) * (zn1 - zgc)
             g4 = (xgc - xl0) * (ygc - ym0) * (zn1 - zgc)
             !             
             g5 = (xl1 - xgc) * (ym1 - ygc) * (zgc - zn0)
             g6 = (xgc - xl0) * (ym1 - ygc) * (zgc - zn0)
             !             
             g7 = (xl1 - xgc) * (ygc - ym0) * (zgc - zn0)
             g8 = (xgc - xl0) * (ygc - ym0) * (zgc - zn0)

             fac2 =Volume_scale
             if (abs(T_marker-1.0) .GT. epsilon(0.0))  then
                fac1 = fac2*weight(idx,is) *p(wgt_adjust,idx,is)
             else
                fac1 = fac2*weight(idx,is)
             end if

             grid(L0,M0,N0) = grid(L0,M0,N0) + g1*fac1
             gnrm(L0,M0,N0) = gnrm(L0,M0,N0) + g1*fac2
             grid(L2,M0,N0) = grid(L2,M0,N0) + g2*fac1  
             gnrm(L2,M0,N0) = gnrm(L2,M0,N0) + g2*fac2
             !            
             grid(L0,M2,N0) = grid(L0,M2,N0) + g3*fac1  
             gnrm(L0,M2,N0) = gnrm(L0,M2,N0) + g3*fac2
             grid(L2,M2,N0) = grid(L2,M2,N0) + g4*fac1  
             gnrm(L2,M2,N0) = gnrm(L2,M2,N0) + g4*fac2
             !            
             grid(L0,M0,N2) = grid(L0,M0,N2) + g5*fac1  
             gnrm(L0,M0,N2) = gnrm(L0,M0,N2) + g5*fac2
             grid(L2,M0,N2) = grid(L2,M0,N2) + g6*fac1  
             gnrm(L2,M0,N2) = gnrm(L2,M0,N2) + g6*fac2
             !            
             grid(L0,M2,N2) = grid(L0,M2,N2) + g7*fac1  
             gnrm(L0,M2,N2) = gnrm(L0,M2,N2) + g7*fac2
             grid(L2,M2,N2) = grid(L2,M2,N2) + g8*fac1  
             gnrm(L2,M2,N2) = gnrm(L2,M2,N2) + g8*fac2
 
             ! Accumulate z-by-z the estimate for F0 based on the particle locations
             if (numerical_denom) then
                F0_num(N0) = F0_num(N0) + (zn1-zgc)*fac2*delta_x*delta_y
                F0_num(N2) = F0_num(N2) + (zgc-zn0)*fac2*delta_x*delta_y
             end if

          end do

          call reduce3(grid)
          call reduce3(gnrm)
          if (numerical_denom) call sum_allreduce(F0_num)
          
          ! Use gnrm to find the average value of g at each grid point
          ! If gnrm_on is false, just use the average number of particles per cell
          if (gnrm_on )then
             do iz = 0,Nz-1
                where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                   grid(:,:,iz) = grid(:,:,iz) /gnrm(:,:,iz)
                end where 
             end do
          else
             grid = grid/real(Ncell)
          end if

          if (numerical_denom) F0_num = F0_num / real(Ncell*Nx*Ny)

          do iz = 0,Nz-1
             if (make_poisson_well_posed) then
                grid(:,:,iz) = grid(:,:,iz) - sum(grid(:,:,iz))/real(Nx*Ny)
             end if
             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid(:,:,iz), rho_k(:,:))

             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                   !
                   ! Integrate over mu and normalize the Monte Carlo integration performed above
                   phi(ikx,iky,iz) = phi(ikx,iky,iz) + Zs(is)*density(is)*J0(ikx,iky,iz,i,is)*rho_k(ikx,iky) *dmu(i)*mcNorm(iz,i)
                   if (numerical_denom) phiDenom(ikx,iky,iz) = phiDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/temperature(is))*(1.0 - J0(ikx,iky,iz,i,is)**2)*dmu(i)*mcNorm(iz,i)*F0_num(iz)
                end do
             end do ! End kx/ky loop
          end do    ! End z loop
       end do ! End mu loop
    end do ! End species loop

    if (phi_diagnostics_on .AND. proc0 .AND. (mod(step,2) .EQ. 0)) call phi_diagnostics(phi)

    ! If desired, output to a file to determine the integrity of the estimate of Gamma_0
    if ( first_denom_diag .AND. numerical_denom_diagnostics_on ) then
       do iky = 0,Ny-1 
          do ikx = 0,Nx/2
             phi_denom_analytic = 0.0
             do is=1,num_species
                call gamma_n(k2_perp(ikx,iky)*temperature(is)*mass(is)/(Zs(is))**2, Gamma_0)
                phi_denom_analytic = phi_denom_analytic + (Zs(is)**2*density(is)/temperature(is))*(1.0-Gamma_0)
             end do
             write(numerical_denom_unit,*) k2_perp(ikx,iky),phi_denom_analytic, (phiDenom(ikx,iky,iz), iz=0,Nz-1)
          end do
       end do 
       close(numerical_denom_unit)
       first_denom_diag = .false.
    end if

    where ( abs(phiDenom) .GT. epsilon(0.0))
       phi = phi/(phiDenom )   
    else where
       phi = 0.0
    end where

    if (k0_method .EQ. 0) then
       phi(0,0,:) = 0.0
    end if
    if (k0_method .EQ. 1) then
       do iz = 0,Nz-1
          phi(:,:,iz) = phi(:,:,iz)- phi(0,0,iz)
       end do
    end if

    if (one_k_mode) then
    ! Single out one k if the option is chosen
       if (init_kz .GT. -1000) then
          phi(:,:,0:init_kz-1) = 0.0
          phi(:,:,init_kz+1 : Nz-init_kz-1) = 0.0
          phi(:,:,Nz-init_kz+1 : Nz-1) = 0.0
       end if
       if (init_kx .GT. -1000) then 
          phi(0:init_kx-1,:,:) = 0.0
          phi(init_kx+1:Nx/2,:,:) = 0.0
       end if
       if (init_ky .GT. -1000) then 
          phi(:,0:init_ky-1,:) = 0.0
          phi(:,init_ky+1:Ny-init_ky-1,:) = 0.0
          phi(:,Ny-init_ky+1 : Ny-1, :) = 0.0
       end if
       
    end if

    ! Exclude a given mode oif option is chosen
    if (exclude_kx .GT. -1000) then
       phi(exclude_kx,:,:) = 0.0
       phi(Nz-exclude_kx,:,:) = 0.0
    end if
    if (exclude_ky .GT. -1000) then
       phi(:,exclude_ky,:) = 0.0
       phi(:,Ny-exclude_ky,:) = 0.0
    end if
    if (exclude_kz .GT. -1000) then
       phi(:,:,exclude_kz) = 0.0
       phi(:,:,Nz-exclude_kz) = 0.0
    end if


    if (beginning) beginning = .false.

    deallocate(rho_k, grid, gnrm)
    if (numerical_denom) deallocate (F0_num)
    !
  end subroutine calculate_phi
  !-------------------------------------------------------------------
  subroutine randomphase_phi(phase_amp,phi_krand)
    use fft_work, only: fftw_forward
    implicit none
    !
    ! Read in an x space phi, convert to k space, then randomize
    ! the phases.  Apply a new randomization every timestep.
    !
    !
    real, intent(in) :: phase_amp
    complex, dimension(0:,0:,0:), intent (out) :: phi_krand
    real, dimension(:,:,:), allocatable :: phi_xrand
    !    
    integer :: iz, iy, ix, ikz, iky, ikx
    real :: xdum, ydum, phase
    integer, save :: plan_real2compl
    logical, save :: beginning=.true.
    integer :: idate(8), isize
    integer, allocatable, dimension(:) :: iseed
    integer :: i, j, k, l, idx
!!$    !
!!$    ! need to seed the first call to random number with a seed
    call date_and_time (values=idate)
    call random_seed  (size=isize)
    allocate (iseed(isize))
    call random_seed (get=iseed)
    !
    iseed = iseed + (idate(8) - 500)
    call random_seed (put=iseed)
!!$    !
    allocate(phi_xrand(0:Nx-1,0:Ny-1,0:Nz-1))
!!$    !
    if(beginning) then
       do iz=0,Nz-1 ! there's no safety here if phi should be 3D, so Nz must be 0
          do iy=0,Ny-1
             do ix=0,Nx-1
!                read(phi_x_in_unit,*) xdum, ydum, phi_xrand(ix,iy,iz)
             end do
          end do
       end do
!       close(phi_x_in_unit)
       call rfftwnd_f77_create_plan (plan_real2compl,3,Msize,fftw_forward,0) 
       beginning=.false.
       !
       call rfftwnd_f77_one_real_to_complex (plan_real2compl, phi_xrand, phi_krand)
       !
       phi_krand = phi_krand/real(Nx*Ny*Nz)   ! FFT scale factor
       !
    end if
    do ikz=0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             call random_number(phase)
             phase = (phase-0.5)*phase_amp
             phi_krand(ikx,iky,ikz) = exp(zi*phase)*phi_krand(ikx,iky,ikz)
          end do
       end do
    end do
    !    
  end subroutine randomphase_phi
  !-------------------------------------------------------------------
  subroutine phi_diagnostics(phi_in) 
    ! Diagnostic modes:
    ! 1 -- Once-through, output k_perp vs phi, and halt execution
    ! 2 -- Log phiErr(z) to a file phiErr.log and halt execution 
    ! 3 -- Output phiErr(z) as a normal output file, continue running
    ! Analytic functions
    ! 1 -- w = 1
    ! 2 -- w = E
    ! 3 -- w = sin(2pi|vz|)
    implicit none
    complex, dimension(0:,0:,0:), intent(in):: phi_in
    real, dimension(:), allocatable:: PhiErr, analytic
    real:: erf, phiDenom

    allocate(PhiErr(0:Nz-1))
    allocate(analytic(0:Nz-1))
    PhiErr  = 0.0

!    phiDenom = (erf( sqrt(Emax)) - (2.0/pi)*sqrt(Emax)*exp(-Emax))
    phiDenom=1.0
    
    if (phi_diag_mode .EQ. 1) open(unit=phi_k_unit,file=phi_k_str,status="replace")

    do ikx = 0,Nx/2
       do iky = 0,Ny-1
          do iz = 0,Nz-1
             if (phi_diag_func .EQ. 1)  analytic(iz) = exp(-0.5*k2_perp(ikx,iky)/B0_grid(iz)**2)
             if (phi_diag_func .EQ. 2)  analytic(iz) = exp(-0.5*k2_perp(ikx,iky)/B0_grid(iz)**2) * (1.5 - 0.5*k2_perp(ikx,iky)/B0_grid(iz)**2)
             if (phi_diag_func .EQ. 3)  analytic(iz) = exp(-0.5*k2_perp(ikx,iky)/B0_grid(iz)**2) * 0.13048657849302733
             if (abs( real(phi(ikx,iky,iz))/phiDenom - analytic(iz) ) .GT. PhiErr(iz)) PhiErr(iz) = abs(real(phi(ikx,iky,iz))/phiDenom - analytic(iz)) 
          end do
          if (phi_diag_mode .EQ. 1) write(phi_k_unit,*) sqrt(k2_perp(ikx,iky)),  (real(phi(ikx,iky,iz))/phiDenom ,analytic(iz),iz=0,Nz-1)
       end do
    end do
    if (phi_diag_mode .EQ. 1) then
       write(*,*) "PhiErr(z) = ", (PhiErr(iz),iz=0,Nz-1)
       close(phi_k_unit)
       stop
    else if (phi_diag_mode .EQ. 2) then
       write(phiErrLog_unit,'(1p,3I10,3g15.7,I10,1000g15.7)') Ncell, Nx, Ny, Lx, Ly,Emax,Nmu,(PhiErr(iz),iz=0,Nz-1)
       write(*,*) "PhiErr(z) = ", (PhiErr(iz),iz=0,Nz-1)
    else if (phi_diag_mode .EQ. 3) then
       close(phiErrVSt_unit)
       open(unit=phiErrVSt_unit,file=phiErrVSt_str,access="append")
       write(phiErrVSt_unit,'(1p,1000g15.7)') time,(PhiErr(iz),iz=0,Nz-1) 
    end if

    deallocate(PhiErr) 
  end subroutine phi_diagnostics
  !-------------------------------------------------------------------
  function efield (E, xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
    !
    ! Finding the electric field at the marker positions.
    !
    implicit none
    real  :: efield, tmp
    real , dimension (0:,0:,0:), intent(in) :: E
    real , intent(in) :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, Volume_scale
    integer :: L0, L2, M0, M2, N0, N2
    !
    tmp = 0.
    !
    call accum (tmp, E(L0, M0, N0), xl1, xgc, ym1, ygc, zn1, zgc)
    call accum (tmp, E(L2, M0, N0), xgc, xl0, ym1, ygc, zn1, zgc)
    !
    call accum (tmp, E(L0, M2, N0), xl1, xgc, ygc, ym0, zn1, zgc)
    call accum (tmp, E(L2, M2, N0), xgc, xl0, ygc, ym0, zn1, zgc)
    !
    call accum (tmp, E(L0, M0, N2), xl1, xgc, ym1, ygc, zgc, zn0)
    call accum (tmp, E(L2, M0, N2), xgc, xl0, ym1, ygc, zgc, zn0)
    !
    call accum (tmp, E(L0, M2, N2), xl1, xgc, ygc, ym0, zgc, zn0)
    call accum (tmp, E(L2, M2, N2), xgc, xl0, ygc, ym0, zgc, zn0)
    !
    efield = tmp * Volume_scale
    !
  end function efield
  !-------------------------------------------------------------------
  subroutine accum (fsum, field, xr, xl, yr, yl, zr, zl)
    !
    ! Used for finding function values at marker positions. 
    !
    implicit none
    real , intent(in) :: field, xr, xl, yr, yl, zr, zl
    real , intent(in out) :: fsum
    !
    fsum = fsum + field*(xr-xl)*(yr-yl)*(zr-zl)
    !
  end subroutine accum
  !-------------------------------------------------------------------
  subroutine update_weights(w, wdot, velz, z, vx, En, Epar)
    !
    ! Solving the weight evolution equation for delta-f gyrokinetics using the
    ! method of characteristics.  See Broemstrup thesis eq 2.21.  Option for 
    ! curvature exists.
    !
    implicit none
    real, dimension(1:,1:), intent(in) :: w, velz, vx, En,z
    real, dimension(1:,1:), intent(in out) :: wdot, Epar
    real :: fac_2 = 0., v2
    real :: vdrift,E_temp
    logical, save :: first = .true.
    integer :: idx
    !
    if (first) then
       if (ParkerLee) fac_2 = 1.
       nprime(1) = -1./Ln_1
       tprime(1) = -1./LT_1
       if (num_species > 1) then
          nprime(2) = -1./Ln_2
          tprime(2) = -1./LT_2
       end if
       if (num_species > 2) then
          nprime(3) = -1./Ln_3
          tprime(3) = -1./LT_3
       end if
    end if
    !
    do k=1,num_species
       epar_fac(k) = Zs(k) /sqrt(temperature(k)*mass(k))
       if (curvature_on) then
          idx=0
          do i=1,Nmu
             mu_temp = mu_grid(i)
             NE = NE_mu(i)
             do j=1,NE
                idx = idx+1
                E_temp = En(idx,k)
                z_temp = z(idx,k)
                B0local = B0(z_temp)
                ! This loop needs to be rearranged in order to put it on the GPU.  Seems not too difficult.
                vdrift = (2.0*E_temp - B0local*mu_grid(i)  ) *Rinv / B0local
                wdot(idx,k) = - vx(idx,k) * ((nprime(k)+tprime(k) * (E_temp - 1.5)) + vdrift) + epar_fac(k) * Epar(idx,k)* velz(idx,k) 
             end do
          end do
       else
          idx=0
          do i=1,Nmu
             mu_temp = mu_grid(i)
             NE = NE_mu(i)
             do j=1,NE
                idx = idx+1
                E_temp = En(idx,k)
                wdot(idx,k) = - vx(idx,k) * ((nprime(k)+tprime(k) *(E_temp - 1.5)))  + epar_fac(k) * Epar(idx,k) * velz(idx,k) 
             end do
          end do
       end if
    end do

!    write(*,*) delta_t*sqrt(sum(p(wd,:,1)**2,Npart))/real(Npart), delta_t*sqrt(sum(p(wd,:,2)**2,Npart))/real(Npart)

!    if (abs(T_marker-1.0) .GE. epsilon(0.0))    p(wgt,:,:) = p(wgt,:,:)*p(wgt_adjust,:,:)
  end subroutine update_weights
  !-------------------------------------------------------------------
  subroutine vspace_coll_diagnostics(delta_f_in)
    !
    ! Computes and outputs delta_f on the velocity-space grid.
    !
    implicit none
    real, dimension(1:,1:), intent(in) :: delta_f_in
    real :: vperp_grid, vz_grid
    integer :: idx, Num_vz, iv,ixi
    logical, save :: first_vel_dia = .TRUE.
    !
    !
    if (first_vel_dia) then
       open(unit=vspace_coll_diagnostics_unit, file=vel_diagnostics_str, status='replace')
       close(vspace_coll_diagnostics_unit)
       first_vel_dia = .FALSE.
    end if

    open(unit=vspace_coll_diagnostics_unit, file=vel_diagnostics_str, status='old', access='append')
    !
    do iv =1,Nv_coll
       Num_vz = N_Chi*(2*iv-1)
       do ixi=1,Nxi_coll
          write(vspace_coll_diagnostics_unit,'(100g15.7)') vpar_coll(iv,ixi), vperp_coll(iv,ixi),delta_f_in(iv,ixi)
       end do
       write(vspace_coll_diagnostics_unit,'(A1)') " "
    end do
    write(vspace_coll_diagnostics_unit,'(A1)') " "
    !
    close(vspace_coll_diagnostics_unit)
    !
  end subroutine vspace_coll_diagnostics

  subroutine particle2grid_coll(collision_count,x_gc,y_gc,z_gc,En_gc,vz_gc,weight,v_int,xi_int,jmax,df_old)
    implicit none
    real,  dimension(0:,1:,1:),intent(inout):: df_old
    integer, intent(in):: jmax, collision_count
    real, dimension(1:,1:), intent(in):: x_gc,y_gc,z_gc,weight,vz_gc,En_gc
    integer,dimension(1:,1:),intent(in):: v_int
    integer,dimension(1:,1:),intent(inout):: xi_int
    integer:: L0,M0,N0,L1,M1,N1,L2,M2,N2,idx,is,iv,ixi,j
    real:: xl0,xl1,xgc,ym0,ym1,ygc,zn0,zn1,zgc, fac,g1,g2,g3,g4,g5,g6,g7,g8
    real, dimension(:,:,:), allocatable:: df_nrm, integrand_n, integrand_u, integrand_E
  
    allocate (df_nrm(0:jmax,1:Nv_coll,1:Nxi_coll))

    df_old = 0.0
    df_nrm = 0.0
    do is = 1,num_species
       idx = 0
       do imu = 1,Nmu
          NE = NE_mu(imu)
          !
          do k = 1,NE
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1                 
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny) ;  N2 = mod(N1, Nz)
             !
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
             !
             g1 = (xl1 - xgc) * (ym1 - ygc) * (zn1 - zgc) ! find the contribution
             g2 = (xgc - xl0) * (ym1 - ygc) * (zn1 - zgc) ! of the gyrocenter to the
             g3 = (xl1 - xgc) * (ygc - ym0) * (zn1 - zgc) ! distribution function at
             g4 = (xgc - xl0) * (ygc - ym0) * (zn1 - zgc) ! each grid point
             g5 = (xl1 - xgc) * (ym1 - ygc) * (zgc - zn0)
             g6 = (xgc - xl0) * (ym1 - ygc) * (zgc - zn0)
             g7 = (xl1 - xgc) * (ygc - ym0) * (zgc - zn0)
             g8 = (xgc - xl0) * (ygc - ym0) * (zgc - zn0)

             !
             fac = weight(idx,is)*Volume_scale
             iv = v_int(idx,is)
             
             ! There may be a better way to do this. As it is, v_grid(Nv_coll) < Emax, 
             ! so a particle's vz may be greater than its v_grid value. Using the particles'
             ! actual v (not its interpolated v_grid) instead. Costs a square root.
             ixi = 1 + min(Nxi_coll-1,int( acos( vz_gc(idx,is)/sqrt(2.0*En_gc(idx,is))) /dxi))

             ! Save for later so we don't have to repeat the pain above
             xi_int(idx,is) = ixi
             !
             ! Deposit all local particles on mesh, to be shared globally
             j = idx_j(L0, M0, N0, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g1*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g1*Volume_scale
             !
             j = idx_j(L2, M0, N0, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g2*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g2*Volume_scale
             !
             j = idx_j(L0, M2, N0, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g3*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g3*Volume_scale
             !
             j = idx_j(L2, M2, N0, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g4*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g4*Volume_scale
             !
             j = idx_j(L0, M0, N2, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g5*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g5*Volume_scale
             !
             j = idx_j(L2, M0, N2, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g6*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g6*Volume_scale
             !
             j = idx_j(L0, M2, N2, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g7*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g7*Volume_scale
             !
             j = idx_j(L2, M2, N2, is)
             df_old(j, iv, ixi) = df_old(j, iv, ixi) + g8*fac
             df_nrm(j, iv, ixi) = df_nrm(j, iv, ixi) + g8*Volume_scale
             !
          end do
       end do
    end do
    !
    call reduce3 (df_old)
    call reduce3 (df_nrm)
    !
    where (abs(df_nrm) > epsilon(0.0)) ! make sure df_nrm is larger than numerical zero
       df_old = df_old/df_nrm ! normalize df_old point by point
    end where

    !
    deallocate (df_nrm)

  end subroutine particle2grid_coll

  subroutine init_collision()
    ! Determines the matrix elements AA and CC for the pitch-angle
    ! scattering term of the collision operator (I. Broemstup thesis eq 5.5).
    ! The coefficient nu_d is defined in eq 5.7 and includes the user-specified
    ! magnitude nu_coll(is).  The tridiagonal matrix elements can be found in eq 5.17.
    !    double precision :: derf
    use constants, only: pi
    implicit none
    !
    real  :: x, ChandraG,Jarg_is,Jarg_kperp,Jarg_v,v_pre,Jarg_z,J0,J1,v_mhalf,v_phalf,G_phalf, G_mhalf,J0_temp,xi_phalf, xi_mhalf
    real,dimension(:,:), allocatable :: nu_par_mhalf, nu_par_phalf
    complex,dimension(:,:), allocatable:: integrandc
    real,dimension(:,:), allocatable:: integrandr
    complex:: tempc
    real:: tempr
     
    integer :: is, j, ixi, iv,jmax
    real:: erf
    !
    ! Need to include nu_ee factor and nu_ei, too.   Important to fix, but not for Zpinch.
!    allocate(Chi(1:Number_Chi,1:Number_E))
    !
    jmax = (num_species*((Nx/2)+1)*Ny*Nz) - 1

    allocate(J0_coll(0:jmax,1:Nv_coll,1:Nxi_coll))
    allocate(xi_grid(Nxi_coll))
    allocate(v_grid(Nv_coll))
    allocate(v_int(Npart,num_species))
    allocate(vpar_coll(1:Nv_coll,1:Nxi_coll))
    allocate(vperp_coll(1:Nv_coll,1:Nxi_coll))
    allocate(F0_coll(1:Nv_coll))
    allocate(nu_D_grid(1:Nv_coll,1:num_species))
    allocate(nu_Dei_grid(1:Nv_coll,1:num_species))
    allocate(integrandr(1:Nv_coll,1:Nxi_coll))
    allocate(integrandc(1:Nv_coll,1:Nxi_coll))
    if (pitch_angle_coll_on) then 
       allocate(AA_xi(1:Nxi_coll,1:Nv_coll,1:num_species))
       allocate(BB_xi(1:Nxi_coll,1:Nv_coll,0:jmax))
       allocate(CC_xi(1:Nxi_coll,1:Nv_coll,1:num_species))
       allocate(nu_s_grid(1:Nv_coll,1:num_species))
       allocate(nu_E_grid(1:Nv_coll,1:num_species))
       allocate(delta_nu_grid(1:Nv_coll,1:num_species))
       allocate(nu_par_grid(1:Nv_coll,1:num_species))
       allocate(nu_par_mhalf(1:Nv_coll,1:num_species))
       allocate(nu_par_phalf(1:Nv_coll,1:num_species))
    end if
    if (energy_diff_coll_on) then
       allocate(AA_En(1:Nv_coll,1:Nxi_coll,1:num_species))
       allocate(BB_En(1:Nv_coll,1:Nxi_coll,0:jmax))
       allocate(CC_En(1:Nv_coll,1:Nxi_coll,1:num_species))
    end if

    dv = sqrt(2.0*Emax)/real(Nv_coll)
    dxi= pi/real(Nxi_coll)

    do i = 1,(Nxi_coll+1)/2
       xi_grid(i) = (real(i)-0.5)*dxi
    end do
    do i = (Nxi_coll+1)/2 + 1,Nxi_coll
       xi_grid(i) = 0.5*pi + (0.5*pi - xi_grid(Nxi_coll-i+1))
    end do

    do i = 1,Nv_coll
       v_grid(i) = (real(i)-0.5)*dv
       F0_coll(i) = exp(-0.5*v_grid(i)**2) / sqrt( (2.0*pi)**3)
       do ixi = 1,Nxi_coll
          integrandr(i,ixi) = F0_coll(i)
          integrandc(i,ixi) = F0_coll(i)
       end do
    end do 

    do ixi = 1,Nxi_coll
       do iv=1,Nv_coll
          vpar_coll(iv,ixi) = v_grid(iv)*cos(xi_grid(ixi)) 
          vperp_coll(iv,ixi) = v_grid(iv)*sin(xi_grid(ixi)) 
       end do
    end do
 
    do j=0,jmax
       is = idx_spec_k(j)
       ikx = idx_ikx(j)
       iky = idx_iky(j)
       iz = idx_iz(j)
       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             call j0_function(sqrt(k2_perp(ikx,iky))*vts(is)*mass(is)*vperp_coll(iv,ixi)/(B0_grid(iz)*abs(Zs(is))),J0_temp)
             J0_coll(j,iv,ixi) = J0_temp
          end do
       end do 
    end do
    
    do is=1,num_species
     
       ! Store v index for each particle, since it doesn't change 
       do idx = 1,Npart
          v_int(idx,is) = 1 + min(Nv_coll-1,int( sqrt(2.0*p(En,idx,is))/dv ))
       end do

       do i=1,Nv_coll
          if (pitch_angle_coll_on) then 
             ! nu_D
             x = v_grid(i) /sqrt(2.0)  ! To fit with normalization in Abel, et al.
             ChandraG =  (erf(x) - (2.0/sqrt(pi))*x*exp(-x**2)) / (2.0*x**2)  
             nu_D_grid(i,is) = nu_coll(is) * ( erf(x) - ChandraG)/x**3

             ! nu_D(ei) for electron-ion interaction
             if (is .EQ. 2) then
                nu_Dei_grid(i,is) = (density(1)*Zs(1)**2*nu_coll(2)/density(2)) /(x**3)
             else
                nu_Dei_grid(i,is) = 0.0
             end if

             ! Other collision frequencies
             nu_s_grid(i,is) = nu_coll(is) * 4.0*ChandraG/x
             nu_par_grid(i,is) = nu_coll(is)*2.0*ChandraG/x**3
             delta_nu_grid(i,is) = nu_D_grid(i,is) - nu_s_grid(i,is)
             nu_E_grid(i,is) = -2.0*Delta_nu_grid(i,is) - nu_par_grid(i,is)
 
             ! Populate "half-step" nu_par for the purposes of calculate energy diffusion matrix
             if (i .GT. 1) then
                x = 0.5*(v_grid(i)+v_grid(i-1)) /sqrt(2.0)
             else
                ! This is fictional; should be zero. But nu_par_mhalf(1) is never needed 
                x = 0.5*v_grid(1) /sqrt(2.0)
             end if
             ChandraG =  (erf(x) - (2.0/sqrt(pi))*x*exp(-x**2)) / (2.0*x**2)  
             nu_par_mhalf(i,is) = nu_coll(is)*2.0*ChandraG/x**3 

             if (i .LT. Nv_coll) then
                x = 0.5*(v_grid(i)+v_grid(i+1)) /sqrt(2.0)
             else
                x = (v_grid(Nv_coll)+0.5*dv) / sqrt(2.0)
             end if
             ChandraG =  (erf(x) - (2.0/sqrt(pi))*x*exp(-x**2)) / (2.0*x**2)  
             nu_par_phalf(i,is) = nu_coll(is)*2.0*ChandraG/x**3

          end if
       end do
       
    end do
    !   
    if (.NOT.coll_ei_on) nu_Dei_grid = 0.0 

    ! Build pitch-angle diffusion matrix
    if (pitch_angle_coll_on) then
       !   
       AA_xi = 0.0 
       BB_xi = 0.0 
       CC_xi = 0.0 
       ! 
       do is=1,num_species
          do iv=1,Nv_coll
             do ixi=2,Nxi_coll-1
                xi_phalf = 0.5*(xi_grid(ixi+1)+xi_grid(ixi))
                G_phalf = sin(xi_phalf)
  
                xi_mhalf = 0.5*(xi_grid(ixi)+xi_grid(ixi-1))
                G_mhalf = sin(xi_mhalf)

                AA_xi(ixi,iv,is) = - 0.5 * nu_D_grid(iv,is) * G_mhalf /(dxi**2*sin(xi_grid(ixi)) ) 
                CC_xi(ixi,iv,is) = - 0.5 * nu_D_grid(iv,is) * G_phalf /(dxi**2*sin(xi_grid(ixi)) ) 
                do iz=0,Nz-1
                   do iky = 0,Ny-1
                      do ikx = 0,Nx/2
                         j = idx_j_k(ikx,iky,iz,is)
                         BB_xi(ixi,iv,j) =  - (AA_xi(ixi,iv,is) + CC_xi(ixi,iv,is))
                         if (gyrodiff_on) BB_xi(ixi,iv,j) = BB_xi(ixi,iv,j) + (k2_perp(ikx,iky)*v_grid(iv)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*(nu_D_grid(iv,is)+nu_Dei_grid(iv,is))*(1.0+cos(xi_grid(ixi))**2)
                      end do
                   end do
                end do
             end do

             xi_phalf = 0.5*(xi_grid(2) + xi_grid(1))
             G_phalf = sin(xi_phalf)

             xi_mhalf = 0.5*(xi_grid(Nxi_coll)+xi_grid(Nxi_coll-1))
             G_mhalf = sin(xi_mhalf)

             AA_xi(Nxi_coll,iv,is) = - 0.5 * nu_D_grid(iv,is) * G_mhalf /(dxi**2*sin(xi_grid(Nxi_coll)) ) 
             CC_xi(1,iv,is) = - 0.5 * nu_D_grid(iv,is) * G_phalf /(dxi**2*sin(xi_grid(1)) ) 

             do iz=0,Nz-1
                do iky = 0,Ny-1
                   do ikx = 0,Nx/2
                      j = idx_j_k(ikx,iky,iz,is)
                      BB_xi(1,iv,j) =  - CC_xi(1,iv,is)
                      if (gyrodiff_on) BB_xi(1,iv,j) = BB_xi(1,iv,j)  + (k2_perp(ikx,iky)*v_grid(iv)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*(nu_D_grid(iv,is)+nu_Dei_grid(iv,is))*(1.0+cos(xi_grid(1))**2) 
                      BB_xi(Nxi_coll,iv,j) = -AA_xi(Nxi_coll,iv,is)
                      if (gyrodiff_on) BB_xi(Nxi_coll,iv,j) = BB_xi(Nxi_coll,iv,j) + (k2_perp(ikx,iky)*v_grid(iv)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*(nu_D_grid(iv,is)+nu_Dei_grid(iv,is))*(1.0+cos(xi_grid(Nxi_coll))**2) 
                   end do
                end do
             end do
          end do
       end do
       !
    end if

    ! Build energy diffusion matrix
    if (energy_diff_coll_on) then
       !   
       AA_En = 0.0 
       BB_En = 0.0 
       CC_En = 0.0 
       ! 
       do is=1,num_species
          do ixi=1,Nxi_coll
             do iv=2,Nv_coll-1
                v_phalf = 0.5*(v_grid(iv+1)+v_grid(iv))
                G_phalf = nu_par_phalf(iv,is)*(v_phalf)**4*exp(-0.5*v_phalf**2)
  
                v_mhalf = 0.5*(v_grid(iv)+v_grid(iv-1))
                G_mhalf = nu_par_mhalf(iv,is)*(v_mhalf)**4*exp(-0.5*v_mhalf**2)
 
                AA_En(iv,ixi,is) = - 0.5 * G_mhalf /(dv**2*v_grid(iv)**2 * exp(-0.5*v_grid(iv)**2) ) 
                CC_En(iv,ixi,is) = - 0.5 * G_phalf /(dv**2*v_grid(iv)**2 * exp(-0.5*v_grid(iv)**2) )
                do iz=0,Nz-1
                   do iky = 0,Ny-1
                      do ikx = 0,Nx/2
                         j = idx_j_k(ikx,iky,iz,is)
                         BB_En(iv,ixi,j) =  - (AA_En(iv,ixi,is) + CC_En(iv,ixi,is)) 
                         if (gyrodiff_on) BB_En(iv,ixi,j) = BB_En(iv,ixi,j) + (k2_perp(ikx,iky)*v_grid(iv)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*nu_par_grid(iv,is)*sin(xi_grid(ixi))**2 
                      end do
                   end do
                end do
             end do

             ! Use Neumann boundary conditions for now...
             v_phalf = 0.5*(v_grid(2) + v_grid(1))
             G_phalf = nu_par_phalf(1,is)*(v_phalf)**4*exp(-0.5*v_phalf**2)

             v_mhalf = 0.5*(v_grid(Nv_coll)+v_grid(Nv_coll-1))
             G_mhalf = nu_par_mhalf(Nv_coll,is)*(v_mhalf)**4*density(is)*exp(-0.5*v_mhalf**2)

             AA_En(Nv_coll,ixi,is) = - 0.5*G_mhalf/(dv**2*v_grid(Nv_coll)**2 * exp(-0.5*v_grid(Nv_coll)**2))
             CC_En(1,ixi,is) = - 0.5*G_phalf/(dv**2*v_grid(1)**2 * exp(-0.5*v_grid(1)**2))
             do iz=0,Nz-1
                do iky = 0,Ny-1
                   do ikx = 0,Nx/2
                      j = idx_j_k(ikx,iky,iz,is)
                      BB_En(1,ixi,j) = - CC_En(1,ixi,is) 
                      if (gyrodiff_on) BB_En(1,ixi,j) = BB_En(1,ixi,j) + (k2_perp(ikx,iky)*v_grid(1)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*nu_par_grid(1,is)*sin(xi_grid(ixi))**2  
                      BB_En(Nv_coll,ixi,j) = - AA_En(Nv_coll,ixi,is)  
                      if (gyrodiff_on) BB_En(Nv_coll,ixi,j) = BB_En(Nv_coll,ixi,j) + (k2_perp(ikx,iky)*v_grid(Nv_coll)**2*mass(is)*temperature(is)/(Zs(is)**2*B0_grid(iz)**2))*0.25*nu_par_grid(Nv_coll,is)*sin(xi_grid(ixi))**2  
                   end do
                end do
             end do
          end do
       end do
       !
!       if (proc0) WRITE(*,*) "A_En = ", AA_En(1,1,1),AA_En(1,1,2), " C_En = ", CC_En(1,1,1),CC_En(1,1,2)
    end if
 
    if (conserve_coll_sm_on) then
       if (pitch_angle_coll_on) call init_conserve_lorentz_coll
       if (energy_diff_coll_on) call init_conserve_en_diff_coll
    end if
  
    if (pitch_angle_coll_on) deallocate(nu_par_mhalf, nu_par_phalf)

  end subroutine init_collision

  subroutine init_conserve_lorentz_coll()
    implicit none
    integer:: jmax, is, iv, ixi, j, j_lo, j_hi, blocksize
    real:: Jarg_is,Jarg_z, Jarg_v, Jarg_kperp, v_pre, J0,J1, temp_integral_r, temp_integral_r1
    real, dimension(:), allocatable:: d_u, RHS, d_q
    real, dimension(:,:), allocatable:: integrand, s0, w0, z0, z2
    logical,save:: first = .true.
          !
    jmax = (num_species*((Nx/2)+1)*Ny*Nz)  - 1
 
    if (first) then
       allocate(v0_lorentz(1:Nv_coll,1:Nxi_coll,0:jmax))
       allocate(v1_lorentz(1:Nv_coll,1:Nxi_coll,0:jmax))
       allocate(v2_lorentz(1:Nv_coll,1:Nxi_coll,0:jmax))

       allocate(v0y0fac_lorentz(0:jmax,1:Nv_coll,1:Nxi_coll))
       allocate(v1y0fac_lorentz(0:jmax,1:Nv_coll,1:Nxi_coll))
       allocate(v2y2fac_lorentz(0:jmax,1:Nv_coll,1:Nxi_coll))
       first = .false.
    end if
   
    allocate(integrand(1:Nv_coll,1:Nxi_coll))
    allocate(RHS(1:Nxi_coll))
    allocate(d_u(1:num_species))
    allocate(d_q(1:num_species))
    allocate(s0(1:Nv_coll,1:Nxi_coll))
    allocate(w0(1:Nv_coll,1:Nxi_coll))
    allocate(z0(1:Nv_coll,1:Nxi_coll))
    allocate(z2(1:Nv_coll,1:Nxi_coll))
       

    ! Populate v0, v1, v2
    do is = 1,num_species
       do iv = 1,Nv_coll
          do iz=0,Nz-1
             do iky=0,Ny-1
                do ikx=0,Nx/2
                   j = idx_j_k(ikx,iky,iz,is)
                   do ixi = 1,Nxi_coll
                      call j1_function(sqrt(k2_perp(ikx,iky))*vts(is)*mass(is)*vperp_coll(iv,ixi)/(B0_grid(iz)*abs(Zs(is))),J1)
                      call j0_function(sqrt(k2_perp(ikx,iky))*vts(is)*mass(is)*vperp_coll(iv,ixi)/(B0_grid(iz)*abs(Zs(is))),J0)

                      if (J0 .NE. J0_coll(j,iv,ixi)) write(*,*) "There's something wrong with idx_j_k!", J0, J0_coll(j,iv,ixi)

                      v0_lorentz(iv,ixi,j) = nu_D_grid(iv,is)*vperp_coll(iv,ixi) * J1 
                      v1_lorentz(iv,ixi,j) = nu_D_grid(iv,is)*vpar_coll(iv,ixi) * J0_coll(j,iv,ixi) 

                      if (is .EQ. 1) v2_lorentz(iv,ixi,j) = 0.0
                      if (is .EQ. 2) v2_lorentz(iv,ixi,j) = vpar_coll(iv,ixi)
                   end do
                end do
             end do
          end do
       end do
    end do

    ! Calculate denominator integrals
    do is = 1,num_species
       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             integrand(iv,ixi) = nu_D_grid(iv,is) * vpar_coll(iv,ixi)**2 * F0_coll(iv) 
          end do
       end do
       call integrate_vgrid_real(integrand,d_u(is))
    end do
    if (num_species .GT. 1) then
       d_q(:) = vts(2)
    else
       d_q(:) = 1.0
    end if

    ! Calculate s0, w0, z0 by inverting triagonal matrix
    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax

    v0y0fac_lorentz = 0.0
    v1y0fac_lorentz = 0.0
    v2y2fac_lorentz = 0.0
    !      
    do j=j_lo, j_hi
       is = idx_spec_k (j) !; if (is /= 1) write (*,*) 'oops'

       do iv =1,Nv_coll
          !
          ! s0
          RHS = -delta_t*v0_lorentz(iv,:,j)/d_u(is)
          call tridiag_solve_real(Nxi_coll,delta_t*AA_xi(:,iv,is),1.0+delta_t*BB_xi(:,iv,j),delta_t*CC_xi(:,iv,is),RHS,s0(iv,:))
          !
          ! w0
          RHS = -delta_t*v1_lorentz(iv,:,j)/d_u(is)
          call tridiag_solve_real(Nxi_coll,delta_t*AA_xi(:,iv,is),1.0+delta_t*BB_xi(:,iv,j),delta_t*CC_xi(:,iv,is),RHS,w0(iv,:))

          !
          ! z0
          if (is .EQ. 2) then
             RHS = -delta_t*nu_Dei_grid(iv,is)*vpar_coll(iv,:)/d_q(is)
             call tridiag_solve_real(Nxi_coll,delta_t*AA_xi(:,iv,is),1.0+delta_t*BB_xi(:,iv,j),delta_t*CC_xi(:,iv,is),RHS,z0(iv,:))
          else
             z0(iv,:) = 0.0
          end if
       end do

       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll

             call integrate_vgrid_real(v0_lorentz(:,:,j)*z0(:,:),temp_integral_r) 
             call integrate_vgrid_real(v0_lorentz(:,:,j)*s0(:,:),temp_integral_r1) 
             z2(iv,ixi) = z0(iv,ixi) - s0(iv,ixi)*temp_integral_r / ( 1.0 + temp_integral_r1 )

             call integrate_vgrid_real(v1_lorentz(:,:,j)*z0(:,:),temp_integral_r) 
             call integrate_vgrid_real(v1_lorentz(:,:,j)*w0(:,:),temp_integral_r1) 
             z2(iv,ixi) = z2(iv,ixi) - w0(iv,ixi)*temp_integral_r / ( 1.0 + temp_integral_r1 )
          end do
       end do


       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll

             call integrate_vgrid_real(v0_lorentz(:,:,j)*s0(:,:),temp_integral_r) 
             v0y0fac_lorentz(j,iv,ixi) = s0(iv,ixi)/(1.0 + temp_integral_r ) 

             call integrate_vgrid_real(v1_lorentz(:,:,j)*w0(:,:),temp_integral_r) 
             v1y0fac_lorentz(j,iv,ixi) = w0(iv,ixi)/(1.0 + temp_integral_r ) 

             call integrate_vgrid_real(v2_lorentz(:,:,j)*z2(:,:),temp_integral_r) 
             v2y2fac_lorentz(j,iv,ixi) = z2(iv,ixi)/(1.0 + temp_integral_r ) 
          end do
       end do

    end do

    call reduce3 (v0y0fac_lorentz)
    call reduce3 (v1y0fac_lorentz)
    call reduce3 (v2y2fac_lorentz)
    deallocate(integrand,RHS,d_u,d_q,s0,w0,z0,z2)

  end subroutine init_conserve_lorentz_coll

  subroutine init_conserve_en_diff_coll()
    implicit none
    integer:: jmax, is, iv, ixi, j, j_lo, j_hi, blocksize
    real:: Jarg_is, Jarg_v, Jarg_kperp,Jarg_z,v_pre,J0,J1, temp_integral_r, temp_integral_r1
    real, dimension(:), allocatable:: d_u, RHS, d_q
    real, dimension(:,:), allocatable:: integrand, s0, w0, z0, z2
    logical,save:: first = .true.
          !
    jmax = (num_species*((Nx/2)+1)*Ny*Nz)  -1
 
    if (first) then
       allocate(v0_en_diff(1:Nv_coll,1:Nxi_coll,0:jmax))
       allocate(v1_en_diff(1:Nv_coll,1:Nxi_coll,0:jmax))
       allocate(v2_en_diff(1:Nv_coll,1:Nxi_coll,0:jmax))

       allocate(v0y0fac_en_diff(0:jmax,1:Nv_coll,1:Nxi_coll))
       allocate(v1y0fac_en_diff(0:jmax,1:Nv_coll,1:Nxi_coll))
       allocate(v2y2fac_en_diff(0:jmax,1:Nv_coll,1:Nxi_coll))
       first = .false.
    end if
   
    allocate(integrand(1:Nv_coll,1:Nxi_coll))
    allocate(RHS(1:Nv_coll))
    allocate(d_u(1:num_species))
    allocate(d_q(1:num_species))
    allocate(s0(1:Nv_coll,1:Nxi_coll))
    allocate(w0(1:Nv_coll,1:Nxi_coll))
    allocate(z0(1:Nv_coll,1:Nxi_coll))
    allocate(z2(1:Nv_coll,1:Nxi_coll))
       
open(unit=70,file="Jtest.out", status="replace")

       ! Populate v0, v1, v2
       do is = 1,num_species
          do iv = 1,Nv_coll
             do iz=0,Nz-1
                do iky=0,Ny-1
                   do ikx=0,Nx/2
                      j = idx_j_k(ikx,iky,iz,is)
                      do ixi = 1,Nxi_coll
                         
                         call j1_function(sqrt(k2_perp(ikx,iky))*vts(is)*mass(is)*vperp_coll(iv,ixi)/(B0_grid(iz)*abs(Zs(is))),J1)
                         v0_en_diff(iv,ixi,j) = - delta_nu_grid(iv,is)*vperp_coll(iv,is) * J1
                         v1_en_diff(iv,ixi,j) = - delta_nu_grid(iv,is)*vpar_coll(iv,is) * J0_coll(j,iv,ixi)
                         if (is .EQ. 1) v2_en_diff(iv,ixi,j) = 0.0
                         if (is .EQ. 2) v2_en_diff(iv,ixi,j) = nu_E_grid(iv,is)*v_grid(iv)**2*J0_coll(j,iv,ixi)
                      end do
                   end do
                end do
             end do
          end do
       end do

       ! Calculate denominator integrals
       do is = 1,num_species
          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll
                integrand(iv,ixi) = delta_nu_grid(iv,is) * vpar_coll(iv,ixi)**2 * F0_coll(iv)
             end do
          end do
         
          call integrate_vgrid_real(integrand,d_u(is))
       end do

       do is = 1,num_species
          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll
                integrand(iv,ixi) = nu_E_grid(iv,is) * (v_grid(iv))**4 * F0_coll(iv)
             end do
          end do
          call integrate_vgrid_real(integrand,d_q(is))
       end do

       ! Calculate s0, w0, z0 by inverting triagonal matrix
       blocksize = (jmax+1)/nproc
       j_lo = blocksize*iproc
       j_hi = min(jmax,j_lo+blocksize-1)
       if (iproc .EQ. (nproc-1)) j_hi = jmax
  
       v0y0fac_en_diff = 0.0
       v1y0fac_en_diff = 0.0
       v2y2fac_en_diff = 0.0
       !      
       do j=j_lo, j_hi
          is = idx_spec_k (j) !; if (is /= 1) write (*,*) 'oops'

          do ixi =1,Nxi_coll
             !
             ! s0
             RHS = -delta_t*v0_en_diff(:,ixi,j)/d_u(is)
             call tridiag_solve_real(Nv_coll,delta_t*AA_En(:,ixi,is),1.0+delta_t*BB_En(:,ixi,j),delta_t*CC_En(:,ixi,is),RHS,s0(:,ixi))
             !
             ! w0
             RHS = -delta_t*v1_en_diff(:,ixi,j)/d_u(is)
             call tridiag_solve_real(Nv_coll,delta_t*AA_En(:,ixi,is),1.0 + delta_t*BB_En(:,ixi,j),delta_t*CC_En(:,ixi,is),RHS,w0(:,ixi))

             !
             ! z0
             RHS = -delta_t*v2_en_diff(:,ixi,j)/d_q(is)
             call tridiag_solve_real(Nv_coll,delta_t*AA_En(:,ixi,is),1.0+delta_t*BB_En(:,ixi,j),delta_t*CC_En(:,ixi,is),RHS,z0(:,ixi))
          end do

          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll
                call integrate_vgrid_real(v0_en_diff(:,:,j)*z0(:,:),temp_integral_r)
                call integrate_vgrid_real(v0_en_diff(:,:,j)*s0(:,:),temp_integral_r1)
                z2(iv,ixi) = z0(iv,ixi) - s0(iv,ixi)*temp_integral_r / ( 1.0 + temp_integral_r1 )
                call integrate_vgrid_real(v1_en_diff(:,:,j)*z0(:,:),temp_integral_r)
                call integrate_vgrid_real(v1_en_diff(:,:,j)*w0(:,:),temp_integral_r1)
                z2(iv,ixi) = z2(iv,ixi) - w0(iv,ixi)*temp_integral_r / ( 1.0 + temp_integral_r1 )
             end do
          end do


          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll

                call integrate_vgrid_real(v0_en_diff(:,:,j)*s0(:,:),temp_integral_r)
                v0y0fac_en_diff(j,iv,ixi) = s0(iv,ixi)/(1.0 + temp_integral_r) 
                call integrate_vgrid_real(v1_en_diff(:,:,j)*w0(:,:),temp_integral_r)
                v1y0fac_en_diff(j,iv,ixi) = w0(iv,ixi)/(1.0 + temp_integral_r ) 
                call integrate_vgrid_real(v2_en_diff(:,:,j)*z2(:,:),temp_integral_r)
                v2y2fac_en_diff(j,iv,ixi) = z2(iv,ixi)/(1.0 + temp_integral_r) 
             end do
          end do

       end do
       ! call sum_allreduce (df_new)
       call reduce3 (v0y0fac_en_diff)
       call reduce3 (v1y0fac_en_diff)
       call reduce3 (v2y2fac_en_diff)
       deallocate(integrand,RHS,d_u,d_q,s0,w0,z0,z2)   

  end subroutine init_conserve_en_diff_coll


  subroutine apply_en_diff_coll(delta_t,jmax,df_in,df_out)
   use mp, only: iproc, nproc
    implicit none
    integer, intent(in):: jmax
    real, intent(in):: delta_t
    complex, dimension(0:,1:,1:), intent(in):: df_in
    complex, dimension(0:,1:,1:):: df_out
    integer:: blocksize,j_lo,j_hi,iE,is,j,num_xi,ixi
    real, dimension(:), allocatable:: gam

    allocate (gam(1:Nxi_coll))
    df_out = 0.0
    !
    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      
    do j=j_lo, j_hi
       do ixi = 1,Nxi_coll
          is = idx_spec_k (j) !; if (is /= 1) write (*,*) 'oops'

          call tridiag_solve_complex(Nv_coll,delta_t*AA_En(:,ixi,is),1.0+delta_t*BB_En(:,ixi,j),delta_t*CC_En(:,ixi,is),df_in(j,:,ixi),df_out(j,:,ixi))

       end do
    end do
    ! call sum_allreduce (df_new)
    call reduce3 (real(df_out))
    call reduce3 (aimag(df_out))

    deallocate(gam)

  end subroutine apply_en_diff_coll
  !-------------------------------------------------------------------
  subroutine conserve_lorentz_coll(jmax,y0,df_out)
   use mp, only: iproc, nproc
    implicit none
    complex, dimension(0:,1:,1:):: df_out
    complex, dimension(0:,1:,1:), intent(in):: y0
    integer, intent(in):: jmax
    integer:: blocksize,j_lo,j_hi,j,ixi,iv
    complex, dimension(:,:), allocatable:: integrand, y2
    complex:: v0y0, v1y0, v2y2 

    allocate(integrand(1:Nv_coll,1:Nxi_coll))
    allocate(y2(1:Nv_coll,1:Nxi_coll))

    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      
    df_out = 0.0

    do j=j_lo, j_hi
       is = idx_spec_k(j)

       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             integrand(iv,ixi) = y0(j,iv,ixi)*v0_lorentz(iv,ixi,j) 
          end do
       end do

       call integrate_vgrid_complex(integrand,v0y0)
   
       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             integrand(iv,ixi) = y0(j,iv,ixi)*v1_lorentz(iv,ixi,j) 
          end do
       end do

       call integrate_vgrid_complex(integrand,v1y0)

       y2(:,:) = y0(j,:,:) - v0y0fac_lorentz(j,:,:) * v0y0 - v1y0fac_lorentz(j,:,:) * v1y0
       if (is .EQ. 1) then
          v2y2 = 0.0
       else if (is .EQ. 2) then

          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll
                integrand(iv,ixi) = y2(iv,ixi)*v2_lorentz(iv,ixi,j)
             end do
          end do
          call integrate_vgrid_complex(integrand,v2y2)
       end if 
       df_out(j,:,:) = y2(:,:) - v2y2fac_lorentz(j,:,:)*v2y2
    end do
    call reduce3(real(df_out))
    call reduce3(aimag(df_out))
 
    deallocate(y2, integrand)

  end subroutine conserve_lorentz_coll
  !-------------------------------------------------------------------
  subroutine conserve_en_diff_coll(jmax,y0,df_out)
   use mp, only: iproc, nproc
    implicit none
    complex, dimension(0:,1:,1:):: df_out
    complex, dimension(0:,1:,1:), intent(in):: y0
    integer, intent(in):: jmax
    integer:: blocksize,j_lo,j_hi,j,ixi,iv
    complex, dimension(:,:), allocatable:: integrand, y2
    complex:: v0y0, v1y0, v2y2 

    allocate(integrand(1:Nv_coll,1:Nxi_coll))
    allocate(y2(1:Nv_coll,1:Nxi_coll))

    df_out = 0.0

    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      
    do j=j_lo, j_hi
       is = idx_spec_k(j)

       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             integrand(iv,ixi) = y0(j,iv,ixi)*v0_en_diff(iv,ixi,j) 
          end do
       end do

       call integrate_vgrid_complex(integrand,v0y0)

       do ixi = 1,Nxi_coll
          do iv = 1,Nv_coll
             integrand(iv,ixi) = y0(j,iv,ixi)*v1_en_diff(iv,ixi,j) 
          end do
       end do

       call integrate_vgrid_complex(integrand,v1y0)

       if (is .EQ. 1) then
          y2(:,:) = y0(j,:,:) - v0y0fac_en_diff(j,:,:) * v0y0 - v1y0fac_en_diff(j,:,:) * v1y0
          v2y2 = 0.0
       else if (is .EQ. 2) then
          y2(:,:) = y0(j,:,:) - v0y0fac_en_diff(j,:,:) * v0y0 - v1y0fac_en_diff(j,:,:) * v1y0

          do ixi = 1,Nxi_coll
             do iv = 1,Nv_coll
                integrand(iv,ixi) = y2(iv,ixi)*v2_en_diff(iv,ixi,j) 
             end do
          end do

          call integrate_vgrid_complex(integrand,v2y2)
       end if 
       df_out(j,:,:) = y2(:,:) - v2y2fac_en_diff(j,:,:)*v2y2
    end do

    call reduce3(real(df_out))
    call reduce3(aimag(df_out))
 
    deallocate(y2, integrand)

  end subroutine conserve_en_diff_coll
  !-------------------------------------------------------------------
  subroutine apply_lorentz_coll(delta_t,jmax,df_in,df_out)
   use mp, only: iproc, nproc
    implicit none
    integer, intent(in):: jmax
    real, intent(in):: delta_t
    complex, dimension(0:,1:,1:):: df_in
    complex, dimension(0:,1:,1:):: df_out
    integer:: blocksize,j_lo,j_hi,iE,is,j,num_xi,iv,ji,iv_i,ixi
    complex:: upari
    real, dimension(:), allocatable:: gam
    real, dimension(:,:), allocatable:: vpar
    complex, dimension(:,:), allocatable:: integrand

    if (num_species .GT. 1) then 
       allocate(integrand(1:Nv_coll,1:Nxi_coll))
    end if 

    df_out = 0.0
    !
    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      
    do j=j_lo, j_hi
       is = idx_spec_k (j) !; if (is /= 1) write (*,*) 'oops'
       do iv=1,Nv_coll
          ! Add const. C(ei) term to electrons
          if (is .EQ. 2) then
             ikx = idx_ikx(j)
             iky = idx_iky(j)
             iz = idx_iz(j)
             ji = idx_j_k(ikx,iky,iz,1)
             if ((is .EQ. 1) .AND. (ji .NE. j)) write(*,*) "There's a problem with j indexing!"
                  
             if (coll_ei_on) then
                do ixi=1,Nxi_coll
                   do iv_i = 1,Nv_coll
                      integrand(iv_i,ixi) = J0_coll(ji,iv_i,ixi) * vpar_coll(iv_i,ixi) *df_in(ji,iv_i,ixi) * F0_coll(iv_i)
                   end do
                end do
                call integrate_vgrid_complex(integrand,upari)
             else
                upari = 0.0
             end if
             df_in(j,iv,:) = df_in(j,iv,:) + delta_t*(vts(1)/vts(2))*nu_Dei_grid(iv,is)*vpar_coll(iv,:)*J0_coll(j,iv,:)*upari


          end if
          call tridiag_solve_complex(Nxi_coll,delta_t*AA_xi(:,iv,is),1.0+delta_t*BB_xi(:,iv,j),delta_t*CC_xi(:,iv,is),df_in(j,iv,:),df_out(j,iv,:))
       end do
    end do
    ! call sum_allreduce (df_new)
    call reduce3 (real(df_out))
    call reduce3 (aimag(df_out))

    if (num_species .GT. 1) deallocate(integrand)

  end subroutine apply_lorentz_coll
  !-------------------------------------------------------------------
  subroutine invfft_df_coll(df_in_k, df_out_x)
    use fft_work, only: fftw_backward
    use mp, only:iproc,nproc
    implicit none
    real,dimension(0:,1:,1:),intent(inout):: df_out_x
    complex,dimension(0:,1:,1:),intent(in):: df_in_k
    real, dimension(:,:), allocatable:: out_temp
    complex, dimension(:,:), allocatable:: in_temp
    integer:: j,j_lo,j_hi,k,blocksize,k_lo,k_hi,Nj_x,jsum,Nj_k,iv,ixi,ix,iy,iz
    integer*8,save:: fft_plan
    logical, save:: first = .true.
    real:: J0

    allocate(in_temp(0:Nx/2,0:Ny-1))
    allocate(out_temp(0:Nx-1,0:Ny-1))

    if (first) then
       call rfftwnd_f77_create_plan (fft_plan,2,Msize(1:2),fftw_backward,0)
       first = .false.
    end if

    df_out_x = 0.0

!    Nj_x = Nx*Ny
!    Nj_k = ( (Nx/2) + 1 )*Ny

!    blocksize = (Nz*num_species)/nproc
!    k_lo = blocksize*iproc
!    k_hi = min(Nz*num_species-1,k_lo+blocksize-1)
!    if (iproc .EQ. (nproc-1)) k_hi = Nz*num_species-1

!    do k = k_lo, k_hi
     do is = 1,num_species
     do iz = 0,Nz-1
       do ixi = 1,Nxi_coll 
          do iv = 1,Nv_coll

!             j = Nj_k * k
             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                   j = idx_j_k (ikx, iky, iz, is)
                   in_temp(ikx,iky) =df_in_k(j,iv,ixi) 
!                   j = j+1
                end do
             end do

             ! Single out one k if the option is chosen
!             if (spectrum_diagnostics_on) then
!                if (init_kx .GT. -1000) then 
!                   in_temp(0:init_kx-1,:) = 0.0
!                   in_temp(init_kx+1:Nx/2,:) = 0.0
!                end if
!                if (init_ky .GT. -1000) then 
!                   in_temp(:,0:init_ky-1) = 0.0
!                   in_temp(:,init_ky+1:Ny-init_ky-1) = 0.0
!                   in_temp(:,Ny-init_ky+1 : Ny-1) = 0.0
!                end if
!             end if

             call rfftwnd_f77_one_complex_to_real (fft_plan, in_temp, out_temp) 


!             j =Nj_x * k
             do iy = 0,Ny-1
                do ix = 0,Nx-1
                   j = idx_j (ix, iy, iz, is)
                   df_out_x(j,iv,ixi) = out_temp(ix,iy)/real(Nx*Ny)
!                   j = j+1
                end do
             end do

          end do
       end do
    end do
    end do
!    call reduce3(df_out_x)

    deallocate(in_temp,out_temp)
  end subroutine invfft_df_coll
  !-------------------------------------------------------------------
  subroutine fft_df_coll(df_in_x, df_out_k)
    use fft_work, only:fftw_forward
    use mp, only:iproc,nproc
    implicit none
    real,dimension(0:,1:,1:), intent(in):: df_in_x
    complex,dimension(0:,1:,1:),intent(inout):: df_out_k
    real, dimension(:,:), allocatable:: in_temp
    complex, dimension(:,:), allocatable:: out_temp
    integer:: j,j_lo,j_hi,k,blocksize,k_lo,k_hi,Nj_x,jsum,iv,ixi,Nj_k,ix,iy,iz
    integer*8, save:: fft_plan
    logical, save:: first = .true.
    real:: J0
    complex, dimension(:,:,:), allocatable:: integrand_n, integrand_u, integrand_E

    allocate(in_temp(0:Nx-1,0:Ny-1))
    allocate(out_temp(0:Nx/2,0:Ny-1))

    if (first) then
       call rfftwnd_f77_create_plan (fft_plan,2,Msize(1:2),fftw_forward,0) 
       first = .false.
    end if

    df_out_k = 0.0

!    Nj_x = Nx*Ny
!    Nj_k = ( (Nx/2) + 1 )*Ny

!    blocksize = (Nz*num_species)/nproc
!    k_lo = blocksize*iproc
!    k_hi = min(Nz*num_species-1,k_lo+blocksize-1)
!    if (iproc .EQ. (nproc-1)) k_hi = Nz*num_species-1

!    do k = k_lo, k_hi
     do is = 1,num_species
     do iz = 0,Nz-1
       do ixi = 1,Nxi_coll 
          do iv = 1,Nv_coll

!             j =Nj_x * k
             do iy = 0,Ny-1
                do ix = 0,Nx-1
                   j = idx_j (ix, iy, iz, is)
                   in_temp(ix,iy) = df_in_x(j,iv,ixi)
!                   j = j+1
                end do
             end do

             call fill_grid_2D_real(Nx,Ny,in_temp) 

             call rfftwnd_f77_one_real_to_complex (fft_plan, in_temp, out_temp) 

             ! Single out one k if the option is chosen
!             if (spectrum_diagnostics_on) then
!                if (init_kx .GT. -1000) then 
!                   out_temp(0:init_kx-1,:) = 0.0
!                   out_temp(init_kx+1:Nx/2,:) = 0.0
!                end if
!                if (init_ky .GT. -1000) then 
!                   out_temp(:,0:init_ky-1) = 0.0
!                   out_temp(:,init_ky+1:Ny-init_ky-1) = 0.0
!                   out_temp(:,Ny-init_ky+1 : Ny-1) = 0.0
!                end if
!             end if

!             j = Nj_k * k
             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                   j = idx_j_k (ikx, iky, iz, is)
                   df_out_k(j,iv,ixi) = out_temp(ikx,iky) 
!                   j = j+1
                end do
             end do 
          end do
       end do
    end do
    end do
!    call reduce3(real(df_out_k))
!    call reduce3(aimag(df_out_k))

 
    deallocate(in_temp,out_temp)
  end subroutine fft_df_coll
  !-------------------------------------------------------------------
  subroutine conserve_diagnostics(collision_count,df)
    ! Assumes a 1x1x1 grid 
    implicit none
    integer, intent(in):: collision_count
    real,dimension(1:,1:), intent(in):: df
    real, dimension(:,:),allocatable:: integrand_n, integrand_u, integrand_E
    real:: dn,upar,dE
    real, save:: dn_init,upar_init,dE_init
    logical,save:: first = .true.
    integer:: iv,ixi

    allocate(integrand_n(Nv_coll,Nxi_coll))
    allocate(integrand_u(Nv_coll,Nxi_coll))
    allocate(integrand_E(Nv_coll,Nxi_coll))
!open(unit=99,file="test.out", status="replace")
    do ixi = 1, Nxi_coll
       do iv = 1,Nv_coll
! write(99,*) vpar_coll(iv,ixi), vperp_coll(iv,ixi), df(iv,ixi)
          integrand_n(iv,ixi) = df(iv,ixi)*F0_coll(iv)
          integrand_u(iv,ixi) = df(iv,ixi)*vpar_coll(iv,ixi)*F0_coll(iv)
          integrand_E(iv,ixi) = df(iv,ixi)*v_grid(iv)**2*F0_coll(iv)
       end do
    end do
!close(99)
!stop
!    write(*,*) "Here."
!    write(*,*) integrand_n(1,1), integrand_u(2,2), integrand_E(3,1)

    call integrate_vgrid_real(integrand_n,dn)
    call integrate_vgrid_real(integrand_u,upar)
    call integrate_vgrid_real(integrand_E,dE)

    if (first) then
       dn_init = dn
       upar_init = upar
       dE_init = dE
       first = .false.
    end if

    open(unit=conserve_unit, file=conserve_str,status="old", access='append')
!    write(*,*) time,dn/dn_init, upar/upar_init, dE/dE_init
    write(conserve_unit,*) time,dn/dn_init, upar/upar_init, dE/dE_init
    close(conserve_unit)

    deallocate(integrand_n,integrand_u,integrand_E)

  end subroutine conserve_diagnostics
  !-------------------------------------------------------------------
  subroutine calculate_phi_coll(h,jmax,phi_coll)
    implicit none
    complex, dimension(0:,1:,1:), intent(in):: h
    integer, intent(in):: jmax
    complex, dimension(0:,0:,0:), intent(inout):: phi_coll
    real, save:: denominator
    complex:: temp_integral_c
    integer:: blocksize,j_lo,j_hi,ikx,iky,iz,is, iv,ixi
    logical, save:: first = .true.
    complex, dimension(:,:), allocatable:: integrand
  
    allocate(integrand(1:Nv_coll,1:Nxi_coll))

    if(first) then
       denominator = 0.0
       do is = 1,num_species
          denominator = denominator + Zs(is)**2*density(is)/temperature(is)
       end do
       first = .false.
    end if
 
    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      
    phi_coll = 0.0
 
    do j=j_lo, j_hi
       ikx = idx_ikx(j)
       iky = idx_iky(j)
       iz = idx_iz(j)
       is = idx_spec_k(j)
       do ixi = 1,Nxi_coll
         do iv = 1,Nv_coll
            integrand = F0_coll(iv)*J0_coll(j,iv,ixi)*h(j,iv,ixi)
         end do
       end do
       call integrate_vgrid_complex(integrand,temp_integral_c)
       phi_coll(ikx,iky,iz) = phi_coll(ikx,iky,iz) + Zs(is)*temp_integral_c
    end do

    call reduce3(real(phi_coll))
    call reduce3(aimag(phi_coll))

    phi_coll = phi_coll / denominator

    deallocate(integrand)
  end subroutine calculate_phi_coll
  !-------------------------------------------------------------------
  subroutine convert_df_h(w,jmax,sgn)
    use mp, only:iproc,nproc
    complex, dimension(0:,1:,1:):: w
    integer, intent(in):: sgn,jmax
    integer:: blocksize,j_lo,j_hi,ikx,iky,iz,is

    blocksize = (jmax+1)/nproc
    j_lo = blocksize*iproc
    j_hi = min(jmax,j_lo+blocksize-1)
    if (iproc .EQ. (nproc-1)) j_hi = jmax
    !      

    do j=j_lo, j_hi
       ikx = idx_ikx(j)
       iky = idx_iky(j)
       iz = idx_iz(j)
       is = idx_spec_k(j)
       w(j,:,:) = w(j,:,:) + real(sgn)*Zs(is)*phi(ikx,iky,iz)*J0_coll(j,:,:)/temperature(is)
    end do

    call reduce3(real(w))
    call reduce3(aimag(w))

  end subroutine
  !-------------------------------------------------------------------
  subroutine grid2particle_coll(pitch_angle_coll_on,gamma,xi_int,df_old,df_new,x_gc,y_gc,z_gc,weight)
    implicit none
    logical, intent(in):: pitch_angle_coll_on
    integer, dimension(1:,1:), intent(in):: xi_int
    real, dimension(0:,1:,1:), intent(in):: df_old, df_new
    real, intent(in):: gamma
    real, dimension(1:,1:), intent(in):: x_gc,y_gc,z_gc
    real, dimension(1:,1:), intent(inout):: weight
   
    integer:: is, idx,imu,NE,k,iv,ixi,L0,M0,N0,L1,M1,N1,L2,M2,N2
    real:: xl0,xl1,xgc,ym0,ym1,ygc,zn0,zn1,zn2,zgc,tmp


    if (pitch_angle_coll_on) then ! use the old and new df to smooth out sharp features
       do is = 1,num_species
          idx=0
          do imu=1,Nmu
             NE = NE_mu(imu)
             do k=1,NE
                !
                idx = idx + 1
                iv = v_int(idx,is)
                ixi = xi_int(idx,is)
                !
                L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
                M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
                N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1
                !
                xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
                ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
                zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
                !
                L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny)  ;  N2 = mod(N1, Nz)
                !
                tmp = coll_dist (df_old, df_new, weight(idx,is), gamma, Volume_scale, &
                     xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, iv,ixi, L0, L2, M0, M2, N0, N2, is)
                ! This line could be done on the GPU
                weight(idx,is) = (1.-gamma)*weight(idx,is) + tmp
                !
             end do
          end do
       end do
    else
       ! coarse-graining, simply interpolate back onto marker particles without using df_new
       do is = 1,num_species
          idx=0
          do imu=1,Nmu
             NE = NE_mu(imu)
             do k=1,NE
                !
                idx = idx + 1
                iv = v_int(idx,is)
                ixi = xi_int(idx,is)
                !
                L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
                M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
                N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1
                !
                xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
                ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
                zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
                !
                L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)
                !
                tmp = dist(df_old, xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, iv,ixi, L0, L2, M0, M2, N0, N2, is, Volume_scale)
                !
                weight(idx,is) = (1.-gamma)*weight(idx,is) + tmp * gamma
                !
             end do
          end do
       end do
    end if
    !
  end subroutine grid2particle_coll
  !-------------------------------------------------------------------
  subroutine collision (delta_t, weight, x_gc, y_gc, z_gc, En_gc, vz_gc)
    use mp     
    implicit none
    ! Linearized conserving gyrokinetic collision operator from Abel, Barnes, et al (2009)
    !
    real, dimension(1:,1:), intent (inout) :: weight
    real, intent(in) :: delta_t
    real, dimension(1:,1:), intent (in) :: x_gc
    real, dimension(1:,1:), intent (in) :: y_gc
    real, dimension(1:,1:), intent (in) :: z_gc
    real, dimension(1:,1:), intent (in) :: En_gc, vz_gc
    integer*8, save :: plan_real2compl
    !
    real, dimension(:,:,:), allocatable :: dfgrid_old, dfgrid_new
    real, dimension(:,:), allocatable:: delta_f_dia
    complex, dimension(:,:,:), allocatable:: dfgrid_k_1, dfgrid_k_2
    !
    logical, save  :: first_pitch_angle_collision =.TRUE.
    logical, save :: beginning = .TRUE.
    !
    integer, save :: collision_count = 0
    integer:: iv,ixi
    !
    real :: cg_fac
  
    integer:: jmax, jmax_k
    integer, dimension(:,:), allocatable:: xi_int
    !
    jmax = num_species*Nx*Ny*Nz - 1
    jmax_k = num_species*((Nx/2)+1)*Ny*Nz - 1
  
    allocate (dfgrid_old(0:jmax,1:Nv_coll,1:Nxi_coll));       dfgrid_old = 0.0
    allocate (dfgrid_new(0:jmax,1:Nv_coll,1:Nxi_coll));       dfgrid_new = 0.0
    allocate (dfgrid_k_1(0:jmax_k,1:Nv_coll, 1:Nxi_coll));    dfgrid_k_1 = 0.0
    allocate (dfgrid_k_2(0:jmax_k,1:Nv_coll, 1:Nxi_coll));    dfgrid_k_2 = 0.0
    allocate (xi_int(1:Npart,1:num_species))

    !
    collision_count = collision_count + 1
    if (proc0) write(*,*) "collision_count = ", collision_count
    !
    call particle2grid_coll(collision_count,x_gc,y_gc,z_gc,En_gc,vz_gc,weight,v_int,xi_int,jmax,dfgrid_old)
!write(*,*) dfgrid_old

    call fft_df_coll(dfgrid_old,dfgrid_k_1)

    if (h_convert_im) call convert_df_h(dfgrid_k_1,jmax_k,+1)

    ! Velocity-space diagnostics
    if( (conserve_diagnostics_on .OR. vspace_coll_diagnostics_on) .AND. MOD(step,vspace_coll_diagnostics_time)==0 .AND. (.NOT.(halfstep_coll_on) .OR. (MOD(collision_count,2)==0)) .and. proc0) then
       allocate(delta_f_dia(1:Nv_coll,1:Nxi_coll))
       do iv=1,Nv_coll
          do ixi=1,Nxi_coll
             j = idx_j_k (v_dia_x, v_dia_y, v_dia_z, is_dia)
             delta_f_dia(iv,ixi) = dfgrid_old(j,iv,ixi)
          end do
       end do
       if (conserve_diagnostics_on) call conserve_diagnostics(collision_count,delta_f_dia(:,:))
       if (vspace_coll_diagnostics_on) call vspace_coll_diagnostics(delta_f_dia(:,:))
       deallocate(delta_f_dia)
    end if

    if (pitch_angle_coll_on) then
       call apply_lorentz_coll(delta_t,jmax_k,dfgrid_k_1,dfgrid_k_2)
       if (conserve_coll_sm_on) then
          call conserve_lorentz_coll(jmax_k,dfgrid_k_2,dfgrid_k_1)
       else
          dfgrid_k_1 = dfgrid_k_2
       end if
    end if

    if (energy_diff_coll_on) then
       call apply_en_diff_coll(delta_t,jmax_k,dfgrid_k_1,dfgrid_k_2)
       if (conserve_coll_sm_on) then
          call conserve_en_diff_coll(jmax_k,dfgrid_k_2,dfgrid_k_1)
       else
          dfgrid_k_1 = dfgrid_k_2
       end if
    end if
  
    if (h_convert_im) call convert_df_h(dfgrid_k_1,jmax_k,-1)

    call invfft_df_coll(dfgrid_k_1,dfgrid_new)

    call grid2particle_coll(pitch_angle_coll_on,gamma,xi_int,dfgrid_old,dfgrid_new,x_gc,y_gc,z_gc, weight)
    !
    deallocate(dfgrid_old, dfgrid_new, dfgrid_k_1, dfgrid_k_2, xi_int)

  end subroutine collision

  subroutine integrate_vgrid_real(f,int_f)
  ! Integrates over velocity space in v-xi coordinates. Includes integration over angle theta (assumed homogeneous - contributes factor of 2pi)
    implicit none
    real, dimension(1:,1:), intent(in):: f
    real, intent(inout):: int_f
    real:: int_f_xi
    integer:: iv, ixi


    int_f = 0.0
    do iv = 1,Nv_coll
       int_f_xi = 0.0
       do ixi = 1,Nxi_coll
          int_f_xi = int_f_xi + f(iv,ixi)*sin(xi_grid(ixi))*dxi
       end do
       int_f = int_f + int_f_xi*v_grid(iv)**2*dv
    end do

    int_f = int_f*2.0*pi
     
  end subroutine integrate_vgrid_real

  subroutine integrate_vgrid_complex(f,int_f)
  ! Integrates over velocity space in v-xi coordinates. Includes integration over angle theta (assumed homogeneous - contributes factor of 2pi)
    implicit none
    complex, dimension(1:,1:), intent(in):: f
    complex, intent(inout):: int_f
    complex:: int_f_xi
    integer:: iv, ixi

    int_f = 0.0
    do ixi = 1,Nxi_coll
       int_f_xi = 0.0
       do iv = 1,Nv_coll
          int_f_xi = int_f_xi + f(iv,ixi)*v_grid(iv)**2*dv
       end do
       int_f = int_f + int_f_xi*sin(xi_grid(ixi))*dxi
    end do

    int_f = int_f*2.0*pi
     
  end subroutine integrate_vgrid_complex

  subroutine tridiag_solve_complex(N,a,b,c,y,x)
  ! Solves Ax=y, where A is a tridiagonal NxN matrix defined by a,b,c vectors
    implicit none
    integer, intent(in):: N
    real, dimension(1:), intent(in):: a,b, c
    complex, dimension(1:), intent(in):: y
    complex, dimension(1:),intent(inout):: x
    real, dimension(:), allocatable:: gam
    real:: m
    integer:: k

    allocate(gam(N))

    x(1) = y(1)
    gam(1) = b(1)
    do k = 2,N
       m = a(k)/gam(k-1)
       gam(k) = b(k) - m * c(k-1)
       x(k) = y(k) - m * x(k-1)
    end do
   
    x(N) = x(N)/gam(N)
    do k=N-1,1,-1
       if (gam(k) .EQ. 0.0) then
          write(*,*) "ERROR: Tridiag solver broken!"
          write(*,*) "a = ", a(:)
          write(*,*) "b = ", b(:)
          write(*,*) "c = ", c(:)
          write(*,*) "y = ", y(:)
          stop
       end if
       x(k) = ( x(k) - c(k)*x(k+1) )/gam(k)
    end do

    deallocate(gam)

  end subroutine tridiag_solve_complex

  subroutine tridiag_solve_real(N,a,b,c,y,x)
  ! Solves Ax=y, where A is a tridiagonal NxN matrix defined by a,b,c vectors
    implicit none
    integer, intent(in):: N
    real, dimension(1:), intent(in):: a,b, c
    real, dimension(1:), intent(in):: y
    real, dimension(1:), intent(inout):: x
    real, dimension(:), allocatable:: gam
    real:: m
    integer:: k

    allocate(gam(N))

    x(1) = y(1)
    gam(1) = b(1)
    do k = 2,N
       m = a(k)/gam(k-1)
       gam(k) = b(k) - m * c(k-1)
       x(k) = y(k) - m * x(k-1)
    end do
   
    x(N) = x(N)/gam(N)
    do k=N-1,1,-1
       if (gam(k) .EQ. 0.0) then
          write(*,*) "ERROR: Tridiag solver broken!"
          write(*,*) "a = ", a(:)
          write(*,*) "b = ", b(:)
          write(*,*) "c = ", c(:)
          write(*,*) "y = ", y(:)
          stop
       end if
       x(k) = ( x(k) - c(k)*x(k+1) )/gam(k)
    end do

    deallocate(gam)

  end subroutine tridiag_solve_real

  function idx_E (j)
    ! Determine the energy index.
    implicit none
    integer :: idx_E
    integer, intent (in) :: j

    idx_E = 1 + mod (j/(Nx*Ny*Nz), Number_E)
    !    idx_E = 1 + j/(Nx*Ny*Nz)

  end function idx_E
  !-------------------------------------------------------------------
  function idx_spec (j)
    ! Determine the species index.
    implicit none
    integer :: idx_spec
    integer, intent (in) :: j
    !
    idx_spec = 1 + j/(Nx*Ny*Nz)
    !
  end function idx_spec
  !-------------------------------------------------------------------
  function idx_spec_k (j)
    ! Determine the species index.
    implicit none
    integer :: idx_spec_k
    integer, intent (in) :: j
    !
    idx_spec_k = 1+  j/(((Nx/2)+1)*Ny*Nz)
    !
  end function idx_spec_k
  !-------------------------------------------------------------------
  function idx_j (ix, iy, iz, is)
    ! Determine the grid location in a 5D grid that has been
    ! collapsed into 2D array.  
    implicit none
    integer :: idx_j
    integer, intent (in) :: ix, iy, iz,is
    !
    idx_j = ix + Nx*(iy + Ny*(iz + Nz*(is-1)))
    !
  end function idx_j
  !-------------------------------------------------------------------
  function idx_j_k (ix, iy, iz, is)
    ! Determine the grid location in a 5D grid that has been
    ! collapsed into 2D array.  
    implicit none
    integer :: idx_j_k
    integer, intent (in) :: ix, iy, iz, is
    !
    idx_j_k = ix + ((Nx/2)+1)*(iy + Ny*(iz + Nz*(is-1)))
    !
  end function idx_j_k
  !-------------------------------------------------------------------
  function idx_ikx (j)
    implicit none
    integer :: idx_ikx
    integer, intent (in) :: j
    !
    idx_ikx = modulo(j,(Nx/2)+1)
    !
  end function idx_ikx
  !-------------------------------------------------------------------
  function idx_iky (j)
    implicit none
    integer :: idx_iky
    integer, intent (in) :: j
    !
    idx_iky = modulo(j,((Nx/2)+1)*Ny)/((Nx/2)+1)
    !
  end function idx_iky
  !-------------------------------------------------------------------
  function idx_iz (j)
    implicit none
    integer :: idx_iz
    integer, intent (in) :: j
    !
    idx_iz = modulo(j,((Nx/2)+1)*Ny*Nz)/(Ny*((Nx/2)+1))
    !
  end function idx_iz
  !-------------------------------------------------------------------
  function coll_dist (old_dist, new_dist, wgt, gamma, Vol, &
       xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, iv,ixi, L0, L2, M0, M2, N0, N2, is)
    ! Find the new value of the distribution function at the marker positions
    ! using both the new and old values of the distribution function after 
    ! the collision operator is applied.
    implicit none
    real  :: coll_dist
    real , dimension (0:,1:,1:), intent(in) :: old_dist, new_dist
    real , intent(in) :: wgt, gamma, Vol
    real , intent(in) :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc
    real  :: tmp
    !
    integer, intent(in) :: L0, L2, M0, M2, N0, N2, iv,ixi, is
    integer :: j
    !
    tmp = 0.
    !
    j = idx_j(L0, M0, N0, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xl1, xgc, ym1, ygc, zn1, zgc, gamma)
    !
    j = idx_j(L2, M0, N0, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xgc, xl0, ym1, ygc, zn1, zgc, gamma)
    !
    j = idx_j(L0, M2, N0, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xl1, xgc, ygc, ym0, zn1, zgc, gamma)
    !
    j = idx_j(L2, M2, N0, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xgc, xl0, ygc, ym0, zn1, zgc, gamma)
    !
    j = idx_j(L0, M0, N2, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xl1, xgc, ym1, ygc, zgc, zn0, gamma)
    !
    j = idx_j(L2, M0, N2, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xgc, xl0, ym1, ygc, zgc, zn0, gamma)
    !
    j = idx_j(L0, M2, N2, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xl1, xgc, ygc, ym0, zgc, zn0, gamma)
    !
    j = idx_j(L2, M2, N2, is)
    call accum_part (tmp, old_dist(j,iv,ixi), new_dist(j,iv,ixi), wgt, xgc, xl0, ygc, ym0, zgc, zn0, gamma)
    !
    coll_dist = tmp * Vol
    !
  end function coll_dist
  !-------------------------------------------------------------------
  subroutine accum_part (fsum, field_old, field_new, wgt, xr, xl, yr, yl, zr, zl, gamma)
    !
    real, intent(in) :: field_old, field_new, wgt, xr, xl, yr, yl, zr, zl, gamma
    real, intent(inout) :: fsum
    !
    fsum = fsum + (xr-xl)*(yr-yl)*(zr-zl)*(field_new + (gamma-1.)*field_old)
    !
  end subroutine accum_part
  !-------------------------------------------------------------------
  function dist (old_dist, xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc,iv,ixi, L0, L2, M0, M2, N0, N2, is, Vol)
    ! Find the new value of the distribution function at the marker positions
    ! using simple coarse-graining instead of a collision operator. 
    implicit none
    real  :: dist
    real , dimension (0:,1:,1:), intent(in) :: old_dist
    real , intent(in) :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, Vol
    real  :: tmp
    !
    integer, intent(in) :: L0, L2, M0, M2, N0, N2,ixi,iv, is
    integer :: j
    !
    tmp = 0.
    !
    j = idx_j (L0, M0, N0, is)
    call accum (tmp, old_dist(j,iv,ixi), xl1, xgc, ym1, ygc, zn1, zgc)
    !
    j = idx_j (L2, M0, N0, is)
    call accum (tmp, old_dist(j,iv,ixi), xgc, xl0, ym1, ygc, zn1, zgc)
    !
    j = idx_j (L0, M2, N0, is)
    call accum (tmp, old_dist(j,iv,ixi), xl1, xgc, ygc, ym0, zn1, zgc)
    !
    j = idx_j (L2, M2, N0, is)
    call accum (tmp, old_dist(j,iv,ixi), xgc, xl0, ygc, ym0, zn1, zgc)
    !
    j = idx_j (L0, M0, N2, is)
    call accum (tmp, old_dist(j,iv,ixi), xl1, xgc, ym1, ygc, zgc, zn0)
    !
    j = idx_j (L2, M0, N2, is)
    call accum (tmp, old_dist(j,iv,ixi), xgc, xl0, ym1, ygc, zgc, zn0)
    !
    j = idx_j (L0, M2, N2, is)
    call accum (tmp, old_dist(j,iv,ixi), xl1, xgc, ygc, ym0, zgc, zn0)
    !
    j = idx_j (L2, M2, N2, is)
    call accum (tmp, old_dist(j,iv,ixi), xgc, xl0, ygc, ym0, zgc, zn0)
    !
    dist = tmp * Vol
    
  end function dist
  !-------------------------------------------------------------------
  subroutine init_J0()
    !
    ! Choice between the full J0 function and a truncated version.
    ! The full function is the double precision version from the special
    ! functions library module.
    !
    ! <doc> Using j0_function instead. Code won't compile as is with j0_func in spfunc.fpp 
    ! This should be fixed. </doc>
    ! use spfunc, only: j0_func => j0
    !
    implicit none
    !
    real :: J0_temp, argNorm,B0inv
    integer:: jmax,j,ixi,iv
    logical:: j0_test = .false.

    j0_test = .false.
  
    if (j0_test .AND. proc0) open(unit=j0_unit,file=j0_str,status="replace")

    do is = 1,num_species
       argNorm = sqrt(2.0)*vts(is)*mass(is)/abs(Zs(is)) 
       do imu = 1,Nmu
          do iz = 0,Nz-1
             B0inv = 1.0/B0_grid(iz)
             do iky = 0,Ny-1
                do ikx = 0,Nx/2
! Fix this to include proper shape function
                   call j0_function(argNorm*sqrt(k2_perp_shaped(ikx,iky)*mu_grid(imu)*B0inv),J0_temp)
                   J0(ikx,iky,iz,imu,is) = J0_temp
                   if (j0_test .AND. proc0) write(j0_unit,*) argNorm*sqrt(k2_perp_shaped(ikx,iky)**2*mu_grid(imu)*B0inv),J0_temp
                end do
             end do
          end do
       end do
    end do 

    if (j0_test .AND. proc0) close(j0_unit) 
    !
  end subroutine init_J0
  !-------------------------------------------------------------------
  subroutine init_J1()
    !
    ! Choice between the full J0 function and a truncated version.
    ! The full function is the double precision version from the special
    ! functions library module.
    !
    ! <doc> Using j0_function instead. Code won't compile as is with j0_func in spfunc.fpp 
    ! This should be fixed. </doc>
    ! use spfunc, only: j0_func => j0
    !
    implicit none
    !
    real :: J1_temp, argNorm,B0inv
    integer:: jmax,j,ixi,iv

    do is = 1,num_species
       argNorm = sqrt(2.0)*vts(is)*mass(is)/abs(Zs(is)) 
       do imu = 1,Nmu
          do iz = 0,Nz-1
             B0inv = 1.0/B0_grid(iz)
             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                   call j1_function(argNorm*sqrt(k2_perp(ikx,iky)*mu_grid(imu)*B0inv),J1_temp)
                   J1(ikx,iky,iz,imu,is) = J1_temp
                end do
             end do
          end do
       end do
    end do 

    !
  end subroutine init_J1

  subroutine init_particles(in_seed)
    !
    ! Place partices in the simulation domain at random positions in
    ! configuration space.  With 4 properties (x,y,z,weight), a 
    ! permutation across processors with period 4 will correct for the 
    ! non-uniqueness introduced by the parallelization.
    !
    use mp
    !
    implicit none
    integer :: idate(8), isize
    integer, allocatable, dimension(:) :: iseed
    integer, intent(in), optional:: in_seed
    integer :: i, j, k, l, idx, Np, add_seed
    !
    real :: tempran
   
    if (present(in_seed)) then
       add_seed = in_seed
    else
       add_seed = 0
    end if

    ! need to seed the first call to random number with a seed that depends on the variable iproc (integer)
    call date_and_time (values=idate)
    call random_seed  (size=isize)
    allocate (iseed(isize))
    call random_seed (get=iseed)
    !
    iseed = iseed + (iproc - nproc) * (idate(1) - 500) + add_seed
    call random_seed (put=iseed)
write(*,*) "idate(1) = ", idate(1)
    !
    select case (mod(iproc,4))
    case (0)
       call random_number (p(wgt,:,:))  ;  p(wgt,:,:) = (p(wgt,:,:) - 0.5) *2.0* init_wgt_mag
       call random_number (p(x,:,:))  ;  p(x,:,:) = p(x,:,:) * Lx
       call random_number (p(y,:,:))  ;  p(y,:,:) = p(y,:,:) * Ly
       call random_number (p(z,:,:)) 
    case (1)
       call random_number (p(x,:,:))  ;  p(x,:,:) = p(x,:,:) * Lx
       call random_number (p(y,:,:))  ;  p(y,:,:) = p(y,:,:) * Ly
       call random_number (p(z,:,:)) 
       call random_number (p(wgt,:,:))  ;  p(wgt,:,:) = (p(wgt,:,:) - 0.5) * 2.0*init_wgt_mag
    case (2)
       call random_number (p(y,:,:))  ;  p(y,:,:) = p(y,:,:) * Ly
       call random_number (p(z,:,:)) 
       call random_number (p(wgt,:,:))  ;  p(wgt,:,:) = (p(wgt,:,:) - 0.5) * 2.0*init_wgt_mag
       call random_number (p(x,:,:))  ;  p(x,:,:) = p(x,:,:) * Lx
    case (3)
       call random_number (p(z,:,:)) 
       call random_number (p(wgt,:,:))  ;  p(wgt,:,:) = (p(wgt,:,:) - 0.5) * 2.0*init_wgt_mag
       call random_number (p(x,:,:))  ;  p(x,:,:) = p(x,:,:) * Lx
       call random_number (p(y,:,:))  ;  p(y,:,:) = p(y,:,:) * Ly
    end select

       do is = 1, num_species
          idx = 0
          do imu = 1,Nmu
             do iz = 0,Nz-1
                Np = Np_mu_z(iz,imu)
                do j=1,Np
                   idx = idx+1

                   p(z,idx,is) = modulo(p(z,idx,is)*delta_z - 0.5e0*delta_z + real(iz)*delta_z ,Lz) 

                   do while ( mu_grid(imu)*B0(p(z,idx,is)) .GE. Emax ) 
                      call random_number (p(z,idx,is)) 
                      p(z,idx,is) = modulo(p(z,idx,is)*delta_z - 0.5e0*delta_z + real(iz)*delta_z ,Lz) 
                   end do
                end do
             end do
          end do
       end do 
    !
  end subroutine init_particles
  !-------------------------------------------------------------------
  !-------------------------------------------------------------------
  subroutine scramble
    !
    ! Option to scramble marker weights or y positions after a restart. 
    ! Requires scramble = T and init_restart = true, so that the other
    ! marker attributes are set by the previous run. 
    !
    use mp
    !
    implicit none
    integer :: idate(8), isize
    integer, allocatable, dimension(:) :: iseed
    integer :: i, j, k, l, idx
    !
    ! need to seed the first call to random number with a seed that depends on the variable iproc (integer)
    call date_and_time (values=idate)
    call random_seed  (size=isize)
    allocate (iseed(isize))
    call random_seed (get=iseed)
    !
    iseed = iseed + (iproc - nproc) * (idate(8) - 500)
    call random_seed (put=iseed)
    !
    if (scramble_ypos) then
       call random_number (p(y,:,:))  ;  p(y,:,:) = p(y,:,:) * Ly
    end if
    !
    if (scramble_wgt) then
       call random_number (p(wgt,:,:)) ; p(wgt,:,:) =  (p(wgt,:,:) - 0.5) * init_wgt_mag
    end if
    !
  end subroutine scramble
!-------------------------------------------- 
  subroutine init_velocity(in_seed)
     use mp, only: proc0
     implicit none
    integer, intent(in), optional:: in_seed
    integer :: j, k, l, idx, add_seed
    real :: vz_temp,xi,Blocal,tempran,tempE, Temp_use
   
    integer :: idate(8), isize
    integer, allocatable, dimension(:) :: iseed
    real:: erf

    if (present(in_seed)) then
       add_seed = in_seed
    else
       add_seed = 0
    end if

    call date_and_time (values=idate)
    call random_seed  (size=isize)
    allocate (iseed(isize))
    call random_seed (get=iseed)
    !
    iseed = iseed + (iproc - nproc) * (idate(1) - 500) + add_seed
    call random_seed (put=iseed)
    !
    !
    ! For each species, normalize the velocity by the thermal speed of that species.
    do is = 1,num_species
       idx=0
       do i = 1,Nmu
          NE = NE_mu(i)
          do j=1,NE
             idx = idx+1
             Blocal = B0(p(z,idx,is))
             p(En,idx,is) = 0.0
             do while ( ( (p(En,idx,is) - mu_grid(i)*Blocal ) .LT. epsilon(0.0) ) ) ! Avoid stationary particles

                call random_number(tempran)

                if ( abs(T_marker - 1.0) .LE. epsilon(0.0) ) then
                   p(En,idx,is) = mu_grid(i)*Blocal + invErf(tempran*erf(sqrt(Emax - mu_grid(i)*Blocal)))**2
                else if (T_marker .LT. 0.0) then
                   p(En,idx,is) = mu_grid(i)*Blocal + tempran**2*(Emax - mu_grid(i)*Blocal) 
                else
                   p(En,idx,is) = mu_grid(i)*Blocal + T_marker*invErf(tempran*erf(sqrt( (Emax - mu_grid(i)*Blocal)/T_marker)))**2
                end if
 
             end do
             p(vz,idx,is) = sqrt(2.0)*sqrt( p(En,idx,is) - mu_grid(i)*Blocal)
             call random_number(tempran)
             p(vz,idx,is) = p(vz,idx,is) * sign(1.0,tempran-0.5)  ! Randomize sign

             if (T_marker .LT. 0.0) then
                p(wgt_adjust,idx,is) = exp(-p(En,idx,is))
             else if ( abs(T_marker - 1.0) .GE. epsilon(0.0) ) then
                p(wgt_adjust,idx,is) = exp(-p(En,idx,is)* ( 1.0 - (1.0/T_marker)) )
             end if
 
          end do
       end do
    end do

  end subroutine init_velocity
   !-------------------------------------------------------------------
  subroutine gamma0_function(x, gamma0)
    !
    ! A&S, p. 378, 9.8
    implicit none
    real, intent (in) :: x
    real, intent (out) :: gamma0
    real*8, parameter, dimension (7) :: a = (/ 1.00000000, 3.5156229, 3.0899424, 1.2067492, .2659732, .0360768, .0045813 /)
    real*8, parameter, dimension (9) :: b = (/ .39894228, .01328592, .00225319, -.00157565, .00916281, -.02057706, .02635537, -.01647633, .00392377/)
    real*8 :: y
    !
    y = dble(x)/3.75d0
    if (dabs(y) .LE. 1.d0) then
       y = y**2
       gamma0 = real(a(1)+y*(a(2)+y*(a(3)+y*(a(4)+y*(a(5)+y*(a(6)+y*a(7)))))))
       gamma0 = gamma0*exp(-x)
    else if (y .GT. 1.d0) then
       y = 1.d0/y
       gamma0 = real((b(1)+y*(b(2)+y*(b(3)+y*(b(4)+y*(b(5)+y*(b(6)+y*(b(7)+y*(b(8)+y*b(9))))))))) / dsqrt(dble(x)))
    else
       write(*,*) "Error: Argument of Gamma0 should not be negative! Something's wrong."
       stop
    end if
    !
  end subroutine gamma0_function
  !-------------------------------------------------------------------
  subroutine j0_function(x, j0)
    !
    ! A&S, p. 369, 9.4
    implicit none
    real, intent (in) :: x
    real, intent (out) :: j0
    real*8, parameter, dimension (7) :: a = (/ 1.0000000, -2.2499997, 1.2656208, - 0.3163866, .0444479, -0.0039444, 0.0002100 /)
    real*8, parameter, dimension (7) :: b = (/  0.79788456, -0.00000077, -0.00552740, -0.00009512, 0.00137237, - 0.00072805,  0.00014476 /)
    real*8, parameter, dimension (7) :: c = (/ -0.78539816, -0.04166397, -0.00003954,  0.00262573, -0.00054125, -0.00029333,  0.00013558 /)
    real*8 :: y 
    !
!    x = sqrt(x_in)
    if (x <= 3.0) then
       y = dble((x/3.0)**2)
       j0 = real(a(1)+y*(a(2)+y*(a(3)+y*(a(4)+y*(a(5)+y*(a(6)+y*a(7)))))))
    else
       y = dble(3.0/x)
       j0 = real((b(1)+y*(b(2)+y*(b(3)+y*(b(4)+y*(b(5)+y*(b(6)+y*b(7))))))) & 
            *dcos(dble(x)+c(1)+y*(c(2)+y*(c(3)+y*(c(4)+y*(c(5)+y*(c(6)+y*c(7))))))) &
            /dsqrt(dble(x)))
    end if

  end subroutine j0_function
  !-------------------------------------------------------------------
  subroutine j1_function(x, j1)
    !
    ! A&S, p. 369, 9.4
    implicit none
    real, intent (in) :: x
    real, intent (out) :: j1
    real*8, parameter, dimension (7) :: a = (/  0.50000000,-0.56249985, 0.21093573,-0.03954289, 0.00443319,-0.00031761, 0.00001109 /)
    real*8, parameter, dimension (7) :: b = (/  0.79788456, 0.00000156, 0.01659667, 0.00017105,-0.00249511, 0.00113653,-0.00020033 /)
    real*8, parameter, dimension (7) :: c = (/ -2.35619449, 0.12499612, 0.00005650,-0.00637879, 0.00074348, 0.00079824,-0.00029166 /)
    real*8 :: y 
    !
!    x = sqrt(x_in)
    if (x <= 3.0) then
       y = dble((x/3.0)**2)
       j1 = x*real(a(1)+y*(a(2)+y*(a(3)+y*(a(4)+y*(a(5)+y*(a(6)+y*a(7)))))))
    else
       y = dble(3.0/x)
       j1 = real((b(1)+y*(b(2)+y*(b(3)+y*(b(4)+y*(b(5)+y*(b(6)+y*b(7))))))) & 
            *dcos(dble(x)+c(1)+y*(c(2)+y*(c(3)+y*(c(4)+y*(c(5)+y*(c(6)+y*c(7))))))) &
            /dsqrt(dble(x)))
    end if
    !
  end subroutine j1_function
  !-------------------------------------------------------------------
  subroutine gamma_n(x_LL,g0_LL)
    !
    implicit none
    integer k
    real :: tolerance 
    parameter(tolerance=1.0e-16)
    !
    real :: xk,xki,xkp1i,error0
    !
    real, intent (in)  :: x_LL
    real, intent (out) :: g0_LL
    real :: tk0_LL, xb2_LL, xb2sq_LL
    !
    if (x_LL .EQ. 0.) then
       g0_LL=1.
       return
    endif
    !
    xb2_LL=.5*x_LL  
    xb2sq_LL=xb2_LL**2
    tk0_LL=exp(-x_LL)
    g0_LL=tk0_LL
    !
    xk=1.
    !
10  continue 
    !
    xki=1./xk
    xkp1i=1./(xk+1.)
    tk0_LL=tk0_LL*xb2sq_LL*xki*xki
    g0_LL=g0_LL+tk0_LL
    xk=xk+1.
    error0=abs(tk0_LL/g0_LL)
    if (error0 > tolerance) goto 10
    !
  end subroutine gamma_n
  !-------------------------------------------------------------------
  subroutine read_inputs
    ! this section reads in parameter values from an input file
    implicit none
    !
    namelist / grid_par / Nx, Ny, Nz, Nmu, Lx, Ly, Lz, use_mask, delta_t_in, delta_t_min, &
              a_par, a_perp,  gaussquad_on
    !
    namelist / particle_par / Emax, Ncell, T_marker, no_streaming

    namelist / time_par / delta_t_in, time_stop,step_max, CFL_fac, nrelax_req, dtrelax_fac, nrelax_req

    namelist / field_par / local_slope,apar_on,beta,gnrm_on,shape_fcn_on,k0_method, make_poisson_well_posed, use_mask, numerical_denom

    namelist / B0_par / B0mode,B0mag, eps, B0file

    !
    namelist / dia_par / phi_diag_mode,phi_diag_func, phi_diagnostics_on, &
            nout_spectrum,z_trajectory_diagnostics_on, Nparticle_traj, navg_spectrum, &
            nz_diagnostics_on, ispec_traj, particle_diagnostics_on, Nrun_phi_diag, part_write_step, & 
            wgt_diagnostics_on,nout_wgt, phiplot_on, nout_phiplot, phiplot_real, &
            superparticle_conserve, imu_conserve, superparticle_fail_cont, use_actual_df, phiplot_z_on,  &
            phi2_diagnostics_on, phikz_diagnostics_on, aparkz_diagnostics_on, phikx_diagnostics_on, numerical_denom_diagnostics_on
    !
    namelist / dia_old_par / diagnostics_new_on, diagnostics_1_on, diagnostics_2_on, diagnostics_3_on, diagnostics_4_on, nwrite, phi_diagnostics_time, diagnostics_iflux_on,energy_diagnostics_on
    !
    namelist / spec_par / tau, nonlinear_on, ParkerLee, time_growth_off,   &
         scramble_ypos, scramble_wgt, curvature_on, num_species,  & 
         flux_tol_on, flux_diff_tol, flux_tol_step, &
         charge_1, mass_1, temperature_1, density_1, Ln_1, LT_1, &
         charge_2, mass_2, temperature_2, density_2, Ln_2, LT_2, &
         charge_3, mass_3, temperature_3, density_3, Ln_3, LT_3, Rinv,  drift_kinetic

    namelist / init_par / one_k_mode,save_for_restart, init_restart,nsave,init_wgt_mode_r, init_wgt_mode_v, init_v0, init_xi0, init_v_width, init_wgt_mag, init_kx, init_ky,init_kz, exclude_kx, exclude_ky, exclude_kz
    namelist / dia_traj / diagnostics_traj_on, Ndisp, traj_dia_step, num_traj, tracers

    ! 
    namelist / collision_par /  Nxi_coll, Nv_coll,  gamma, time_collision_on, collision_time, collision_on, & 
         pitch_angle_coll_on, nu_coll_1, nu_coll_2, nu_coll_3, krook_operator_on, nu_krook, energy_diff_coll_on, &
         conserve_coll_mc_on,conserve_coll_sm_on, conserve_diagnostics_on, halfstep_coll_on, coll_ei_on, h_convert_im, h_convert_mc, gyrodiff_on
    !
    namelist / vel_dia_par / is_dia, v_dia_x, v_dia_y, v_dia_z, & 
         vspace_coll_diagnostics_time, vspace_coll_diagnostics_on
    !
    call getarg(1, runname)
    in_str = trim(runname) // ".in"
    open(in_unit, file=in_str)
    !
    read(in_unit,particle_par)
    rewind in_unit
    read(in_unit,field_par)
    rewind in_unit
    read(in_unit,time_par)
    rewind in_unit
    read(in_unit,grid_par)
    rewind in_unit
    read(in_unit,B0_par)
    rewind in_unit
    read(in_unit,dia_par)
    rewind in_unit
    read(in_unit,dia_old_par)
    rewind in_unit
    read(in_unit,init_par)
    rewind in_unit
    read(in_unit,spec_par)
    rewind in_unit
    read(in_unit,dia_traj)
    rewind in_unit
    read(in_unit,collision_par)
    rewind in_unit
    read(in_unit,vel_dia_par)
    rewind in_unit
    !
    close(in_unit)
    !
    if (v_dia_x > Nx-1 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*     v_dia_x too big      *'
       write (*,*) '****************************'
    end if
    !
    if (v_dia_y > Ny-1 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*     v_dia_y too big      *'
       write (*,*) '****************************'
    end if
    !
    if (v_dia_z > Nz-1 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*     v_dia_z too big      *'
       write (*,*) '****************************'
    end if
    !
    if (v_dia_x < 0 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*    v_dia_x too small     *'
       write (*,*) '****************************'
    end if
    !
    if (v_dia_y < 0 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*    v_dia_y too small     *'
       write (*,*) '****************************'
    end if
    !
    if (v_dia_z < 0 .AND. proc0) then
       write (*,*) '****************************'
       write (*,*) '*    v_dia_z too small     *'
       write (*,*) '****************************'
    end if
    !
    allocate(mass(1:num_species))
    allocate(temperature(1:num_species))
    allocate(density(1:num_species))
    allocate(Zs(1:num_species))
    allocate(L_T(1:num_species))
    allocate(L_N(1:num_species))
    allocate(nu_coll(1:num_species))
    allocate(vts(1:num_species))
    allocate(epar_fac(1:num_species))
    allocate(nprime(1:num_species))
    allocate(Tprime(1:num_species))
    !

    if(num_species == 1) then 
       mass = mass_1
       temperature = temperature_1
       density = density_1
       Zs = charge_1
       L_T = LT_1
       L_N= Ln_1
       nu_coll = nu_coll_1
    end if
    !
    if(num_species > 1) then
       mass(1) = mass_1
       temperature(1) = temperature_1
       density(1) = density_1
       Zs(1) = charge_1
       L_T(1) = LT_1
       L_N(1)= Ln_1
       nu_coll(1) = nu_coll_1    
       mass(2) = mass_2
       temperature(2) = temperature_2
       density(2) = density_2
       Zs(2) = charge_2
       L_T(2) = LT_2
       L_N(2)= Ln_2
       nu_coll(2) = nu_coll_2   

       ! If user inputs negative number for electron collision frequency, use one scaled by mass 
       if (nu_coll(2) .LT. 0.0) nu_coll(2) = nu_coll(1)*sqrt((temperature(1)/temperature(2))**2*mass(1)/mass(2))*(density(2)/density(1))**2/Zs(1)**2
    end if
    !
    if (num_species > 2) then
       mass(3) = mass_3
       temperature(3) = temperature_3
       density(3) = density_3
       Zs(3) = charge_3
       L_T(3) = LT_3
       L_N(3)= LN_3
       nu_coll(3) = nu_coll_3 
    end if
    !
    vts = sqrt(temperature/mass)
    !
    if (num_species > 3) then
       write(*,*) "ERROR, too many species"
       stop
    end if

    if ( (step_max .LT. 1) .AND. (time_stop .LT. 0.0) ) then
       write(*,*) "ERROR: Either time_stop or step_max must be specified."
       stop
    end if
  !
  ! Termination logic:
  ! If neither step_max or time_stop have been specified, abort code
  ! If only time_stop is specified, step_max is determined from time_stop/delta_t
  ! If only nstep is specified, time_end is determined from nstep*delta_t
  ! If both are specified, code will stop upon reaching the first termination condition.
  if (step_max .LT. 1) step_max = int(time_stop/delta_t_in)
  if (time_stop .LT. 0.0) time_stop =step_max*delta_t_in

    if (phiplot_on .AND. (nout_phiplot .LT. 0)) then
       write(*,*) "ERROR: If phiplot is on, timestep write interval must be specified."
       stop
    end if

    if (Apar_on .AND. (beta .LT. 0.0)) then
       write(*,*) "ERROR: If A_parallel is turned on, beta must be specified."
       stop
    end if

    if (numerical_denom .AND. num_species .LT. 2) then
       write(*,*) "ERROR: numerical_denom option doesn't work for adibatic electrons yet"
       stop
    end if
       
    !
  end subroutine read_inputs
  !-------------------------------------------------------------------
  subroutine setup_restart
    !
    implicit none
    character (14) :: suffix
    !
    restart_unit = 28
    !
    write (suffix,'(a9,i0)') '_restart.', iproc
    restart_str = 'restartdir/' // trim(runname) // suffix
    !
  end subroutine setup_restart
  !-------------------------------------------------------------------
  subroutine setup_traj_IO
    !
    implicit none
    character (8) :: suffix1
    character (9) :: suffix2
    character (9) :: suffix3
    !
    diagnostics_traj_unit = 44
    diagnostics_vtraj_unit = 45
    diag_traj_mom_unit = 46
    !
    write (suffix1,'(a6,i2.2)') '_traj.', iproc
    write (suffix2,'(a7,i2.2)') '_vtraj.', iproc
    write (suffix3,'(a7,i2.2)') '_trmom.', iproc
    !    
    diagnostics_traj_str = trim(runname) // suffix1
    diagnostics_vtraj_str = trim(runname) // suffix2
    diag_traj_mom_str = trim(runname) // suffix3
    !
  end subroutine setup_traj_IO
  !-------------------------------------------------------------------
  subroutine setup_iflux_IO
    !
    implicit none
    character (11) :: suffix1
    character (11) :: suffix2
    character (14) :: suffix3
    !
    diagnostics_ihflux_unit = 74
    diagnostics_ipflux_unit = 75
    diagnostics_iweights2_unit = 76
    !
    write (suffix1,'(a7,i4.4)') '_pflux.', iproc
    write (suffix2,'(a7,i4.4)') '_hflux.', iproc
    write (suffix3,'(a10,i4.4)') '_weights2.', iproc
    !    
    diagnostics_ihflux_str = trim(runname) // suffix1
    diagnostics_ipflux_str = trim(runname) // suffix2
    diagnostics_iweights2_str = trim(runname) // suffix3
    !
  end subroutine setup_iflux_IO
  !-------------------------------------------------------------------
  subroutine setup_IO
    ! Set file unit numbers, name output files, and open them as directed.
    ! Also write labels into the first line of the file.
    !
    implicit none
    !
    phi_k_unit     = 16
    energy_unit    = 17
    phiErrLog_unit = 18
    phiErrVSt_unit = 19
    !
    diagnostics_1_unit = 20    
    diagnostics_2_unit = 21
    diagnostics_3_unit = 22
    diagnostics_4_unit = 23
    particle_diagnostics_unit = 24
    z_trajectory_diagnostics_unit=25
    !
    vspace_coll_diagnostics_unit = 29
    !

!    spectrum_k2_unit = 30
!    spectrum_kx_unit = 31
!    spectrum_ky_unit = 32
!    spectrum_kz_unit = 33
    phi2_unit = 34
    phikz_unit = 35
    aparkz_unit = 36
    phikx_unit = 37
    numerical_denom_unit = 38
    !
    diagnostics_heatflux      = 40
    diagnostics_particleflux  = 41
    diagnostics_weights       = 42
    diagnostics_density       = 43
    !
    B0_unit = 64
    B0out_unit = 65
    nz_diagnostics_unit = 66
    conserve_unit = 69
    j0_unit = 70
    wgt_unit = 71
    phiplot_unit = 72

    j0_str =  trim(runname) // ".j0"
    particle_diagnostics_str = trim(runname) // ".part"
    z_trajectory_diagnostics_str = trim(runname) // ".ztraj"
    B0out_str = trim(runname) // "_B0.out"
    nz_diagnostics_str = trim(runname) // ".nz"
    trajgw_str = trim(runname) // "_trajgw.out"
    spectrumnew_str = trim(runname) // "_specnew.out"
    conserve_str = trim(runname) // ".conserve"
    wgt_str = trim(runname) // ".wgt"
    phiplot_str = trim(runname) // ".phiplot"

    numerical_denom_str = trim(runname) // ".denom"

    phi_2_str = trim(runname) // "_phi2.out"
    rho_x_str = trim(runname) // "_rhox.out"
    rho_2_str = trim(runname) // "_rho2.out"
    log_phi_2_str = trim(runname) // "_log_phi2.out"
    !
    phi2_str  = trim(runname) // ".phi2"
    phikz_str  = trim(runname) // ".phikz"
    phikx_str  = trim(runname) // ".phikx"
    aparkz_str  = trim(runname) // ".aparkz"
!    spectrum_k2_str  = trim(runname) // ".kperp_spectrum"
!    spectrum_kx_str  = trim(runname) // ".kx_spectrum"
!    spectrum_ky_str  = trim(runname) // ".ky_spectrum"
!    spectrum_kz_str  = trim(runname) // ".kz_spectrum"
    vz_str = trim(runname) // ".vz_list"
    !
    phi_k_str     = trim(runname) // ".phi_k"
!    phiErrLog_str = trim(runname) // ".phiErrLog"
    phiErrLog_str = "phiErrLog.plt"
    phiErrVSt_str     = trim(runname)//".phiErrVSt"
!    phi_x_in_str  = trim(runname) // ".phi_in"
    energy_str    = trim(runname) // ".energy"
    !
    diagnostics_1_str = trim(runname) // ".dia_1"
    diagnostics_2_str = trim(runname) // ".dia_2"
    diagnostics_3_str = trim(runname) // ".dia_3"
    diagnostics_4_str = trim(runname) // ".dia_4"
    !
    diagnostics_hflux_str = trim(runname) //".hflux"
    diagnostics_pflux_str = trim(runname) //".pflux"
    diagnostics_weights_str = trim(runname) //".weights2"
    diagnostics_density_str = trim(runname) //".density" 
    !
    vel_diagnostics_str = trim(runname) //".vel_space"
    !
    if(diagnostics_new_on) then
       if(diagnostics_1_on) open(unit=diagnostics_1_unit, file=diagnostics_1_str, status='replace')
       if(diagnostics_2_on) open(unit=diagnostics_2_unit, file=diagnostics_2_str, status='replace')
       if(diagnostics_3_on) then
          open(unit=diagnostics_3_unit, file=diagnostics_3_str, status='replace')
          write(diagnostics_3_unit,*) "# frequency, growth rate, kx, ky, kz"
       end if
       if(diagnostics_4_on) then
          if (num_species <2) then
             open(unit=diagnostics_4_unit, file=diagnostics_4_str, status='replace')
             write(diagnostics_4_unit,*) "# time, sum_weights^2, heat_flux, phi^2, sum_weights"
          else
             open(unit=diagnostics_4_unit, file=diagnostics_4_str, status='replace')
             write(diagnostics_4_unit,*) "# time, sum_weights^2, heat_flux, phi^2, sum_weights"
             open(unit=diagnostics_heatflux, file=diagnostics_hflux_str ,status='replace')
             write(diagnostics_heatflux,*) "# time, heatflux species 1, species 2, species 3"
             open(unit=diagnostics_particleflux, file=diagnostics_pflux_str ,status='replace')  
             write(diagnostics_particleflux,*) "# time, particleflux species 1, species 2, species 3"
             open(unit=diagnostics_weights, file=diagnostics_weights_str ,status='replace')  
             write(diagnostics_weights,*) "# time, weights^2 species 1, species 2, species 3"
             open(unit=diagnostics_density ,file=diagnostics_density_str ,status='replace')  
             write(diagnostics_density,*) "# time, density species 1, species 2, species 3"
          end if
       end if
    end if
    !
    if(phi2_diagnostics_on) then
       open(unit=phi2_unit, file=phi2_str, status='replace')
       write(phi2_unit,*) "# time, |phi|^2"
    end if
    !
    if(phikz_diagnostics_on) then
       open(unit=phikz_unit, file=phikz_str, status='replace')
       write(phikz_unit,*) "# time, |phi(ikz)|^2, kz=0,Nz-1"
    end if
    !
    if(phikx_diagnostics_on) then
       open(unit=phikx_unit, file=phikx_str, status='replace')
       write(phikx_unit,*) "# time, |phi(ikx)|^2, kx=0,Nx-1"
    end if
    !
    if(aparkz_diagnostics_on) then
       open(unit=aparkz_unit, file=aparkz_str, status='replace')
       write(aparkz_unit,*) "# time, |phi(ikz)|^2, kz=0,Nz-1"
    end if
    !
    if(phi_diagnostics_on) then
       if (phi_diag_mode .EQ. 3) open(unit=phiErrVSt_unit,file=phiErrVSt_str,status="replace")
!       open(unit=phi_k_unit, file=phi_k_str, status="replace")
!       open(unit=phi_x_unit, file=phi_x_str, status="replace")
!       write(phi_x_unit,*) "# x, y, phi(x,y,0)"
!       write(phi_k_unit,*) "# k_perp, phi(ikx,iky,iz=0,Nz-1)"
    end if

    !
    if(energy_diagnostics_on) then
       open(unit=energy_unit, file=energy_str, status='replace')
    end if

    if(numerical_denom_diagnostics_on) then
       open(unit=numerical_denom_unit, file=numerical_denom_str, status='replace')
    end if

    if(conserve_diagnostics_on) then
       open(unit=conserve_unit, file=conserve_str, status='replace')
       close(conserve_unit)
    end if

    if (nz_diagnostics_on) then
       open(unit=nz_diagnostics_unit,file=nz_diagnostics_str, status='replace')
    end if

    if (z_trajectory_diagnostics_on) then
       open(unit=z_trajectory_diagnostics_unit,file=z_trajectory_diagnostics_str, status='replace')
    end if
       
    if (wgt_diagnostics_on) then
       open(unit=wgt_unit, file=wgt_str, status='replace')
       close(wgt_unit)
    end if
    !
  end subroutine setup_IO
  !-------------------------------------------------------------------
 subroutine allocate_round1
    implicit none

    allocate(mu_grid(1:Nmu))
    allocate (dmu(1:Nmu))
    allocate(NE_mu(1:Nmu))
    allocate (z_grid(0:Nz))
    allocate (B0_grid(0:Nz))
    allocate (Np_mu_z(0:Nz-1,1:Nmu))

  end subroutine allocate_round1 
! -----------------------------------------------------------------
  subroutine allocate_round2
    !
    implicit none
    integer :: j, Nopt
    !
    allocate (kx(0:Nx/2))
    allocate (ky(0:Ny-1))
    allocate (kz(0:Nz-1))
    allocate (k2_perp(0:Nx/2,0:Ny-1))
    !
    allocate (phi(0:Nx/2,0:Ny-1,0:Nz-1))    
    if (Apar_on) allocate (Apar(0:Nx/2,0:Ny-1,0:Nz-1))    

!    if (spectrum_diagnostics_on) then
!    end if

    allocate(sumPhi(0:Ny-1))
    allocate(sumPhiGrowthRate(0:Ny-1))

    allocate(PhiErr(0:Nz-1))

    allocate(phiDenom(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(AparDenom(0:Nx/2,0:Ny-1,0:Nz-1))
    !
    !
    Nopt = 0

    if (use_actual_df) then
       Nopt = Nopt + 1
       wgt_df = Nparam_mandatory + Nopt
    else
       wgt_df = 1
    end if

    if (B0mode .NE. 0) then
       Nopt = Nopt+1
       zturn = Nparam_mandatory + Nopt
    else
       zturn = 1
    end if

    if ( abs(T_marker - 1.0) .GT. epsilon(0.0)) then
       Nopt = Nopt+1
       wgt_adjust = Nparam_mandatory + Nopt
    else
       wgt_adjust = 1
    end if

    if (conserve_coll_mc_on) then
       allocate(J1(0:Nx/2,0:Ny-1,0:Nz-1,1:Nmu,1:num_species))
       allocate (I_corr_Uperp_k(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
       allocate (I_corr_Q_k(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
       coll_corr = Nparam_mandatory + Nopt + 1
       nu_D = Nparam_mandatory + Nopt + 1
       delta_nu = Nparam_mandatory + Nopt + 1
       Nopt = Nopt+3
    else
       coll_corr = 1
       nu_D = 1
       delta_nu = 1
    end if
       

    allocate(p(1:Nparam_mandatory+Nopt,1:Npart,1:num_species))

 
    !
    xmid => x                        ! no perp drifts if linear, so xmid = x
    if (nonlinear_on) xmid => xm     !    perp drifts if nonlinear, so xmid = xm
    !
    ymid => y                        ! no perp drift if linear, slab, so ymid = y
    if (nonlinear_on) ymid => ym     !    perp drift if nonlinear, so ymid = ym
    if (curvature_on) ymid => ym     !    perp drift if zpinch, so ymid = ym
    !
    !
    allocate(J0(0:Nx/2,0:Ny-1,0:Nz-1,1:Nmu,1:num_species))
    !
    allocate (x_grid(0:Nx))
    allocate (y_grid(0:Ny))
    allocate (mcNorm(0:Nz-1,1:Nmu))
    allocate (mcNormSumMu(0:Nz-1))
    allocate (Nbounce(1:Nmu,1:num_species))
    allocate (curvdrift_max(1:num_species))
    !
    allocate (k2_perp_shaped(0:Nx/2,0:Ny-1))
    allocate (k_shape_z(0:Nz-1))
    allocate (k_shape_z2(0:Nz-1))
    allocate (k_shape_y(0:Ny-1))
    allocate (k_shape_x(0:Nx/2))
    allocate (k_shape_y2(0:Ny-1))
    allocate (k_shape_x2(0:Nx/2))
    !
  end subroutine allocate_round2
 !-------------------------------------------------------------------
  subroutine init_grid_round1
    implicit none
    real:: NE_mu_real, Np_mu_z_real
    real,dimension(:), allocatable:: Np_carry_over
    real:: sumNorm
    integer:: Np_temp, Nmu_temp
    real:: erf, Temp_use
    logical:: superparticle_found = .false.
 
    allocate(Np_carry_over(0:Nz-1))
   !
    if (gaussquad_on) then
       if (proc0) write(*,*) "Using Gauss-Legendre quadrature in mu."
       call gaussquad()
    else
       do imu = 1,Nmu
          mu_grid(imu) = Emax*(real(imu)-0.5)**2  / ( real(Nmu**2)*B0min)
       end do

       dmu(1) = 2.0*mu_grid(1) 
       do i = 2,Nmu
          dmu(i) = 2.0 * ( mu_grid(i) - (mu_grid(i-1)+0.5*dmu(i-1)))
       end do
    end if

    if (Nmu .LE. 1) then
       mu_grid(1) = 0.0
       dmu(1) = 1.0
    end if


    if ( T_marker .LE. 0.0 ) then
       Temp_use = 1.e28
    else  
       Temp_use = T_marker
    end if

    do is = 1, num_species
       if (( Emax .GE. ( 0.5*(delta_z/delta_t)**2 + 0.25*Emax*B0min/(B0max*real(Nmu**2))) ) .AND. proc0)  then
          write(*,*) "ERROR: Inputs incompatible. Emax > ",( 0.5*mass(is)*(delta_z/delta_t)**2 + 0.25*mu_grid(1)*B0min)
          write(*,*) "The condition: Emax < 0.5*mass*(delta_z/delta_t)**2 + mu*B must hold to ensure particles don't bounce twice in a timestep."
          write(*,*) "Fixes: Descrease delta_t, Decrease Emax, or Increase delta_z."
          write(*,*) "Alternatively, edit code to handle multi-bounce timesteps or to exclude such particles."
          !stop
       end if
    end do


    sumNorm = 0.0
    do iz = 0, Nz-1
       do imu = 1, Nmu
          if (Emax .GE. mu_grid(imu)*B0_grid(iz)) sumNorm = sumNorm + exp(-mu_grid(imu)*B0_grid(iz)/Temp_use)  *erf(sqrt( (Emax - mu_grid(imu)*B0_grid(iz))/Temp_use))
       end do
    end do   

    Npart = 0
    Np_carry_over = 0.0
    Nmu_temp = Nmu
    do imu = 1,Nmu_temp
       NE_mu(imu) = 0
       do iz = 0, Nz-1

          if (Emax .GE. mu_grid(imu)*B0_grid(iz)) then 
             Np_mu_z_real =  real(Ncell*Nx*Ny*Nz)*exp(-mu_grid(imu)*B0_grid(iz)/Temp_use)   * erf(sqrt( (Emax - mu_grid(imu)*B0_grid(iz))/Temp_use))/(real(nproc)*sumNorm)
             Np_temp = int(Np_mu_z_real)

             Np_carry_over(iz) = Np_carry_over(iz) + (Np_mu_z_real - real(Np_temp))
             if (Np_carry_over(iz) .GT. 1.0) then
                Np_temp = Np_temp + 1
                Np_carry_over(iz) = Np_carry_over(iz) - 1.0
             end if
             !if ((imu .EQ. Nmu) .AND. Np_carry_over(iz) .GT. 0.5) Np_temp = Np_temp + 1
 
             ! Think of a way to consolidate mu_grid so that each grid point has at least one particle

             Np_mu_z(iz,imu) = Np_temp
          else
             Np_mu_z(iz,imu) = 0
          endif
          NE_mu(imu)  = NE_mu(imu) + Np_mu_z(iz,imu)  
       end do

       if (proc0) write(*,*) "imu = ", imu, "iz = ", iz, ", mu = ", mu_grid(imu), " NE_mu = ", NE_mu(imu)

       Npart = Npart + NE_mu(imu)
 
       if (superparticle_conserve .AND. ( .NOT. superparticle_found ) ) then
          if ( imu .EQ. imu_conserve) then
             superparticle_found = .true.
             idx_conserve = Npart
          else if ( (mu_grid(imu) .GT. 0.5) ) then
             superparticle_found = .true.
             imu_conserve = imu 
             idx_conserve = Npart
          end if
       end if

    end do


    if (proc0 .AND. (NE_mu(Nmu) .EQ. 0)) write(*,*) "*** WARNING *** Maximum mu not represented with any particles"

    deallocate(Np_carry_over)

  end subroutine init_grid_round1
  !-------------------------------------------------------------------
  subroutine gaussquad()
    implicit none
    
    real, dimension(:),allocatable :: weights, abscissae


    allocate(weights(1:Nmu))
    allocate(abscissae(1:Nmu))

    if (Nmu .EQ. 18) then

    weights(10) = 0.1691423829631436
    weights(11) = 0.1642764837458327
    weights(12) = 0.1546846751262652
    weights(13) = 0.1406429146706507
    weights(14) = 0.1225552067114785
    weights(15) = 0.1009420441062872
    weights(16) = 0.0764257302548891
    weights(17) = 0.0497145488949698
    weights(18) = 0.0216160135264833
       
    abscissae(10) = 0.0847750130417353
    abscissae(11) = 0.2518862256915055
    abscissae(12) = 0.4117511614628426
    abscissae(13) = 0.5597708310739475
    abscissae(14) = 0.6916870430603532
    abscissae(15) = 0.8037049589725231
    abscissae(16) = 0.8926024664975557
    abscissae(17) = 0.9558239495713977
    abscissae(18) = 0.9915651684209309
       do i = 1,Nmu/2
          weights(i) = weights(Nmu-i+1) 
          abscissae(i) = -abscissae(Nmu-i+1) 
       end do

       do i = 1,Nmu
          mu_grid(i) = Emax*(abscissae(i) + 1.0)/(2.0*B0min)
          dmu(i) = 0.5*Emax/B0min*weights(i)
       end do

    else if (Nmu .EQ. 32) then

weights(17) = 0.09654008851473
weights(18) = 0.09563872007927
weights(19) = 0.0938443990808
weights(20) = 0.09117387869576
weights(21) = 0.0876520930044
weights(22) = 0.08331192422695
weights(23) = 0.07819389578707
weights(24) = 0.07234579410885
weights(25) = 0.06582222277636
weights(26) = 0.05868409347854
weights(27) = 0.05099805926238
weights(28) = 0.04283589802223
weights(29) = 0.03427386291302
weights(30) = 0.02539206530926
weights(31) = 0.01627439473091
weights(32) = 0.00701861000947

abscissae(17) = 0.04830766568774
abscissae(18) = 0.1444719615828
abscissae(19) = 0.23928736225214
abscissae(20) = 0.33186860228213
abscissae(21) = 0.42135127613064
abscissae(22) = 0.50689990893223
abscissae(23) = 0.58771575724076
abscissae(24) = 0.66304426693022
abscissae(25) = 0.73218211874029
abscissae(26) = 0.79448379596794
abscissae(27) = 0.84936761373257
abscissae(28) = 0.89632115576605
abscissae(29) = 0.93490607593774
abscissae(30) = 0.96476225558751
abscissae(31) = 0.98561151154527
abscissae(32) = 0.99726386184948
       do i = 1,Nmu/2
          weights(i) = weights(Nmu-i+1) 
          abscissae(i) = -abscissae(Nmu-i+1) 
       end do

       do i = 1,Nmu
          mu_grid(i) = Emax*(abscissae(i) + 1.0)/(2.0*B0min)
          dmu(i) = 0.5*Emax/B0min*weights(i)
       end do
    else if (Nmu .EQ. 64) then

weights(33) = 0.04869095700914
weights(34) = 0.0485754674415
weights(35) = 0.0483447622348
weights(36) = 0.04799938859646
weights(37) = 0.04754016571483
weights(38) = 0.04696818281621
weights(39) = 0.04628479658131
weights(40) = 0.04549162792742
weights(41) = 0.04459055816376
weights(42) = 0.04358372452932
weights(43) = 0.04247351512365
weights(44) = 0.04126256324262
weights(45) = 0.03995374113272
weights(46) = 0.03855015317862
weights(47) = 0.03705512854024
weights(48) = 0.03547221325688
weights(49) = 0.03380516183714
weights(50) = 0.03205792835485
weights(51) = 0.0302346570724
weights(52) = 0.02833967261426
weights(53) = 0.02637746971505
weights(54) = 0.02435270256871
weights(55) = 0.02227017380838
weights(56) = 0.02013482315353
weights(57) = 0.0179517157757
weights(58) = 0.01572603047602
weights(59) = 0.01346304789672
weights(60) = 0.01116813946013
weights(61) = 0.00884675982636
weights(62) = 0.00650445796898
weights(63) = 0.00414703326056
weights(64) = 0.0017832807217

abscissae(33) = 0.02435029266342
abscissae(34) = 0.0729931217878
abscissae(35) = 0.12146281929612
abscissae(36) = 0.16964442042399
abscissae(37) = 0.21742364374001
abscissae(38) = 0.26468716220877
abscissae(39) = 0.31132287199021
abscissae(40) = 0.35722015833767
abscissae(41) = 0.40227015796399
abscissae(42) = 0.44636601725346
abscissae(43) = 0.48940314570705
abscissae(44) = 0.53127946401989
abscissae(45) = 0.57189564620263
abscissae(46) = 0.61115535517239
abscissae(47) = 0.64896547125466
abscissae(48) = 0.68523631305423
abscissae(49) = 0.71988185017161
abscissae(50) = 0.75281990726053
abscissae(51) = 0.78397235894334
abscissae(52) = 0.8132653151228
abscissae(53) = 0.84062929625258
abscissae(54) = 0.86599939815409
abscissae(55) = 0.88931544599511
abscissae(56) = 0.9105221370785
abscissae(57) = 0.92956917213196
abscissae(58) = 0.9464113748584
abscissae(59) = 0.96100879965205
abscissae(60) = 0.97332682778991
abscissae(61) = 0.98333625388463
abscissae(62) = 0.99101337147674
abscissae(63) = 0.99634011677196
abscissae(64) = 0.99930504173577

       do i = 1,Nmu/2
          weights(i) = weights(Nmu-i+1) 
          abscissae(i) = -abscissae(Nmu-i+1) 
       end do

       do i = 1,Nmu
          mu_grid(i) = Emax*(abscissae(i) + 1.0)/(2.0*B0min)
          dmu(i) = 0.5*Emax/B0min*weights(i)
       end do
 
    else
       write(*,*) "Gaussian quadrature not available for Nmu = ", Nmu, ". Edit the code to include." 
       stop
    end if
       
    deallocate(weights,abscissae)

  end subroutine
   !-------------------------------------------------------------------
  subroutine init_B0
    implicit none
    real, dimension(:), allocatable:: B0_in, z_in
    integer:: k_sym,Nz_in,k_in
    real:: slope,z_start,z_end

    delta_z = Lz/real(Nz)

    do iz=0,Nz
       z_grid(iz) = real(iz)*delta_z
    end do

    ! <doc> B0mode < 0 needs to be fixed </doc>
    if (B0mode .LT. 0) then
    ! Read from file and linearly interpolate

       ! <doc> B0mode < 0 needs to be fixed </doc>
       write(*,*) "B0mode<1 (interpolated from file) is currently broken. Fix the code of use another option."
       stop

       open(B0_unit,file=B0file,status="old")
       read(B0_unit,*) Nz_in
       if (Nz_in .LT. 2) then
          write(*,*) "ERROR: Nz_in must be > 1. Use uniform field option (B0mode = 0) instead."
          stop
       end if
       allocate(B0_in(0:2*Nz_in-1))
       allocate(z_in(0:2*Nz_in-1))
       do iz=0,Nz_in-1
          read(B0_unit,*) z_in(iz), B0_in(iz) 
       end do
       close(B0_unit)
       if ( abs(z_in(0)) .GT. epsilon(0.0)) then
          write(*,*) "ERROR: z_in(0) must be zero."
          write(*,*) "z_in(0) = ", z_in(0)
          stop
       end if
       !
       do k_in = 0, Nz_in-2
          if ( (B0_in(k_in+1) .GT. B0_in(k_in))  ) then 
             write(*,*) "ERROR: B0 must be symmetric about its minimum at the center of the domain. Please redefine a B0 up to this point of symmetry."
             stop
          end if
       end do
       !
       Nz_in = 2*(Nz_in-1)

       do iz = Nz_in, (Nz_in+1)/2, -1
          z_in(iz) = Lz -  z_in(Nz_in-iz)
          B0_in(iz) = B0_in(Nz_in-iz)
       end do

       ! Rescale to Lz
       z_start = z_in(0)
       z_end = z_in(Nz_in)
       do k_in = 0, Nz_in-1
          z_in(k_in) = z_in(k_in)*Lz/(z_end - z_start)
       end do

       open(B0out_unit,file=B0out_str,status="replace")
       write(B0out_unit,*) "Rescaled B0 from file:"
       do k_in = 0, Nz_in-1
          write(B0out_unit,'(2F15.8)') z_in(k_in), B0_in(k_in)
       end do

       write(B0out_unit,*) "Interpolated B0 onto grid:"
       k_in = 0
       do iz=0,Nz-1
          do while ( z_grid(iz) .GT.  z_in(k_in) )
             k_in = k_in+1
          end do
          if ( (k_in+1) .GT. (2*Nz_in-2) ) then
             write(*,*) "Error in B0 interpolation. Array out of bounds. Investigate."
          end if  
          slope = ( B0_in(k_in+1) - B0_in(k_in) )/(z_in(k_in+1) - z_in(k_in))           
          B0_grid(iz) = B0_in(k_in) + slope * ( z_grid(iz)  - z_in(k_in)) 
          write(B0out_unit,'(2F15.8)') z_grid(iz), B0_grid(iz)
       end do
       B0_grid(Nz) = B0_grid(0)
     
       close(B0_unit)
       deallocate(B0_in,z_in)
    !
    else if (B0mode .EQ. 0) then
    ! Uniform field
       B0_grid(0:Nz) = B0mag
    !
    else if (B0mode .EQ. 1) then
    ! Analytic field #1
       if (proc0) then
          open(B0out_unit,file=B0out_str,status="replace")
          write(B0out_unit,*) "Interpolated field: "
       end if
       do iz = 0,Nz/2
          B0_grid(iz) = B0mag / (1.0 + eps*cos(pi*(2.0*z_grid(iz)/Lz - 1.0)) )
       end do
       do iz = Nz, (Nz+1)/2, -1
          B0_grid(iz) = B0_grid(Nz-iz)
       end do

       if (proc0) then
          do iz = 0,Nz
             write(B0out_unit,*) z_grid(iz), B0_grid(iz)
          end do
        
          write(B0out_unit,*) "Inverse test:"
          do iz = 0,Nz
             write(B0out_unit,*) z_grid(iz), B0_grid(iz), invB0(B0_grid(iz))
          end do
          close(B0out_unit)
       end if

    else if (B0mode .EQ. 2) then
    
    ! Analytic field #2
       if (proc0) then
          open(B0out_unit,file=B0out_str,status="replace")
          write(B0out_unit,*) "Interpolated field: "
       end if
       do iz = 0,Nz/2
          B0_grid(iz) = B0mag + eps*cos(pi*(2.0*z_grid(iz)/Lz))
       end do
       do iz = Nz, (Nz+1)/2, -1
          B0_grid(iz) = B0_grid(Nz-iz)
       end do

       if (proc0) then
          do iz = 0,Nz
             write(B0out_unit,*) z_grid(iz), B0_grid(iz)
          end do
          write(B0out_unit,*) "Inverse test:"
          do iz = 0,Nz
             write(B0out_unit,*) z_grid(iz), B0_grid(iz), invB0(B0_grid(iz))
          end do
          close(B0out_unit)
       end if

    end if

    B0min = B0_grid(0)
    B0max = B0_grid(0)
    do iz=0,Nz
       if (B0_grid(iz) .LT. B0min) then
          B0min = B0_grid(iz)
       end if
       if (B0_grid(iz) .GT. B0max) then
          B0max = B0_grid(iz)
       end if
    end do


  end subroutine init_B0
  
   !-------------------------------------------------------------------
   subroutine init_grid_round2
    ! Determine grid spacing in both (x,y,z) and (kx,ky,kz) space.
    !
    implicit none
    real:: sumNorm,NE_mu_real
    real:: erf

    Msize(1) = Nx
    Msize(2) = Ny
    Msize(3) = Nz
    !
    scale_x = 2.0*pi/Lx ! Lx is in units of rho ... scale_x = 2.0*pi/(Lx/rho) 
    scale_y = 2.0*pi/Ly ! Lx is in units of rho ... scale_y = 2.0*pi/(Ly/rho) 
    scale_z = 2.0*pi/Lz ! Lx is in units of a   ... scale_z = 2.0*pi/(Lz/a)
    !
    delta_x = Lx/real(Nx)
    delta_y = Ly/real(Ny)
    !
    Volume_scale = 1./(delta_x*delta_y*delta_z)
    !
    do i=0,Nx
       x_grid(i) = real(i)*delta_x
    end do
    !
    do i=0,Ny
       y_grid(i) = real(i)*delta_y
    end do
    
    do ikx=0,Nx/2
       kx(ikx) = real(ikx)*scale_x
    end do
    !
    do iky=0,Ny/2
       ky(iky) = real(iky)*scale_y
    end do
    !
    do iky=(Ny/2)+1,Ny-1
       ky(iky) = -ky(Ny-iky)
    end do
    !
    do ikz=0,Nz/2
       kz(ikz) = real(ikz)*scale_z
    end do
    !
    do ikz=(Nz/2)+1,Nz-1
       kz(ikz) = -kz(Nz-ikz)
    end do
    !
    k_shape_x2(:) = 1.0
    k_shape_y2(:) = 1.0
    k_shape_z2(:) = 1.0
    k_shape_x(:) = 1.0
    k_shape_y(:) = 1.0
    k_shape_z(:) = 1.0

    if (shape_fcn_on) then

       do ikx = 1,Nx/2
          k_shape_x2(ikx) = sin(0.5*kx(ikx)*delta_x)/(0.5*kx(ikx)*delta_x)
       end do

       do iky = 1,Ny-1
          k_shape_y2(iky) = sin(0.5*ky(iky)*delta_y)/(0.5*ky(iky)*delta_y)
       end do

       do ikx = 1,Nx/2
          k_shape_x(ikx) = sin(kx(ikx)*delta_x)/(kx(ikx)*delta_x)
       end do
  
       do iky = 1,Ny-1
          k_shape_y(iky) = sin(ky(iky)*delta_y)/(ky(iky)*delta_y)
       end do

       do ikz = 1,Nz-1
          k_shape_z(ikz) = sin(kz(ikz)*delta_z)/(kz(ikz)*delta_z)
       end do

       do ikz = 1,Nz-1
          k_shape_z2(ikz) = sin(0.5*kz(ikz)*delta_z)/(0.5*kz(ikz)*delta_z)
       end do

    end if

    do iz = 0,Nz-1 
       do imu=1,Nmu
          if ((Emax - mu_grid(imu)*B0_grid(iz)) .GT. 0.0) then
             if (T_marker .LT. 0.0) then
                mcNorm(iz,imu) = B0_grid(iz) * 2.0*sqrt(1.0/pi)* sqrt(Emax - mu_grid(imu))
             else if ( abs(T_marker - 1.0) .GT. epsilon(0.0)) then
                mcNorm(iz,imu) =  B0_grid(iz) *sqrt(T_marker)*  exp(-mu_grid(imu)*B0_grid(iz)/T_marker) *erf(sqrt( (Emax-mu_grid(imu)*B0_grid(iz)) / T_marker))
             else
                mcNorm(iz,imu) = B0_grid(iz) * exp(-mu_grid(imu)*B0_grid(iz)) *erf(sqrt(Emax-mu_grid(imu)*B0_grid(iz)))
             end if
          else
             mcNorm(iz,imu) = 0.0
          end if
       end do
       mcNormSumMu(iz)= 0.0
       do i = 1,Nmu
          mcNormSumMu(iz) = mcNormSumMu(iz) + mcNorm(iz,i)*dmu(i)
       end do
          
    end do

    if (Nmu .EQ. 1) mcNorm = 1.0

    do iky=0,Ny-1
       do ikx=0,Nx/2
          k2_perp(ikx,iky) = kx(ikx)**2 + ky(iky)**2
          k2_perp_shaped(ikx,iky) = kx(ikx)**2 * k_shape_x2(ikx)**2 + ky(iky)**2 * k_shape_y2(iky)**2
       end do
    end do


    if (proc0) then
       write (*,*) '#----------------------------'
       write (*,*) '# '
       write (*,*) '# Total number of particles on processor 0: ',Npart
       write (*,*) '# '
       write (*,*) '# ky values: '
       do i=0,Ny-1
          write (*,*) '# ',ky(i)
       end do
       write (*,*) '# '
       write (*,*) '# kx values: '
       do i=0,Nx/2
          write (*,*) '# ',kx(i)
       end do
       write (*,*) '# '
       write (*,*) '# kz values: '
       do i=0,Nz-1
          write (*,*) '# ',kz(i)
       end do
       write (*,*) '#----------------------------'
       write (*,*)
    end if
    !
  end subroutine init_grid_round2

   subroutine spectrum_phi_old(phi_in)
    !
    ! Diagnostic output for phi spectrum.
    !
    use mp, only: proc0
    !
    implicit none
    complex, dimension(0:,0:,0:), intent(in) :: phi_in
    real, dimension(:), allocatable :: phi_kx, phi_ky, phi_z, phi2_k
    real:: phi2_tot
    real, dimension(:),save, allocatable :: phi_kx_m1, phi_kx_m2, phi_ky_m1, phi_ky_m2, phi_z_m1, phi_z_m2
    real,save:: phi2_tot_m1, phi2_tot_m2
    real, dimension(:,:), allocatable :: phi_k
    real :: tmp, phi_sq, phi_sq_m1, phi_sq_m2
    integer, dimension(:), allocatable :: number
    logical,save:: first = .true.
    integer :: ikx, iky, ikz, ik
    complex:: omega_temp
    real,save:: omega_phi2_tot, gamma_phi2_tot
    real, dimension(:),save, allocatable:: omega_phi_z, omega_phi_ky, omega_phi_kx
    real, dimension(:),save, allocatable:: gamma_phi_z, gamma_phi_ky, gamma_phi_kx
    !

    allocate(phi_kx(0:Nx/2))
    allocate(phi_ky(0:Ny-1))
    allocate(phi_z(0:Nz-1))
    allocate(phi_k(0:Nx/2,0:Ny-1))
    allocate(phi2_k(1:Nx/2))
    allocate(number(1:Nx/2))

    if (first) then
       allocate(phi_kx_m1(0:Nx/2))
       allocate(phi_kx_m2(0:Nx/2))
       allocate(phi_ky_m1(0:Ny-1))
       allocate(phi_ky_m2(0:Ny-1))
       allocate(phi_z_m1(0:Nz-1))
       allocate(phi_z_m2(0:Nz-1))
       allocate(omega_phi_kx(0:Nx/2))
       allocate(omega_phi_ky(0:Ny-1))
       allocate(omega_phi_z(0:Nz-1))
       allocate(gamma_phi_kx(0:Nx/2))
       allocate(gamma_phi_ky(0:Ny-1))
       allocate(gamma_phi_z(0:Nz-1))
       write(spectrum_k2_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:",(ik*2.*pi/Lx,", ",ik=1,Nx/2)
       write(spectrum_kx_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:",(ik*2.*pi/Lx,", ",ik=0,Nx/2) 
       write(spectrum_ky_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:", (ik*2.*pi/Ly,ik=0,Ny/2),( (-Ny+ik)*(2.*pi/Ly),", ",ik=Ny/2+1,Ny-1)
       write(spectrum_kz_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:", (ik*2.*pi/Lz,ik=0,Nz/2),( (-Nz+ik)*(2.*pi/Lz),", ",ik=Nz/2+1,Nz-1)
       omega_phi_kx = 0.0
       omega_phi_ky = 0.0
       omega_phi_z = 0.0
       gamma_phi_kx = 0.0
       gamma_phi_ky = 0.0
       gamma_phi_z = 0.0
       phi_kx_m1    = 0.0
       phi_ky_m1    = 0.0
       phi_z_m1    = 0.0
       phi_kx_m2    = 0.0
       phi_ky_m2    = 0.0
       phi_z_m2    = 0.0

       first = .false.
    end if
    !
    number    = 0 
    phi_k     = 0.0
    phi2_k    = 0.0
    phi_kx    = 0.0
    phi_ky    = 0.0
    phi_z    = 0.0
    !
    do iz=0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             phi_k(ikx,iky) = phi_k(ikx,iky) + real(phi_in(ikx,iky,iz)*conjg(phi_in(ikx,iky,iz)))
             phi_kx(ikx)    = phi_kx(ikx) + real(phi_in(ikx,iky,iz)*conjg(phi_in(ikx,iky,iz)))

             phi_ky(iky)    = phi_ky(iky) + real(phi_in(ikx,iky,iz)*conjg(phi_in(ikx,iky,iz)))
             phi_z(iz)    = phi_z(iz) + real(phi_in(ikx,iky,iz)*conjg(phi_in(ikx,iky,iz)))
          end do
       end do
    end do

    do iky = 0,Ny-1
       if ( (phi_ky_m1(iky) .NE. 0.0) .AND. ( (2.0 - (phi_ky(iky)/phi_ky_m1(iky)) - (phi_ky_m2(iky)/phi_ky_m1(iky))) .GT. 0.0 ) ) then
          omega_phi_ky(iky) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (phi_ky(iky)/phi_ky_m1(iky)) - (phi_ky_m2(iky)/phi_ky_m1(iky))) + (1.0-alpha_ewa)*omega_phi_ky(iky)
       else
          omega_phi_ky(iky) = 0.0 + (1.0 - alpha_ewa)*omega_phi_ky(iky)
       end if
        
       if (phi_ky_m1(iky) .NE. phi_ky(iky)) then
          gamma_phi_ky(iky) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(phi_ky(iky)-phi_ky_m1(iky))/(phi_ky(iky)+phi_ky_m1(iky)) + (1.0 - alpha_ewa)*gamma_phi_ky(iky)
       else
          gamma_phi_ky(iky) = 0.0
       end if
    end do

    do ikx = 0,Nx/2
       if ( (phi_ky_m1(ikx) .NE. 0.0) .AND. ( (2.0 - (phi_kx(ikx)/phi_kx_m1(ikx)) - (phi_kx_m2(ikx)/phi_kx_m1(ikx))) .GT. 0.0 ) ) then
          omega_phi_kx(ikx) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (phi_kx(ikx)/phi_kx_m1(ikx)) - (phi_kx_m2(ikx)/phi_kx_m1(ikx))) + (1.0-alpha_ewa)*omega_phi_kx(ikx)
       else
          omega_phi_kx(ikx) = 0.0 + (1.0 - alpha_ewa)*omega_phi_kx(ikx)
       end if
       if (phi_kx_m1(ikx) .NE. phi_kx(ikx)) then
          gamma_phi_kx(ikx) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(phi_kx(ikx)-phi_kx_m1(ikx))/(phi_kx(ikx)+phi_kx_m1(ikx)) + (1.0 - alpha_ewa)*gamma_phi_kx(ikx)
       else
          gamma_phi_kx(ikx) = 0.0 + (1.0 - alpha_ewa)*gamma_phi_kx(ikx)
       end if
    end do

    do iz = 0,Nz-1
       if ( (phi_z_m1(iz) .NE. 0.0) .AND. ( (2.0 - (phi_z(iz)/phi_z_m1(iz)) - (phi_z_m2(iz)/phi_z_m1(iz))) .GT. 0.0 ) ) then
          omega_phi_z(iz) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (phi_z(iz)/phi_z_m1(iz)) - (phi_z_m2(iz)/phi_z_m1(iz))) + (1.0-alpha_ewa)*omega_phi_z(iz)
       else
          omega_phi_z(iz) = 0.0 + (1.0 - alpha_ewa)*omega_phi_z(iz)
       end if
       if (phi_z_m1(iz) .NE. phi_z(iz)) then
          gamma_phi_z(iz) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(phi_z(iz)-phi_z_m1(iz))/(phi_z(iz)+phi_z_m1(iz)) + (1.0 - alpha_ewa)*gamma_phi_z(iz)
       else
          gamma_phi_z(iz) = 0.0 + (1.0 - alpha_ewa)*gamma_phi_z(iz)
       end if
    end do

    !
    phi2_tot = 0.0
    do iz = 0,Nz-1
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             phi2_tot = phi2_tot + cabs(phi_in(ikx,iky,iz))**2
          end do
       end do
    end do

    if ( (phi2_tot_m1 .NE. 0.0) .AND. ( (2.0 - (phi2_tot/phi2_tot_m1) - (phi2_tot_m2/phi2_tot_m1)) .GT. 0.0 ) ) then
       omega_phi2_tot = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (phi2_tot/phi2_tot_m1) - (phi2_tot_m2/phi2_tot_m1)) + (1.0-alpha_ewa)*omega_phi2_tot
    else
       omega_phi2_tot = 0.0 + (1.0 - alpha_ewa)*omega_phi2_tot
    end if
    if (phi2_tot_m1 .NE. phi2_tot) then
       gamma_phi2_tot = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(phi2_tot-phi2_tot_m1)/(phi2_tot+phi2_tot_m1) + (1.0 - alpha_ewa)*gamma_phi2_tot
    else
       gamma_phi2_tot = 0.0 + (1.0 - alpha_ewa)*gamma_phi2_tot
    end if
    
 
!    do ik=1,Nx/2
!       do ikx=0,ik
!          do iky=0,ik
!             if (ikx**2+iky**2 .le. ik**2 .and. ikx**2+iky**2 > (ik-1)**2) then
!                phi2_k(ik) = phi2_k(ik) + phi_k(ikx,iky)
!                number(ik) = number(ik) + 1
!             end if
!          end do
!       end do
!    end do
    !
!    do ik=1,Nx/2
!       do ikx=0,ik
!          do iky=Ny/2+1,Ny-1
!             if (ikx**2+(Ny-iky)**2 .le. ik**2 .and. ikx**2+(Ny-iky)**2 > (ik-1)**2) then
!                phi2_k(ik) = phi2_k(ik) + phi_k(ikx,iky)
!                 number(ik) = number(ik) + 1
!            end if
!          end do
!       end do
!    end do
    !

!    write(spectrum_k2_unit,'(1p,1000g15.7)') time,(phi2_k(ik),ik=1,Nx/2)
    write(spectrum_k2_unit,'(1p,1000g15.7)') time,omega_phi2_tot,gamma_phi2_tot,phi2_tot
    write(spectrum_kx_unit,'(1p,1000g15.7)') time,(omega_phi_kx(ik),gamma_phi_kx(ik),phi_kx(ik),ik=0,Nx/2)
    write(spectrum_ky_unit,'(1p,1000g15.7)') time,(omega_phi_ky(ik),gamma_phi_ky(ik),phi_ky(ik),ik = 0,Ny/2),(omega_phi_ky(ik),gamma_phi_ky(ik),phi_ky(ik),ik = Ny/2+1,Ny-1)
    write(spectrum_kz_unit,'(1p,1000g15.7)') time,(omega_phi_z(ik) ,gamma_phi_z(ik) ,phi_z(ik),ik = 0,Nz/2),(omega_phi_z(ik), gamma_phi_z(ik) ,phi_z(ik),ik = Nz/2+1,Nz-1)      
    close(spectrum_k2_unit)
    open(unit=spectrum_k2_unit, file=spectrum_k2_str, status='old', access='append')

    close(spectrum_kx_unit)
    open(unit=spectrum_kx_unit, file=spectrum_kx_str, status='old', access='append')

    close(spectrum_ky_unit)
    open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')

    close(spectrum_kz_unit)
    open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
 
    phi2_tot_m2 = phi2_tot_m1
    phi_z_m2 = phi_z_m1
    phi_ky_m2 = phi_ky_m1
    phi_kx_m2 = phi_kx_m1

    phi2_tot_m1 = phi2_tot
    phi_z_m1 = phi_z
    phi_ky_m1 = phi_ky
    phi_kx_m1 = phi_kx

!    do ik=1,Nx/2
!       phi2_k = phi2_k(ik)*(ik*2.*pi/Lx)/number(ik)
!       write(spectrum_k2_unit,*) ik*2.*pi/Lx, phi2_k(ik)
!       close(spectrum_k2_unit)
!       open(unit=spectrum_k2_unit, file=spectrum_k2_str, status='old', access='append')
!    end do
!    !
!    do ik=0,Nx/2
!       write(spectrum_kx_unit,*) ik*2.*pi/Lx, phi_kx(ik) 
!       close(spectrum_kx_unit)
!       open(unit=spectrum_kx_unit, file=spectrum_kx_str, status='old', access='append')
!    end do
!    !
!    do ik=Ny/2+1,Ny-1
!       write(spectrum_ky_unit,*) (-Ny+ik)*(2.*pi/Ly), phi_ky(ik)
!       close(spectrum_ky_unit)
!       open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')
!    end do
!    !
!    do ik=0,Ny/2
!       write(spectrum_ky_unit,*) ik*2.*pi/Ly, phi_ky(ik)
!       close(spectrum_ky_unit)
!       open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')
!    end do
!    !
!    do ik=Nz/2+1,Nz-1
!       write(spectrum_kz_unit,*) (-Nz+ik)*(2.*pi/Lz), phi_kz(ik)
!       close(spectrum_kz_unit)
!       open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
!    end do
!    do ik=0,Nz/2
!       write(spectrum_kz_unit,*) ik*2.*pi/Lz, phi_kz(ik)
!       close(spectrum_kz_unit)
!       open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
!    end do
    !
    deallocate(phi_k, phi2_k, phi_kx, phi_ky, phi_z, number)
    !
  end subroutine spectrum_phi_old
  !-------------------------------------------------------------------
 !-------------------------------------------------------------------
  subroutine spectrum_phi(phi_in)
    !
    ! Diagnostic output for phi spectrum.
    !
    use mp, only: proc0
    !
    implicit none
    complex, dimension(0:,0:,0:), intent(in) :: phi_in
    complex, dimension(:), allocatable :: phi_kx, phi_ky, phi_z
    real, dimension(:), allocatable :: phi2_k
    complex, save,dimension(:), allocatable :: phi_kx_m1, phi_z_m1, phi_ky_m1
    complex, dimension(:,:), allocatable :: phi_k
    real :: tmp, phi_sq, phi_sq_m1, phi_sq_m2
    integer, dimension(:), allocatable :: number
    logical,save:: first = .true.
    integer :: ikx, iky, ikz, ik
    complex, dimension(:), save,allocatable:: omega_phi_z, omega_phi_ky, omega_phi_kx

    !

    allocate(phi_kx(0:Nx/2))
    allocate(phi_ky(0:Ny-1))
    allocate(phi_z(0:Nz-1))
    allocate(phi_k(0:Nx/2,0:Ny-1))
    allocate(phi2_k(1:Nx/2))
    allocate(number(1:Nx/2))

    if (first) then
       allocate(omega_phi_kx(0:Nx/2))
       allocate(omega_phi_ky(0:Ny-1))
       allocate(omega_phi_z(0:Nz-1))
       allocate(phi_kx_m1(0:Nx/2))
       allocate(phi_ky_m1(0:Ny-1))
       allocate(phi_z_m1(0:Nz-1))
       write(spectrum_k2_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:",(ik*2.*pi/Lx,", ",ik=1,Nx/2)
       write(spectrum_kx_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:",(ik*2.*pi/Lx,", ",ik=0,Nx/2) 
       write(spectrum_ky_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:", (ik*2.*pi/Ly,ik=0,Ny/2),( (-Ny+ik)*(2.*pi/Ly),", ",ik=Ny/2+1,Ny-1)
       write(spectrum_kz_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, phi:", (ik*2.*pi/Lz,ik=0,Nz/2),( (-Nz+ik)*(2.*pi/Lz),", ",ik=Nz/2+1,Nz-1)
       omega_phi_kx = 0.0
       omega_phi_ky = 0.0
       omega_phi_z = 0.0
       phi_kx_m1    = 0.0
       phi_ky_m1    = 0.0
       phi_z_m1    = 0.0

       first = .false.
    end if
    !
    number    = 0 
    phi_k     = 0.0
    phi2_k    = 0.0
    phi_kx    = 0.0
    phi_ky    = 0.0
    phi_z    = 0.0
    !
    do iz=0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             phi_k(ikx,iky) = phi_k(ikx,iky) + phi_in(ikx,iky,iz)
             phi_kx(ikx)    = phi_kx(ikx) + phi_in(ikx,iky,iz)

             phi_ky(iky)    = phi_ky(iky) + phi_in(ikx,iky,iz)
             phi_z(iz)    = phi_z(iz) + phi_in(ikx,iky,iz)
          end do
       end do
    end do

    do iky = 0,Ny-1
       if ( cabs(phi_ky_m1(iky)) .NE. 0.0 ) then
          omega_phi_ky(iky) = (1.0-alpha_ewa)*omega_phi_ky(iky) + alpha_ewa * ( (zi/(real(nout_spectrum)*delta_t))*log( phi_ky(iky)/phi_ky_m1(iky)) )
       else
          omega_phi_ky(iky) = 0.0
       end if

    end do

    do ikx = 0,Nx/2
       if ( cabs(phi_kx_m1(ikx)) .NE. 0.0 ) then
          omega_phi_kx(ikx) = (1.0-alpha_ewa)*omega_phi_kx(ikx) + alpha_ewa * ( (zi/(real(nout_spectrum)*delta_t))*log( phi_ky(ikx)/phi_ky_m1(ikx)) )
       else
          omega_phi_kx(ikx) = 0.0
       end if
    end do

    do iz = 0,Nz-1
       if ( cabs(phi_z_m1(iz)) .NE. 0.0 ) then
          omega_phi_z(iz) = (1.0-alpha_ewa)*omega_phi_z(iz) + alpha_ewa * ( (zi/(real(nout_spectrum)*delta_t))*log( phi_z(iz)/phi_z_m1(iz)) )
       else
          omega_phi_z(iz) = 0.0
       end if
    end do

    !
    do ik=1,Nx/2
       do ikx=0,ik
          do iky=0,ik
             if (ikx**2+iky**2 .le. ik**2 .and. ikx**2+iky**2 > (ik-1)**2) then
                phi2_k(ik) = phi2_k(ik) + phi_k(ikx,iky)
                number(ik) = number(ik) + 1
             end if
          end do
       end do
    end do
    !
    do ik=1,Nx/2
       do ikx=0,ik
          do iky=Ny/2+1,Ny-1
             if (ikx**2+(Ny-iky)**2 .le. ik**2 .and. ikx**2+(Ny-iky)**2 > (ik-1)**2) then
                phi2_k(ik) = phi2_k(ik) + real(phi_k(ikx,iky))*conjg(phi_k(ikx,iky))
                number(ik) = number(ik) + 1
             end if
          end do
       end do
    end do
    !

    write(spectrum_kx_unit,'(1p,1000g15.7)') time,(real(omega_phi_kx(ik)),aimag(omega_phi_kx(ik)),cabs(phi_kx(ik)),ik=0,Nx/2)
    write(spectrum_ky_unit,'(1p,1000g15.7)') time,(real(omega_phi_ky(ik)),aimag(omega_phi_ky(ik)),cabs(phi_ky(ik)),ik=0,Ny-1)
    write(spectrum_kz_unit,'(1p,1000g15.7)') time,(real(omega_phi_z(ik)),aimag(omega_phi_z(ik)),cabs(phi_z(ik)),ik=0,Nz-1)

    write(spectrum_k2_unit, '(1p,1000g15.7)') time,(phi2_k(ik),ik=1,Nx/2)
    close(spectrum_k2_unit)
    open(unit=spectrum_k2_unit, file=spectrum_k2_str, status='old', access='append')

    close(spectrum_kx_unit)
    open(unit=spectrum_kx_unit, file=spectrum_kx_str, status='old', access='append')

    close(spectrum_ky_unit)
    open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')

    close(spectrum_kz_unit)
    open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
 
    phi_z_m1 = phi_z
    phi_ky_m1 = phi_ky
    phi_kx_m1 = phi_kx

!    do ik=1,Nx/2
!       phi2_k = phi2_k(ik)*(ik*2.*pi/Lx)/number(ik)
!       write(spectrum_k2_unit,*) ik*2.*pi/Lx, phi2_k(ik)
!       close(spectrum_k2_unit)
!       open(unit=spectrum_k2_unit, file=spectrum_k2_str, status='old', access='append')
!    end do
!    !
!    do ik=0,Nx/2
!       write(spectrum_kx_unit,*) ik*2.*pi/Lx, phi_kx(ik) 
!       close(spectrum_kx_unit)
!       open(unit=spectrum_kx_unit, file=spectrum_kx_str, status='old', access='append')
!    end do
!    !
!    do ik=Ny/2+1,Ny-1
!       write(spectrum_ky_unit,*) (-Ny+ik)*(2.*pi/Ly), phi_ky(ik)
!       close(spectrum_ky_unit)
!       open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')
!    end do
!    !
!    do ik=0,Ny/2
!       write(spectrum_ky_unit,*) ik*2.*pi/Ly, phi_ky(ik)
!       close(spectrum_ky_unit)
!       open(unit=spectrum_ky_unit, file=spectrum_ky_str, status='old', access='append')
!    end do
!    !
!    do ik=Nz/2+1,Nz-1
!       write(spectrum_kz_unit,*) (-Nz+ik)*(2.*pi/Lz), phi_kz(ik)
!       close(spectrum_kz_unit)
!       open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
!    end do
!    do ik=0,Nz/2
!       write(spectrum_kz_unit,*) ik*2.*pi/Lz, phi_kz(ik)
!       close(spectrum_kz_unit)
!       open(unit=spectrum_kz_unit, file=spectrum_kz_str, status='old', access='append')
!    end do
    !
    deallocate(phi_k, phi2_k, phi_kx, phi_ky, phi_z, number)
    !
  end subroutine spectrum_phi
  !-------------------------------------------------------------------
  subroutine krook_operator(weights_in)
    !
    implicit none 
    !
    real, dimension(1:,1:), intent(inout)  :: weights_in
    !
    weights_in = weights_in*(1.- nu_krook*delta_t)
    !
  end subroutine krook_operator
  !-------------------------------------------------------------------
  subroutine reduce1 (grid)
    !
    ! sum_all_reduce for a one-dimensional array.
    !
    implicit none
    real, dimension(:) :: grid
    real, dimension(:), allocatable :: temp_grid
    integer :: l, m, i, j, icnt
    !
    l = size(grid, 1)
    !
    allocate (temp_grid(l))
    !
    icnt = 0
    do i=1,l
      icnt = icnt + 1
      temp_grid(icnt) = grid(i)
    end do
    !
     call sum_allreduce (temp_grid) ! reduce it
    !
    icnt = 0
    do i=1,l
       icnt = icnt + 1
       grid(i) = temp_grid(icnt)
    end do
    !
  end subroutine reduce1
  !-------------------------------------------------------------------
  subroutine reduce2 (grid)
    !
    ! sum_all_reduce for a two-dimensional array.
    !
    implicit none
    real, dimension(:,:) :: grid
    real, dimension(:), allocatable :: temp_grid
    integer :: l, m, i, j, icnt
    !
    l = size(grid, 1)
    m = size(grid, 2)
    !
    allocate (temp_grid(l*m))
    !
    icnt = 0
    do j=1,m
       do i=1,l
          icnt = icnt + 1
          temp_grid(icnt) = grid(i,j)
       end do
    end do
    !
     call sum_allreduce (temp_grid) ! reduce it
    !
    icnt = 0
    do j=1,m
       do i=1,l
          icnt = icnt + 1
          grid(i,j) = temp_grid(icnt)
       end do
    end do
    !
  end subroutine reduce2
  !-------------------------------------------------------------------
  subroutine reduce3 (grid)
    !
    ! sum_all_reduce for a three-dimensional array.
    !
    implicit none
    real, dimension(:,:,:) :: grid
    real, dimension(:), allocatable :: temp_grid
    integer :: l, m, n, i, j, k, icnt
    !
    l = size(grid, 1)
    m = size(grid, 2)
    n = size(grid, 3)
    !
    allocate (temp_grid(l*m*n))
    !
    icnt = 0
    do k=1,n
       do j=1,m
          do i=1,l
             icnt = icnt + 1
             temp_grid(icnt) = grid(i,j,k)
          end do
       end do
    end do
    !
    call sum_allreduce (temp_grid) ! reduce it
    !
    icnt = 0
    do k=1,n
       do j=1,m
          do i=1,l
             icnt = icnt + 1
             grid(i,j,k) = temp_grid(icnt)
          end do
       end do
    end do
    !
  end subroutine reduce3

  subroutine init_bounce
    implicit none
    real:: Bbounce

    do is = 1,num_species
       idx = 0
       do imu = 1,Nmu
          NE = NE_mu(imu)
          do j = 1,NE
             idx = idx+1
             Bbounce = p(En,idx,is)/mu_grid(imu)

             if (Bbounce .LT. B0min) then
                write(*,*) "ERROR: Particle loaded incorrectly. Should be forbidden in this magnetic field. Investigate."
                write(*,*) "is = ",is,"idx = ", idx,"En = ",p(En,idx,is),"imu = ", imu,"mu = ", mu_grid(imu), "Blocal = ", B0(p(z,idx,is)), "B0max = ", B0max, "B0min = ", B0min
                stop
             else if (Bbounce .GE. B0max) then
                p(zturn,idx,is) = -999.0
             else if (Bbounce .EQ. B0min) then
                if (B0_grid(Nz/2) .NE. B0min) then
                   write(*,*) "ERROR: B(Nz/2) must be the minimum field."
                   stop
                endif
                iz = Nz/2 
                do while(B0_grid(iz) .EQ. B0min)
                   iz = iz-1
                end do
                p(zturn,idx,is) = 0.5e0 - z_grid(iz+1)
             else if (Bbounce .EQ. B0max) then
                if (B0_grid(0) .NE. B0max) then
                   write(*,*) "ERROR: B(z=0) must be the maximum field."
                   stop
                endif
                iz = 0
                do while(B0_grid(iz) .EQ. B0max)
                   iz = iz+1
                end do
                p(zturn,idx,is) = 0.5e0*Lz - z_grid(iz-1)
             else
                p(zturn,idx,is) = 0.5e0*Lz - invB0(Bbounce)
                if ( p(zturn,idx,is) .GT. 0.5*Lz) then
                   write(*,*) "Something's wrong!",p(zturn,idx,is),Bbounce, invB0(Bbounce)
                   stop
                end if
             end if

             if ( ( p(En,idx,is) - mu_grid(imu)*B0(p(z,idx,is)) ) .LT. 0.0 ) then
                write(*,*) "ERROR: Particle loaded incorrectly. Initialized beyond its turning point. Investigate."
                write(*,*) "is = ",is,"idx = ", idx,"z = ", p(z,idx,is), "zturn = ", p(zturn,idx,is),"Diff = ",(abs(p(z,idx,is)) - abs(p(zturn,idx,is))),"eps = ",epsilon(0.0)
                stop
             end if
          end do
       end do
    end do
  end subroutine init_bounce

  function B0(z)
    !
    implicit none
    real:: B0
    real, intent(in):: z
    integer:: N0
    !
    N0 = 0
    do while ( z_grid(N0+1) .LT. z )
       N0=N0+1
    end do

    B0 = B0_grid(N0) + (z - z_grid(N0)) * (B0_grid(N0+1) - B0_grid(N0)) /delta_z
  end function
  !
  function gradB0(z)
    implicit none
    real:: gradB0
    real, intent(in):: z
    integer:: N0
    N0 = 0
    do while ( z_grid(N0+1) .LT. z )
       N0=N0+1
    end do

    gradB0 = (B0_grid(N0+1) - B0_grid(N0))/delta_z
  end function

  function invB0(B)
    implicit none
    real:: invB0
    real, intent(in):: B
    integer:: N0,N1,N2

    N0 = Nz/2
    do while ( B0_grid(N0+1) .LT. B ) ! B0 is monotonically decreasing until the symmetry point
       N0 = N0 + 1
    end do
    N2 = N0+1

    invB0 = z_grid(N0) + (B - B0_grid(N0)) * (z_grid(N2)-z_grid(N0)) / (B0_grid(N2) - B0_grid(N0))

!    invB0 = abs(  ( z_grid(N0)*(B - B0_grid(N0)) + z_grid(N2)*(B0_grid(N2) - B) ) / (B0_grid(N2) - B0_grid(N0)) )

  end function

! From:
! Ren-Raw Chen, rutgers business school
! translate from
! http://home.online.no/~pjacklam/notes/invnorm
! a routine written by John Herrero
   function invErf(x)
      implicit none
      real*4, intent(in):: x
      real*4:: invErf
      real*8 p,p_low,p_high
      real*8 a1,a2,a3,a4,a5,a6
      real*8 b1,b2,b3,b4,b5
      real*8 c1,c2,c3,c4,c5,c6
      real*8 d1,d2,d3,d4
      real*8 z,q,r
      a1=-39.6968302866538
      a2=220.946098424521
      a3=-275.928510446969
      a4=138.357751867269
      a5=-30.6647980661472
      a6=2.50662827745924
      b1=-54.4760987982241
      b2=161.585836858041
      b3=-155.698979859887
      b4=66.8013118877197
      b5=-13.2806815528857
      c1=-0.00778489400243029
      c2=-0.322396458041136
      c3=-2.40075827716184
      c4=-2.54973253934373
      c5=4.37466414146497
      c6=2.93816398269878
      d1=0.00778469570904146
      d2=0.32246712907004
      d3=2.445134137143
      d4=3.75440866190742
      
      p = 0.5d0*(dble(x)+1.d0)
      
      p_low=0.02425
      p_high=1-p_low
      
      if(p.lt.p_low) then
         q=dsqrt(-2.d0*dlog(p))
         z=(((((c1*q+c2)*q+c3)*q+c4)*q+c5)*q+c6)/((((d1*q+d2)*q+d3)*q+d4)*q+1.d0)
      else 
         if (p.le.p_high) then
            q=p-0.5d0
            r=q*q
            z=(((((a1*r+a2)*r+a3)*r+a4)*r+a5)*r+a6)*q/(((((b1*r+b2)*r+b3)*r+b4)*r+b5)*r+1.d0)
         else
            q=dsqrt(-2.d0*dlog(1-p))
            z=-(((((c1*q+c2)*q+c3)*q+c4)*q+c5)*q+c6)/((((d1*q+d2)*q+d3)*q+d4)*q+1.d0)
         end if
      end if

      invErf=real(z)/sqrt(2.e0)

   end function invErf

! Old version from wikipedia. Only accurate to 1e-4
!  function invErf(x)
!  implicit none
!  real:: invErf,a,temp1,temp2
!  real,intent(in):: x
 
!  a = 0.147
!  temp1 = (2.0/(pi*a)) + 0.5*log(1.0 - (x**2))
!  temp2 = sqrt(temp1**2 - log( 1.0 - (x**2) )/a) - temp1
!  invErf = sqrt(temp2) 
!  end function

  subroutine output_particles
  implicit none

  iOut = iOut + 1

  if (iOut .LT. 10) then
     write(unit=strnum1,fmt='(I1)') iOut 
     open(unit=70,file="data/part_t"//trim(strnum1)//".out",status="replace")
  else if (iOut .LT. 100) then
     write(unit=strnum2,fmt='(I2)') iOut 
     open(unit=70,file="data/part_t"//trim(strnum2)//".out",status="replace")
  else if (iOut .LT. 1000) then 
     write(unit=strnum3,fmt='(I3)') iOut 
     open(unit=70,file="data/part_t"//trim(strnum3)//".out",status="replace")
  else if (iOut .LT. 10000) then 
     write(unit=strnum3,fmt='(I3)') iOut 
     open(unit=70,file="data/part_t"//trim(strnum4)//".out",status="replace")
  end if

  do is = 1,num_species
     idx = 0
     do imu = 1,Nmu
        do j = 1,NE_mu(imu) 
           idx = idx+1
           if ( ( mod(idx,Npart/NpartOut) .EQ. 0) .OR. (NpartOut .LT. 0) ) then
              write(70,*) idx,p(x,idx,is),p(y,idx,is),p(z,idx,is),p(En,idx,is), mu_grid(imu), p(vz,idx,is), sqrt(2.0)*sqrt(mu_grid(imu)*B0(p(z,idx,is))), p(wgt,idx,is)
           endif
        end do
     end do
  end do
  close(70)

  end subroutine output_particles
 
  subroutine nz_diagnostics(z_p)
    implicit none
    real, dimension(1:,1:), intent(in):: z_p
    integer,dimension(:), allocatable:: numAtZ

     allocate(numAtZ(0:Nz-1))
     numAtZ = 0
     do is = 1,num_species
        do idx = 1,Npart
           iz = mod(int( z_p(idx,is)/delta_z),Nz)
!           iz = modulo(int((z_p(idx,is)/delta_z)-0.5*delta_z),Nz)
           numAtZ(iz) = numAtZ(iz) + 1
        end do
     end do

     write(nz_diagnostics_unit,'(1p,1000g15.7)') time, (B0_grid(i),real(numAtZ(i)),i=0,Nz-1)

     deallocate(numAtZ)
  end subroutine nz_diagnostics

  subroutine output_trajectories
  implicit none

  write(trajgw_unit,*) time, (p(z,idx*(Npart/NpartTraj),1),idx=1,NpartTraj)

  end subroutine output_trajectories

  subroutine particle_diagnostics()
    implicit none

    if (particle_diagnostics_on) then
       open(unit=particle_diagnostics_unit,file=particle_diagnostics_str, status='replace')
    end if

    write(particle_diagnostics_unit,*) "#is,idx,x,y,z,En,mu,vz,vperp,wgt"

    do is = 1,num_species
       idx = 0
       do imu=1,Nmu
          NE = NE_mu(imu)
          do j = 1,NE
             idx = idx+1
             write(particle_diagnostics_unit,'(1p,1000g15.7)') is,idx,p(x,idx,is),p(y,idx,is),p(z,idx,is),p(En,idx,is),mu_grid(imu), &
             p(vz,idx,is),sqrt(2.0*mu_grid(imu)*B0(p(z,idx,is))),real(mod(nint(p(z,idx,is)/delta_z),Nz)),p(wgt,idx,is), p(Ez,idx,is)
!write(*,*) idx,Npart,NE_mu(imu), p(vz,idx,is), p(En,idx,is), mu_grid(imu)*B0(p(z,idx,is)), p(En,idx,is) - mu_grid(imu)*B0(p(z,idx,is))
          end do
       end do
    end do

    close(particle_diagnostics_unit)
       
  end subroutine particle_diagnostics
     
  subroutine z_trajectory_diagnostics()
    implicit none

       is = ispec_traj
       write(z_trajectory_diagnostics_unit,'(1p,1000g15.7)') time, (p(z,k*(Npart/Nparticle_traj),is),p(vz,k*(Npart/Nparticle_traj),is),p(Ez,k*(Npart/Nparticle_traj),is),p(wgt,k*(Npart/Nparticle_traj),is),k=1,Nparticle_traj)

  end subroutine z_trajectory_diagnostics

  subroutine phi_diag_repeat(Nrun)
    implicit none
    integer,intent(in):: Nrun
    integer:: irun

!    if (proc0) open(unit=phiErrLog_unit,file=phiErrLog_str,status="replace")
!    if (proc0) close(phiErrLog_unit)

    do irun=1,Nrun_phi_diag
       call init_velocity(irun)
       if (proc0) open(unit=phiErrLog_unit,file=phiErrLog_str,access="append")
       call calculate_phi(p(wgt,:,:),p(x,:,:),p(y,:,:),p(z,:,:),p(En,:,:),phi)
       if (proc0) close(phiErrLog_unit)
    end do
 
!    if (proc0) close(phiErrLog_unit)
    stop
  end subroutine phi_diag_repeat

  subroutine calculate_coll_corr(x_gc,y_gc,z_gc,vz_gc,En_gc,nu_D,delta_nu,weight,coll_corr)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    !
    ! Compute gyroaveraged electric field with the full J0 function in k-space.
    ! Convert back to real space and find gyroaveraged velocity. See Sec. 2.3.2 in Broemstrup thesis.
    !
    integer :: idx
    integer*8, save :: plan_compl2real_2d,plan_real2compl_2d
    logical, save :: first=.true.
    !    
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc, vz_gc, En_gc,weight
    real, dimension(1:,1:), intent(inout)::coll_corr, nu_D, delta_nu
    !    
    !
    real :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, mu_temp, z_temp, B0local,nu_Dei,fac1,fac2,fac3,fac4,fac5,fac6,x,G
    real:: g1, g2, g3, g4, g5, g6, g7, g8
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2,imu
    real:: erf
    !    
    real,dimension(:,:,:), allocatable :: grid1,grid2,grid3,grid4,grid5,grid6,gnrm
    complex,dimension(:,:,:,:), allocatable, save:: UL1_corr, UD1_corr, UE_corr
    complex,dimension(:,:), allocatable:: UL1_corr_temp, UD1_corr_temp, UE_corr_temp
    real, dimension(:), allocatable, save:: UL_denom, UD_denom, UE_denom
    real:: UL_denom_temp, UD_denom_temp, UE_denom_temp, delta_nu_temp,En_temp,nu_D_temp,nu_E

    complex,dimension(:,:,:,:),allocatable:: UL0_int, UL1_int, UD0_int, UD1_int, UE_int
    complex,dimension(:,:,:), allocatable::Cei_int 
    complex,dimension(:,:), allocatable::  UL0_int_temp, UL1_int_temp, UD0_int_temp, UD1_int_temp, UE_int_temp,Cei_int_temp

    complex,dimension(:,:),allocatable:: UL0_k,UL1_k,UD0_k,UD1_k,UE_k,Cei_k
    real,dimension(:,:,:),allocatable:: UL0,UL1,UD0,UD1,UE,Cei

    allocate(grid1(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(grid2(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(grid3(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gnrm(0:Nx-1,0:Ny-1,0:Nz-1))

    !
    if (first) then
       ! call rfftwnd_f77_create_plan (plan_k2real,3,Msize,fftw_complex_to_real,fftw_estimate)
       call rfftwnd_f77_create_plan (plan_real2compl_2d,2,Msize(1:2),fftw_forward,0) 
       call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0) 

       allocate (UL_denom(1:num_species))
       allocate (UD_denom(1:num_species))
       allocate (UE_denom(1:num_species))
       allocate (UL1_corr(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
       allocate (UD1_corr(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
       allocate (UE_corr(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))

       ! Calculate the corrections necessary to convert df to h

       do is=1,num_species
          do idx=1,Npart
             x = sqrt(En_gc(idx,is))
             G = (erf(x) - (2.0*x/sqrt(pi))*exp(-x*x))/(2.0*x**2)
             nu_D(idx,is) = nu_coll(is)*(erf(x)- G)/x**3
             delta_nu(idx,is) = nu_D(idx,is) - nu_coll(is)*4.0*G/x 
          end do
       end do
 
       if (h_convert_mc) then
          allocate (UL1_corr_temp(0:Nx/2,0:Ny-1))
          allocate (UD1_corr_temp(0:Nx/2,0:Ny-1))
          allocate (UE_corr_temp(0:Nx/2,0:Ny-1))

          UL1_corr = 0.0
          UD1_corr = 0.0
          UE_corr = 0.0

          do is = 1,num_species
             idx = 0
             do imu=1,Nmu
                gnrm(:,:,:) = 0.e0
                grid1(:,:,:) = 0.e0
                grid2(:,:,:) = 0.e0
                grid3(:,:,:) = 0.e0
                NE = NE_mu(imu)
                do j=1,NE
                   idx = idx+1
   
                   L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
                   M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
                   N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1                 
                   L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)

                   xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
                   ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
                   zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
                   !            
                   g1 = (xl1 - xgc) * (ym1 - ygc) * (zn1 - zgc)
                   g2 = (xgc - xl0) * (ym1 - ygc) * (zn1 - zgc)
                   !            
                   g3 = (xl1 - xgc) * (ygc - ym0) * (zn1 - zgc)
                   g4 = (xgc - xl0) * (ygc - ym0) * (zn1 - zgc)
                   !             
                   g5 = (xl1 - xgc) * (ym1 - ygc) * (zgc - zn0)
                   g6 = (xgc - xl0) * (ym1 - ygc) * (zgc - zn0)
                   !             
                   g7 = (xl1 - xgc) * (ygc - ym0) * (zgc - zn0)
                   g8 = (xgc - xl0) * (ygc - ym0) * (zgc - zn0)

                   nu_D_temp = nu_D(idx,is) 
                   delta_nu_temp = delta_nu(idx,is)
                   En_temp = En_gc(idx,is)
                   nu_E = delta_nu_temp* ((0.5/En_temp)-2.0) - (0.5/En_temp)*nu_D_temp  
                   

                   fac1 = Volume_scale*nu_D_temp
                   fac2 = Volume_scale*delta_nu_temp
                   fac3 = Volume_scale*nu_E*En_temp
 
                   grid1(L0,M0,N0) = grid1(L0,M0,N0) + g1*fac1
                   grid2(L0,M0,N0) = grid2(L0,M0,N0) + g1*fac2
                   grid3(L0,M0,N0) = grid3(L0,M0,N0) + g1*fac3
                   gnrm(L0,M0,N0) = gnrm(L0,M0,N0) + g1*Volume_scale
                   grid1(L2,M0,N0) = grid1(L2,M0,N0) + g2*fac1
                   grid2(L2,M0,N0) = grid2(L2,M0,N0) + g2*fac2  
                   grid3(L2,M0,N0) = grid3(L2,M0,N0) + g2*fac3
                   gnrm(L2,M0,N0) = gnrm(L2,M0,N0) + g2*Volume_scale
                   !            
                   grid1(L0,M2,N0) = grid1(L0,M2,N0) + g3*fac1  
                   grid2(L0,M2,N0) = grid2(L0,M2,N0) + g3*fac2  
                   grid3(L0,M2,N0) = grid3(L0,M2,N0) + g3*fac3
                   gnrm(L0,M2,N0) = gnrm(L0,M2,N0) + g3*Volume_scale
                   grid1(L2,M2,N0) = grid1(L2,M2,N0) + g4*fac1  
                   grid2(L2,M2,N0) = grid2(L2,M2,N0) + g4*fac2  
                   grid3(L2,M2,N0) = grid3(L2,M2,N0) + g4*fac3
                   gnrm(L2,M2,N0) = gnrm(L2,M2,N0) + g4*Volume_scale
                   !            
                   grid1(L0,M0,N2) = grid1(L0,M0,N2) + g5*fac1  
                   grid2(L0,M0,N2) = grid2(L0,M0,N2) + g5*fac2  
                   grid3(L0,M0,N2) = grid3(L0,M0,N2) + g5*fac3
                   gnrm(L0,M0,N2) = gnrm(L0,M0,N2) + g5*Volume_scale
                   grid1(L2,M0,N2) = grid1(L2,M0,N2) + g6*fac1  
                   grid2(L2,M0,N2) = grid2(L2,M0,N2) + g6*fac2  
                   grid3(L2,M0,N2) = grid3(L2,M0,N2) + g6*fac3
                   gnrm(L2,M0,N2) = gnrm(L2,M0,N2) + g6*Volume_scale
                   !            
                   grid1(L0,M2,N2) = grid1(L0,M2,N2) + g7*fac1  
                   grid2(L0,M2,N2) = grid2(L0,M2,N2) + g7*fac2  
                   grid3(L0,M2,N2) = grid3(L0,M2,N2) + g7*fac3
                   gnrm(L0,M2,N2) = gnrm(L0,M2,N2) + g7*Volume_scale
                   grid1(L2,M2,N2) = grid1(L2,M2,N2) + g8*fac1  
                   grid2(L2,M2,N2) = grid2(L2,M2,N2) + g8*fac2  
                   grid3(L2,M2,N2) = grid3(L2,M2,N2) + g8*fac3
                   gnrm(L2,M2,N2) = gnrm(L2,M2,N2) + g8*Volume_scale
                end do

                call reduce3(grid1)
                call reduce3(grid2)
                call reduce3(grid3)
                call reduce3(gnrm)
          
                ! Normalize the MC integration and integrate over discrete mu
                do iz = 0,Nz-1
                   where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                      grid1(:,:,iz) = mcNorm(iz,imu)* grid1(:,:,iz) /gnrm(:,:,iz)
                      grid2(:,:,iz) = mcNorm(iz,imu)* grid2(:,:,iz) /gnrm(:,:,iz)
                      grid3(:,:,iz) = mcNorm(iz,imu)* grid3(:,:,iz) /gnrm(:,:,iz)
                   end where 

                   call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid1(:,:,iz), UL1_corr_temp(:,:))
                   call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid2(:,:,iz), UD1_corr_temp(:,:))
                   call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid3(:,:,iz), UE_corr_temp(:,:))

                   do iky = 0,Ny-1
                      do ikx = 0,Nx/2
                      !
                         UL1_corr(ikx,iky,iz,is) = UL1_corr(ikx,iky,iz,is) + UL1_corr_temp(ikx,iky)*sqrt(mu_grid(imu))*J1(ikx,iky,iz,imu,is)*J0(ikx,iky,iz,imu,is) * dmu(imu)
                         UD1_corr(ikx,iky,iz,is) = UD1_corr(ikx,iky,iz,is) + UD1_corr_temp(ikx,iky)*sqrt(mu_grid(imu))*J1(ikx,iky,iz,imu,is)*J0(ikx,iky,iz,imu,is) * dmu(imu)
                         UE_corr(ikx,iky,iz,is) = UE_corr(ikx,iky,iz,is) + UE_corr_temp(ikx,iky)*J0(ikx,iky,iz,imu,is)**2 * dmu(imu)

                      end do
                   end do ! End kx/ky loop
                end do    ! End z loop
             end do ! End mu loop
             UL1_corr(:,:,:,is) = UL1_corr(:,:,:,is) * Zs(is)/temperature(is)
             UD1_corr(:,:,:,is) = UD1_corr(:,:,:,is) * Zs(is)/temperature(is)
             UE_corr(:,:,:,is) = UE_corr(:,:,:,is) * Zs(is)/temperature(is)
          end do ! End species loop
          
          deallocate(UL1_corr_temp, UD1_corr_temp, UE_corr_temp)

       else
          UL1_corr = 0.0
          UD1_corr = 0.0
          UE_corr = 0.0
       endif

       UL_denom = 0.0
       UD_denom = 0.0
       UE_denom = 0.0
       do is = 1,num_species
          idx = 0
          do imu = 1,Nmu
             UL_denom_temp = 0.0
             UD_denom_temp = 0.0
             UE_denom_temp = 0.0
             NE = NE_mu(imu)
             do j=1,NE
                idx = idx+1
 
                nu_E = delta_nu(idx,is)* ((0.5/En_gc(idx,is))-2.0) - (0.5/En_gc(idx,is))*nu_D(idx,is)
                UL_denom_temp = UL_denom_temp + nu_D(idx,is)*vz_gc(idx,is)**2
                UD_denom_temp = UD_denom_temp + delta_nu(idx,is)*vz_gc(idx,is)**2
                UE_denom_temp = UE_denom_temp + nu_E*En_gc(idx,is)**2
             end do
             if (NE .NE. 0) then
                UL_denom(is) = UL_denom(is) + UL_denom_temp*dmu(imu)/real(NE)
                UD_denom(is) = UD_denom(is) + UD_denom_temp*dmu(imu)/real(NE)
                UE_denom(is) = UE_denom(is) + UE_denom_temp*dmu(imu)/real(NE)
             else
                write(*,*) "ERROR: Collision operator will not work if there is a void mu."
             end if
          end do
          write(*,*) "UL_denom(",is,") = ", UL_denom(is)
          write(*,*) "UD_denom(",is,") = ", UD_denom(is)
          write(*,*) "UE_denom(",is,") = ", UE_denom(is)
       end do

       first=.false.
    end if


    allocate(grid4(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(grid5(0:Nx-1,0:Ny-1,0:Nz-1))
 
    allocate(UL0_int_temp(0:Nx/2,0:Ny-1))
    allocate(UD0_int_temp(0:Nx/2,0:Ny-1))
    allocate(UL1_int_temp(0:Nx/2,0:Ny-1))
    allocate(UD1_int_temp(0:Nx/2,0:Ny-1))
    allocate(UE_int_temp(0:Nx/2,0:Ny-1))
    allocate(UL0_int(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
    allocate(UD0_int(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
    allocate(UL1_int(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
    allocate(UD1_int(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))
    allocate(UE_int(0:Nx/2,0:Ny-1,0:Nz-1,1:num_species))

    if (coll_ei_on) then
       allocate(grid6(0:Nx-1,0:Ny-1,0:Nz-1))
       allocate(Cei_int_temp(0:Nx/2,0:Ny-1))
       allocate(Cei_int(0:Nx/2,0:Ny-1,0:Nz-1))
       Cei_int = 0.0
    end if

    UL0_int = 0.0
    UD0_int = 0.0
    UL1_int = 0.0
    UD1_int = 0.0
    UE_int = 0.0

    do is = 1,num_species
       idx = 0
       do imu=1,Nmu
          gnrm(:,:,:) = 0.e0
          grid1(:,:,:) = 0.e0
          grid2(:,:,:) = 0.e0
          grid3(:,:,:) = 0.e0
          grid4(:,:,:) = 0.e0
          grid5(:,:,:) = 0.e0
          if (coll_ei_on) grid6(:,:,:) = 0.e0

          NE = NE_mu(imu)
          do j=1,NE
             idx = idx+1
 
             L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1                 
             L2 = mod(L1, NX)  ;  M2 = mod(M1, NY) ;  N2 = mod(N1, NZ)

             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
             !            
             g1 = (xl1 - xgc) * (ym1 - ygc) * (zn1 - zgc)
             g2 = (xgc - xl0) * (ym1 - ygc) * (zn1 - zgc)
             !            
             g3 = (xl1 - xgc) * (ygc - ym0) * (zn1 - zgc)
             g4 = (xgc - xl0) * (ygc - ym0) * (zn1 - zgc)
             !             
             g5 = (xl1 - xgc) * (ym1 - ygc) * (zgc - zn0)
             g6 = (xgc - xl0) * (ym1 - ygc) * (zgc - zn0)
             !             
             g7 = (xl1 - xgc) * (ygc - ym0) * (zgc - zn0)
             g8 = (xgc - xl0) * (ygc - ym0) * (zgc - zn0)

             nu_D_temp = nu_D(idx,is) 
             delta_nu_temp = delta_nu(idx,is)
             En_temp = En_gc(idx,is)
             nu_E = delta_nu_temp* ((0.5/En_temp)-2.0) - (0.5/En_temp)*nu_D_temp  

             fac1 = Volume_scale*nu_D_temp*weight(idx,is)
             fac2 = fac1*vz_gc(idx,is)
             fac3 = Volume_scale*delta_nu_temp*weight(idx,is)
             fac4 = fac3*vz_gc(idx,is)
             fac5 = Volume_scale*nu_E*En_temp*weight(idx,is)

             if (coll_ei_on) then 
                fac6 = Volume_scale*vz_gc(idx,is)*weight(idx,is)
                grid6(L0,M0,N0) = grid6(L0,M0,N0) + g1*fac6
                grid6(L2,M0,N0) = grid6(L2,M0,N0) + g2*fac6
                grid6(L0,M2,N0) = grid6(L0,M2,N0) + g3*fac6  
                grid6(L2,M2,N0) = grid6(L2,M2,N0) + g4*fac6  
                grid6(L0,M0,N2) = grid6(L0,M0,N2) + g5*fac6  
                grid6(L2,M0,N2) = grid6(L2,M0,N2) + g6*fac6  
                grid6(L0,M2,N2) = grid6(L0,M2,N2) + g7*fac6  
                grid6(L2,M2,N2) = grid6(L2,M2,N2) + g8*fac6  
             end if

             grid1(L0,M0,N0) = grid1(L0,M0,N0) + g1*fac1
             grid2(L0,M0,N0) = grid2(L0,M0,N0) + g1*fac2
             grid3(L0,M0,N0) = grid3(L0,M0,N0) + g1*fac3
             grid4(L0,M0,N0) = grid4(L0,M0,N0) + g1*fac4
             grid5(L0,M0,N0) = grid5(L0,M0,N0) + g1*fac5
             gnrm(L0,M0,N0) = gnrm(L0,M0,N0) + g1*Volume_scale
             grid1(L2,M0,N0) = grid1(L2,M0,N0) + g2*fac1
             grid2(L2,M0,N0) = grid2(L2,M0,N0) + g2*fac2  
             grid3(L2,M0,N0) = grid3(L2,M0,N0) + g2*fac3
             grid4(L2,M0,N0) = grid4(L2,M0,N0) + g2*fac4  
             grid5(L2,M0,N0) = grid5(L2,M0,N0) + g2*fac5
             gnrm(L2,M0,N0) = gnrm(L2,M0,N0) + g2*Volume_scale
             !            
             grid1(L0,M2,N0) = grid1(L0,M2,N0) + g3*fac1  
             grid2(L0,M2,N0) = grid2(L0,M2,N0) + g3*fac2  
             grid3(L0,M2,N0) = grid3(L0,M2,N0) + g3*fac3  
             grid4(L0,M2,N0) = grid4(L0,M2,N0) + g3*fac4  
             grid5(L0,M2,N0) = grid5(L0,M2,N0) + g3*fac5  
             gnrm(L0,M2,N0) = gnrm(L0,M2,N0) + g3*Volume_scale
             grid1(L2,M2,N0) = grid1(L2,M2,N0) + g4*fac1  
             grid2(L2,M2,N0) = grid2(L2,M2,N0) + g4*fac2  
             grid3(L2,M2,N0) = grid3(L2,M2,N0) + g4*fac3  
             grid4(L2,M2,N0) = grid4(L2,M2,N0) + g4*fac4  
             grid5(L2,M2,N0) = grid5(L2,M2,N0) + g4*fac5  
             gnrm(L2,M2,N0) = gnrm(L2,M2,N0) + g4*Volume_scale
             !            
             grid1(L0,M0,N2) = grid1(L0,M0,N2) + g5*fac1  
             grid2(L0,M0,N2) = grid2(L0,M0,N2) + g5*fac2  
             grid3(L0,M0,N2) = grid3(L0,M0,N2) + g5*fac3  
             grid4(L0,M0,N2) = grid4(L0,M0,N2) + g5*fac4  
             grid5(L0,M0,N2) = grid5(L0,M0,N2) + g5*fac5  
             gnrm(L0,M0,N2) = gnrm(L0,M0,N2) + g5*Volume_scale
             grid1(L2,M0,N2) = grid1(L2,M0,N2) + g6*fac1  
             grid2(L2,M0,N2) = grid2(L2,M0,N2) + g6*fac2  
             grid3(L2,M0,N2) = grid3(L2,M0,N2) + g6*fac3  
             grid4(L2,M0,N2) = grid4(L2,M0,N2) + g6*fac4  
             grid5(L2,M0,N2) = grid5(L2,M0,N2) + g6*fac5  
             gnrm(L2,M0,N2) = gnrm(L2,M0,N2) + g6*Volume_scale
             !            
             grid1(L0,M2,N2) = grid1(L0,M2,N2) + g7*fac1  
             grid2(L0,M2,N2) = grid2(L0,M2,N2) + g7*fac2  
             grid3(L0,M2,N2) = grid3(L0,M2,N2) + g7*fac3  
             grid4(L0,M2,N2) = grid4(L0,M2,N2) + g7*fac4  
             grid5(L0,M2,N2) = grid5(L0,M2,N2) + g7*fac5  
             gnrm(L0,M2,N2) = gnrm(L0,M2,N2) + g7*Volume_scale
             grid1(L2,M2,N2) = grid1(L2,M2,N2) + g8*fac1  
             grid2(L2,M2,N2) = grid2(L2,M2,N2) + g8*fac2  
             grid3(L2,M2,N2) = grid3(L2,M2,N2) + g8*fac3  
             grid4(L2,M2,N2) = grid4(L2,M2,N2) + g8*fac4  
             grid5(L2,M2,N2) = grid5(L2,M2,N2) + g8*fac5  
             gnrm(L2,M2,N2) = gnrm(L2,M2,N2) + g8*Volume_scale
          end do

          call reduce3(grid1)
          call reduce3(grid2)
          call reduce3(grid3)
          call reduce3(grid4)
          call reduce3(grid5)
          if (coll_ei_on .and. (is .eq. 1) ) call reduce3(grid6)
          call reduce3(gnrm)
       
          ! Normalize the MC integration and integrate over discrete mu
          do iz = 0,Nz-1
             where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                grid1(:,:,iz) = mcNorm(iz,imu)* grid1(:,:,iz) /gnrm(:,:,iz)
                grid2(:,:,iz) = mcNorm(iz,imu)* grid2(:,:,iz) /gnrm(:,:,iz)
                grid3(:,:,iz) = mcNorm(iz,imu)* grid3(:,:,iz) /gnrm(:,:,iz)
                grid4(:,:,iz) = mcNorm(iz,imu)* grid4(:,:,iz) /gnrm(:,:,iz)
                grid5(:,:,iz) = mcNorm(iz,imu)* grid5(:,:,iz) /gnrm(:,:,iz)
             end where 
             if (coll_ei_on .AND. (is .EQ. 1)) then 
                where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                   grid6(:,:,iz) = mcNorm(iz,imu)* grid6(:,:,iz) /gnrm(:,:,iz)
                end where
             end if

             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid1(:,:,iz), UL1_int_temp(:,:))
             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid2(:,:,iz), UL0_int_temp(:,:))
             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid3(:,:,iz), UD1_int_temp(:,:))
             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid4(:,:,iz), UD0_int_temp(:,:))
             call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid5(:,:,iz), UE_int_temp(:,:))
             if (coll_ei_on .AND. (is .EQ. 1) ) call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, grid6(:,:,iz), Cei_int_temp(:,:))
             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                !
                   UL1_int(ikx,iky,iz,is) = UL1_int(ikx,iky,iz,is) + UL1_int_temp(ikx,iky)*sqrt(mu_grid(imu))*J1(ikx,iky,iz,imu,is) * dmu(imu)
                   UL0_int(ikx,iky,iz,is) = UL0_int(ikx,iky,iz,is) + UL0_int_temp(ikx,iky)*J0(ikx,iky,iz,imu,is) * dmu(imu)
                   UD1_int(ikx,iky,iz,is) = UD1_int(ikx,iky,iz,is) + UD1_int_temp(ikx,iky)*sqrt(mu_grid(imu))*J1(ikx,iky,iz,imu,is) * dmu(imu)
                   UD0_int(ikx,iky,iz,is) = UD0_int(ikx,iky,iz,is) + UD0_int_temp(ikx,iky)*J0(ikx,iky,iz,imu,is) * dmu(imu)
                   UE_int(ikx,iky,iz,is) = UE_int(ikx,iky,iz,is) + UE_int_temp(ikx,iky)*J0(ikx,iky,iz,imu,is)*dmu(imu)
                   if (coll_ei_on .AND. (is .EQ. 1) ) Cei_int(ikx,iky,iz) = Cei_int(ikx,iky,iz) + Cei_int_temp(ikx,iky)*dmu(imu)
                end do
             end do ! End kx/ky loop
          end do    ! End z loop
       end do ! End mu loop
    end do ! End species loop

    deallocate(gnrm,grid1,grid2,grid3,grid4,grid5, UL0_int_temp,UL1_int_temp,UD0_int_temp,UD1_int_temp,UE_int_temp)

 
    allocate(UL0_k(0:Nx/2,0:Ny-1))
    allocate(UL1_k(0:Nx/2,0:Ny-1))
    allocate(UD0_k(0:Nx/2,0:Ny-1))
    allocate(UD1_k(0:Nx/2,0:Ny-1))
    allocate(UE_k(0:Nx/2,0:Ny-1))
    allocate(UL0(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(UL1(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(UD0(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(UD1(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(UE(0:Nx-1,0:Ny-1,0:Nz-1))

    if (coll_ei_on) then
       deallocate(grid6,Cei_int_temp)
       allocate(Cei_k(0:Nx/2,0:Ny-1))
       allocate(Cei(0:Nx-1,0:Ny-1,0:Nz-1))
    end if
    !
    do is=1,num_species
    !    
       idx = 0
       do imu=1,Nmu  ! for each vperp value...
          mu_temp = mu_grid(imu)
          do iz = 0,Nz-1
             do iky=0,Ny-1
                do ikx=0,Nx/2
                   UL0_k(:,:) = J0(:,:,iz,imu,is)*UL0_int(:,:,iz,is) / (real(Nx*Ny)*UL_denom(is))
                   UL1_k(:,:) = 2.0*J1(:,:,iz,imu,is)*B0_grid(iz)*sqrt(mu_temp)*(UL1_int(:,:,iz,is)+phi(:,:,iz)*UL1_corr(:,:,iz,is)) / (real(Nx*Ny)*UL_denom(is))
                   UD0_k(:,:) = -J0(:,:,iz,imu,is)*UD0_int(:,:,iz,is) / (real(Nx*Ny)*UD_denom(is))
                   UD1_k(:,:) = -2.0*J1(:,:,iz,imu,is)*B0_grid(iz)*sqrt(mu_temp)*(UD1_int(:,:,iz,is)+phi(:,:,iz)*UD1_corr(:,:,iz,is)) / (real(Nx*Ny)*UD_denom(is))
                   UE_k(:,:) = J0(:,:,iz,imu,is)*(UE_int(:,:,iz,is)+phi(:,:,iz)*UE_corr(:,:,iz,is)) / (real(Nx*Ny)*UE_denom(is))
                   if (coll_ei_on .AND. (is .EQ. 2)) Cei_k(:,:) = vts(1)*J0(:,:,iz,imu,2)*Cei_int(:,:,iz)/(density(1)*vts(2)* real(Nx*Ny))

               end do
             end do
             !          
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, UL0_k(:,:), UL0(:,:,iz))
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, UL1_k(:,:), UL1(:,:,iz))
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, UD0_k(:,:), UD0(:,:,iz))
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, UD1_k(:,:), UD1(:,:,iz))
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, UE_k(:,:), UE(:,:,iz))
             if (coll_ei_on .AND. (is .EQ. 2)) then
                call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, Cei_k(:,:), Cei(:,:,iz))
             end if
          end do
          !          
          NE = NE_mu(imu)
          do k=1,NE            
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1
             !             
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
             !             
             L2 = mod(L1, NX)  ;  M2 = mod(M1, NY)  ;  N2 = mod(N1, NZ)
             !             
             nu_D_temp = nu_D(idx,is) 
             delta_nu_temp = delta_nu(idx,is)
             En_temp = En_gc(idx,is)
             nu_E = delta_nu_temp* ((0.5/En_temp)-2.0) - (0.5/En_temp)*nu_D_temp  

             coll_corr(idx,is) = 0.0
             if (pitch_angle_coll_on) then
                coll_corr(idx,is) = coll_corr(idx,is) + nu_D_temp*vz_gc(idx,is)*efield(UL0(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
                coll_corr(idx,is) = coll_corr(idx,is) + nu_D_temp*efield(UL1(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             end if
             if (energy_diff_coll_on) then
                coll_corr(idx,is) = coll_corr(idx,is) + delta_nu_temp*vz_gc(idx,is)*efield(UD0(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
                coll_corr(idx,is) = coll_corr(idx,is) + delta_nu_temp*efield(UD1(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
                coll_corr(idx,is) = coll_corr(idx,is) + nu_E*En_temp*efield(UE(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             end if

             if (coll_ei_on .AND. (is .EQ. 2)) then
                nu_Dei = nu_coll(2)* (density(1)/density(2))*Zs(1)**2* sqrt((2.0/En_temp)**3)
                coll_corr(idx,is) = coll_corr(idx,is) + vz_gc(idx,is)*nu_Dei*efield(Cei(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             end if
          end do
       end do
    end do

    deallocate( UL0_k,UL1_k,UD0_k,UD1_k,UE_k,UL0,UL1,UD0,UD1,UE)
    if (coll_ei_on) then
       deallocate(Cei_k,Cei)
    end if
    
!    deallocate(UL_denom, UD_denom, UE_denom,UL1_corr,UD1_corr,UE_corr)
    !
  end subroutine calculate_coll_corr

  subroutine wgt_diagnostics()
    use mp, only: sum_reduce, nproc,proc0
    implicit none
    integer:: is,idx, wgt_p
    real,dimension(:), allocatable:: avg_wgt, avg_wgt_sq, var_rel, uz, ux, uy, heatflux_x, heatflux_y
    real:: vz_part, En_part

    allocate(avg_wgt(num_species))
    allocate(avg_wgt_sq(num_species))
    allocate(var_rel(num_species))
    allocate(uz(num_species))
    allocate(ux(num_species))
    allocate(uy(num_species))
    allocate(heatflux_x(num_species))
    allocate(heatflux_y(num_species))

    if (use_actual_df) then
       wgt_p = wgt_df
    else
       wgt_p = wgt
    end if

    avg_wgt(:) = 0.0
    avg_wgt_sq(:) = 0.0
    ux(:) = 0.0
    uy(:) = 0.0
    uz(:) = 0.0
    heatflux_x(:) = 0.0
    heatflux_y(:) = 0.0

    do is = 1,num_species
       do idx = 1,Npart
          avg_wgt(is) = avg_wgt(is) + p(wgt_p,idx,is)
          avg_wgt_sq(is) = avg_wgt_sq(is) + (p(wgt_p,idx,is)**2)
          uz(is) = uz(is) + p(wgt_p,idx,is)*p(vz,idx,is)       
          ux(is) = ux(is) + p(wgt_p,idx,is)*p(vx,idx,is)       
          uy(is) = uy(is) + p(wgt_p,idx,is)*p(vy,idx,is)       
          heatflux_x(is) = heatflux_x(is) + p(wgt_p,idx,is)*p(vx,idx,is)*p(En,idx,is)
          heatflux_y(is) = heatflux_y(is) + p(wgt_p,idx,is)*p(vy,idx,is)*p(En,idx,is)
       end do

       if (superparticle_conserve) then
          p(wgt_p,idx_conserve,is) = - (avg_wgt(is) - p(wgt,idx_conserve,is))/real((Npart-1)*nproc)
          vz_part = - ( uz(is) - p(vz,idx_conserve,is)*p(wgt_p,idx_conserve,is) )/real((Npart-1)*nproc)
          En_part = mu_grid(imu_conserve) * B0(p(z,Npart,is)) + 0.5*vz_part**2
          if ( En_part .GT. Emax ) then
             write(*,*) "WARNING: Superparticle energy > Emax. Bad things may happen."
             write(*,*) "Conserving particle energy = ", En_part, " vz = ", vz_part
             if ( .NOT. superparticle_fail_cont ) stop
          end if
          p(vz,idx_conserve,is) = vz_part
          p(En,idx_conserve,is) = En_part
       end if
    end do
 
    call sum_reduce (avg_wgt,0) 
    call sum_reduce (avg_wgt_sq,0) 
    call sum_reduce (uz,0) 
    call sum_reduce (ux,0) 
    call sum_reduce (uy,0) 

    ux = ux/real(Npart*nproc)
    uy = uy/real(Npart*nproc)
    uz = uz/real(Npart*nproc)
    heatflux_x = heatflux_x/real(Npart*nproc)
    heatflux_y = heatflux_y/real(Npart*nproc)
    avg_wgt = avg_wgt/real(Npart*nproc)
    avg_wgt_sq = avg_wgt_sq/real(Npart*nproc)

    if (proc0) then
       do is =1,num_species
          if (avg_wgt(is) .NE. 0.0) then
             var_rel(is) = (avg_wgt_sq(is) - avg_wgt(is)**2)/abs(avg_wgt(is))

             if (var_rel(is) .LT. 0.0) write(*,*) "Variance negative!"
          else
             var_rel(is) = 0.0
          end if
       end do 

 
       open (unit=wgt_unit, file=wgt_str, access='append')
       write(wgt_unit,'(1p,1000g15.7)') time,(sqrt(var_rel(is)/real(Npart*nproc)),avg_wgt_sq(is),avg_wgt(is),ux(is),uy(is),uz(is),heatflux_x(is),heatflux_y(is),is=1,num_species)
       close(wgt_unit)
    end if
   
    deallocate(avg_wgt, avg_wgt_sq,var_rel)
  end subroutine wgt_diagnostics

  subroutine phiplot(phi_in)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    complex,dimension(0:,0:,0:),intent(in):: phi_in
    complex,dimension(:,:,:),allocatable:: phi_local
    integer:: ikx, iky, ix,iy,iz
    real, dimension(:,:,:), allocatable:: phi_real
    integer*8, save::plan_compl2real_2d
    integer, save:: nout_phiplot = 0
    character(len=40):: nout_str
    logical, save:: first = .true.
!    character(len=40)::phiplot_str



!    write(unit=nout_str,fmt='(I0)') nout_phiplot
!    phiplot_str = "phiplotdir/"// trim(runname) // "." // trim(nout_str)
    if (first) then
       open(unit=phiplot_unit,file=phiplot_str,status="replace")
       close(phiplot_unit)
       first = .false.
    end if

    open(unit=phiplot_unit,file=phiplot_str,status="old",access="append")

    if (phiplot_real) then
       if (nout_phiplot .EQ. 0) then
          call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0) 
       end if
 
       write(*,*) "Plotting phi(x,y)"

       allocate(phi_real(0:Nx-1,0:Ny-1,0:Nz - 1))
       allocate(phi_local(0:Nx/2,0:Ny-1,0:Nz-1))

       phi_local = phi_in ! fftw overwrites input array
       
       do iz=0,Nz-1
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, phi_local(:,:,iz), phi_real(:,:,iz))
       end do
       phi_real = phi_real / real(Nx*Ny)

       do iy = 0,Ny-1
          do ix = 0,Nx-1
             write(phiplot_unit,'(100g15.7)') x_grid(ix),y_grid(iy),(phi_real(ix,iy,iz),iz=0,Nz-1)
          end do
          write(phiplot_unit,'(A1)') " "
       end do
       deallocate(phi_local,phi_real)
    else
       write(*,*) "Plotting phi(kx,ky)"
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             write(phiplot_unit,'(100g15.7)') kx(ikx),ky(iky),(phi_in(ikx,iky,iz),iz=0,Nz-1)
          end do
          write(phiplot_unit,'(A1)') " "
       end do
    end if
  
    write(phiplot_unit,'(A1)') " "

    nout_phiplot = nout_phiplot + 1
    close(phiplot_unit)

  end subroutine phiplot

  subroutine phiplot_z(phi_in)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    complex,dimension(0:,0:,0:),intent(in):: phi_in
    complex,dimension(:,:,:),allocatable:: phi_local, phi_k
    integer:: ikx, iky, ix,iy,iz
    real, dimension(:,:,:), allocatable:: phi_real
    integer*8, save::plan_compl2real_2d,plan_compl2compl_1d
    integer, save:: nout_phiplot = 0
    character(len=40):: nout_str
    logical, save:: first = .true.
!    character(len=40)::phiplot_str



!    write(unit=nout_str,fmt='(I0)') nout_phiplot
!    phiplot_str = "phiplotdir/"// trim(runname) // "." // trim(nout_str)
    if (first) then
       open(unit=phiplot_unit,file=phiplot_str,status="replace")
       close(phiplot_unit)
       first = .false.
    end if

    open(unit=phiplot_unit,file=phiplot_str,status="old",access="append")

    if (phiplot_real) then
       if (nout_phiplot .EQ. 0) then
          call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0) 
       end if
 
       write(*,*) "Plotting phi(x,y)"

       allocate(phi_real(0:Nx-1,0:Ny-1,0:Nz - 1))
       allocate(phi_local(0:Nx/2,0:Ny-1,0:Nz-1))

       phi_local = phi_in ! fftw overwrites input array
       
       do iz=0,Nz-1
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, phi_local(:,:,iz), phi_real(:,:,iz))
       end do
       phi_real = phi_real / real(Nx*Ny)

       do iy = 0,Ny-1
          do iz = 0,Nz-1
             write(phiplot_unit,'(100g15.7)') z_grid(iz),y_grid(iy),(phi_real(ix,iy,iz),ix=0,Nx-1)
          end do
          write(phiplot_unit,'(A1)') " "
       end do
       deallocate(phi_local,phi_real)
    else
       allocate(phi_k(0:Nx/2,0:Ny-1,0:Nz-1))

       if (nout_phiplot .EQ. 0) then
          call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       end if
      
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             call fftwnd_f77_one (plan_compl2compl_1d, phi(ikx,iky,:), phi_k(ikx,iky,:))
          end do
       end do
       phi_k = phi_k / real(Nz)
 
       write(*,*) "Plotting phi(kx,ky)"
       do iky = 0,Ny-1
          do ikz = 0,Nz-1
             write(phiplot_unit,'(100g15.7)') kz(ikz),ky(iky),(phi_k(ikx,iky,ikz),ikx=0,Nx/2)
          end do
          write(phiplot_unit,'(A1)') " "
       end do
       deallocate(phi_k)
    end if
  
    write(phiplot_unit,'(A1)') " "

    nout_phiplot = nout_phiplot + 1
    close(phiplot_unit)

  end subroutine phiplot_z

  subroutine init_wgt()
    implicit none
    real:: xi_part, dxi
    
    if (init_wgt_mode_r .EQ. 1) then
      ! Single out one k if the option is chosen
       p(wgt,:,:) = init_wgt_mag
       if (init_kx .GT. -1000) then 
          write(*,*) "Initializing a single mode along x."
          do is = 1,num_species
             do idx = 1,Npart
                p(wgt,idx,is) = p(wgt,idx,is)*cos( kx(init_kx)*p(x,idx,is))
             end do
          end do
       end if
       if (init_ky .GT. -1000) then
          write(*,*) "Initializing a single mode along y."
          do is = 1,num_species
             do idx = 1,Npart
                p(wgt,idx,is) = p(wgt,idx,is)*cos( ky(init_ky)*p(y,idx,is))
             end do
          end do
       end if
       if (init_kz .GT. -1000) then 
          write(*,*) "Initializing a single mode along z."
          do is = 1,num_species
             do idx = 1,Npart
                p(wgt,idx,is) = p(wgt,idx,is)*cos( kz(init_kz)*p(z,idx,is))
             end do
          end do
       end if
    end if
 
    if (init_wgt_mode_v .EQ. 1) then
       do is = 1,num_species
          idx = 0
          do imu = 1,Nmu
             NE = NE_mu(imu)
             do j = 1,NE
                idx = idx+1
                xi_part = sqrt( (p(En,idx,is) - mu_grid(imu)*B0(p(z,idx,is)) )/p(En,idx,is))*sign(1.0,p(vz,idx,is))
                dxi = sqrt(2.0*p(En,idx,is))*sqrt(1.0 - init_xi0**2)
                if (init_v0 .GT. -1000.0) then 
                   if (init_xi0 .GT. -1000.0) then 
                      ! Single spot
                      if ( abs(sqrt(2.0*p(En,idx,is)) - init_v0) .LT. init_v_width) then
                         if (abs(xi_part - init_xi0) .LT. init_v_width) then 
                            p(wgt,idx,is) = init_wgt_mag
                         else
                            p(wgt,idx,is) = 0.0
                         end if
                      else
                         p(wgt,idx,is) = 0.0
                      end if
                   else
                      ! Energy band
                      if ( abs(sqrt(2.0*p(En,idx,is)) - init_v0) .LT. init_v_width) then
                         p(wgt,idx,is) = init_wgt_mag
                      else
                         p(wgt,idx,is) = 0.0
                      end if
                   end if
                else
                   if (init_xi0 .GT. -1000.0) then 
                      ! Xi band
                      if (abs(xi_part - init_xi0) .LT. init_v_width) then 
                         p(wgt,idx,is) = init_wgt_mag
                      else
                         p(wgt,idx,is) = 0.0
                      end if
                   end if
                end if
             end do
          end do
       end do
    end if

!    if (abs(T_marker - 1.0) .GT. epsilon(0.0)) then
!       p(wgt,:,:) = p(wgt,:,:)*p(wgt_adjust,:,:)
!    end if
       

  end subroutine init_wgt

  subroutine fill_grid_3D_real(Nx,Ny,Nz,grid_in)
    implicit none
    integer,intent(in):: Nx,Ny,Nz
    real,dimension(0:,0:,0:):: grid_in
    real, dimension(:,:,:), allocatable:: grid
    real:: sum 
    integer:: i,j,k

    allocate(grid(-1:Nx,-1:Ny,-1:Nz))
    grid(:,:,:) = 0.0
    grid(0:Nx-1,0:Ny-1,0:Nz-1) = grid_in(0:Nx-1,0:Ny-1,0:Nz-1)

    do k=0,Nz-1
       do j=0,Ny-1
          do i=0,Nx-1
             if ( abs(grid(i,j,k)) .LT. epsilon(0.0) ) then
                sum = 0.0  
                sum = sum + grid(i,j,k-1) + grid(i,j,k+1)
                sum = sum + grid(i,j-1,k) + grid(i,j+1,k)
                sum = sum + grid(i-1,j,k) + grid(i+1,j,k)
                grid_in(i,j,k) = sum/6.0
             end if
          end do
       end do
    end do

  end subroutine fill_grid_3D_real

  subroutine fill_grid_2D_real(Nx,Ny,grid_in)
    implicit none
    integer,intent(in):: Nx,Ny
    real,dimension(0:,0:):: grid_in
    real, dimension(:,:), allocatable:: grid
    real:: sum 
    integer:: i,j

    allocate(grid(-1:Nx,-1:Ny))
    grid(:,:) = 0.0
    grid(0:Nx-1,0:Ny-1) = grid_in(0:Nx-1,0:Ny-1)

    do j=0,Ny-1
       do i=0,Nx-1
          if ( abs(grid(i,j)) .LT. epsilon(0.0) ) then
             sum = 0.0  
             sum = sum + grid(i,j-1) + grid(i,j+1)
             sum = sum + grid(i-1,j) + grid(i+1,j)
             grid_in(i,j) = sum/4.0
          end if
       end do
    end do

  end subroutine fill_grid_2D_real

 subroutine calculate_phiApar(weight, x_gc, y_gc, z_gc, En_gc, vz_gc,phi,Apar)
    ! Similar to calculate_phi, except A_parallel is also calculated from gyroaveraged Ampere's Law
    use fft_work, only: fftw_forward
    implicit none
    !
    real, dimension(1:,1:),intent(inout) :: weight
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc, En_gc, vz_gc
    complex, dimension(0:,0:,0:):: phi, Apar
    !    
    real :: g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13, g14, g15, g16
    real, dimension(8) :: gg
    real,dimension(:), allocatable:: F0_num
    real, dimension(:,:,:), allocatable :: ngrid,jgrid, gnrm
    complex, dimension(:,:), allocatable :: rho_k, jpar_k
    complex, dimension(:,:), allocatable :: filter    !
    complex :: J0_rho_k, testfac
    !
    real  :: fac1, fac2, dfac, test, ErrLow, fac3, mu_fac_phi, mu_fac_Apar
    real  :: xgc, ygc, zgc, xl0, xl1, ym0, ym1, zn0, zn1, mgc, mk0,mk1,mk2
    integer :: idx, L0, L1, L2, M0, M1, M2, N0, N1, N2, is,mk
    integer :: ivperp, ivz, icell, J0idx
    integer, dimension(1) :: ig
    integer*8, save :: plan_real2compl_1d
    integer*8, save :: plan_real2compl_2d, plan_inv
    logical, save :: beginning=.true.
    integer, save :: phi_subroutine_count = 0
    ! 
    complex :: numerator
    real:: J0val,Enorm,halfvz2,vperp,vperpweight,mtest,m_temp,analytic
    real:: B0inv,argNorm,k2,J0arg,J0val1,J0val2,normfac,jacNorm,maxanalytic, Gamma_0
    real:: erf
    !
    allocate (ngrid(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (jgrid(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (gnrm(0:Nx-1,0:Ny-1,0:Nz-1)) 
    allocate (rho_k(0:Nx/2,0:Ny-1))  
    allocate (jpar_k(0:Nx/2,0:Ny-1))  
    if (numerical_denom) allocate (F0_num(0:Nz-1))

    phi = 0.0
    Apar = 0.0
    !
    phi_subroutine_count = phi_subroutine_count + 1
    !
    if(beginning) then
       !
       call rfftwnd_f77_create_plan (plan_real2compl_2d,2,Msize(1:2),fftw_forward,0) 

       phiDenom = 0.0
       AparDenom = 0.0

       ! Note that the drift kinetic option is not any more efficient than the default GK mode
       ! If serious drift kinetic work is desired, the code would need a little redesign or more extensive options
       if (drift_kinetic) then
          J0 = 1.0
       end if
      
       do ikx = 0,Nx/2
          do iky = 0,Ny-1
             do iz = 0,Nz-1
                if (num_species .EQ. 1) then
                   write(*,*) "ERROR: Apar calculation not implemented for adiabatic electrons yet"
                   stop 

                   if (drift_kinetic) then
                      Gamma_0 = 1.0 - k2_perp_shaped(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2
                   else
                      call gamma_n(k2_perp_shaped(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2, Gamma_0)
                      !call gamma0_function(k2_perp(ikx,iky)*temperature(1)*mass(1)/(Zs(1)*B0_grid(iz))**2, Gamma_0)
                   end if
                   phiDenom(ikx,iky,iz) = (tau + Zs(1)*(1.0 - Gamma_0))
                else 
                   AparDenom(ikx,iky,iz) = 2.0*k2_perp_shaped(ikx,iky) /beta
                   do is=1,num_species
                      if (drift_kinetic) then
                         if (is .EQ. 2) then
                            Gamma_0 = 1.0
                         else
                            Gamma_0 = 1.0 - k2_perp_shaped(ikx,iky)
                         end if
                      else
                         call gamma_n(k2_perp_shaped(ikx,iky)*temperature(is)*mass(is)/(Zs(is)*B0_grid(iz))**2, Gamma_0)
                      end if
                      phiDenom(ikx,iky,iz) = phiDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/temperature(is))*(1.0-Gamma_0)
                      AparDenom(ikx,iky,iz) = AparDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/mass(is))*Gamma_0
                   end do
                end if
             end do
          end do
       end do
           
    end if

    if (numerical_denom) then
       phiDenom = 0.0
       do iz = 0,Nz-1
          AparDenom(:,:,iz) = 2.0*k2_perp_shaped(:,:) /beta
       end do
    end if

    ! For each vperp value, interpolate the charge to the x,y,z grid.  Integrates over v_parallel.
    do is=1,num_species
       idx = 0
       do i = 1,Nmu
          gnrm(:,:,:) = 0.e0
          ngrid(:,:,:) = 0.e0
          jgrid(:,:,:) = 0.e0

          mu_fac_phi = Zs(is)*density(is)*dmu(i)
          mu_fac_Apar = mu_fac_phi*vts(is)
          
          NE = NE_mu(i)

          ! For each discrete mu, deposit the particles onto a 3D grid,
          ! Monte Carlo integrating in the process
          do j = 1,NE
             idx = idx+1

             L0 = mod(int(x_gc(idx,is)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,is)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,is)/delta_z),Nz)  ;  N1 = N0 + 1                 
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny) ; N2 = mod(N1,Nz)

             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,is)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,is)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,is)
             !            
             g1 = (xl1 - xgc) * (ym1 - ygc) * (zn1 - zgc)
             g2 = (xgc - xl0) * (ym1 - ygc) * (zn1 - zgc)
             !            
             g3 = (xl1 - xgc) * (ygc - ym0) * (zn1 - zgc)
             g4 = (xgc - xl0) * (ygc - ym0) * (zn1 - zgc)
             !             
             g5 = (xl1 - xgc) * (ym1 - ygc) * (zgc - zn0)
             g6 = (xgc - xl0) * (ym1 - ygc) * (zgc - zn0)
             !             
             g7 = (xl1 - xgc) * (ygc - ym0) * (zgc - zn0)
             g8 = (xgc - xl0) * (ygc - ym0) * (zgc - zn0)

             fac2 =Volume_scale  
             if (abs(T_marker-1.0) .GE. epsilon(0.0))  then
                fac1 = fac2*weight(idx,is)*p(wgt_adjust,idx,is)
             else
                fac1 = fac2*weight(idx,is)
             end if
             fac3 = fac1*vz_gc(idx,is)
                

             ngrid(L0,M0,N0) = ngrid(L0,M0,N0) + g1*fac1
             gnrm(L0,M0,N0) = gnrm(L0,M0,N0) + g1*fac2
             jgrid(L0,M0,N0) = jgrid(L0,M0,N0) + g1*fac3
             
             ngrid(L2,M0,N0) = ngrid(L2,M0,N0) + g2*fac1
             gnrm(L2,M0,N0) = gnrm(L2,M0,N0) + g2*fac2
             jgrid(L2,M0,N0) = jgrid(L2,M0,N0) + g2*fac3
             
             ngrid(L0,M2,N0) = ngrid(L0,M2,N0) + g3*fac1
             gnrm(L0,M2,N0) = gnrm(L0,M2,N0) + g3*fac2
             jgrid(L0,M2,N0) = jgrid(L0,M2,N0) + g3*fac3
             
             ngrid(L2,M2,N0) = ngrid(L2,M2,N0) + g4*fac1
             gnrm(L2,M2,N0) = gnrm(L2,M2,N0) + g4*fac2
             jgrid(L2,M2,N0) = jgrid(L2,M2,N0) + g4*fac3
             
             ngrid(L0,M0,N2) = ngrid(L0,M0,N2) + g5*fac1
             gnrm(L0,M0,N2) = gnrm(L0,M0,N2) + g5*fac2
             jgrid(L0,M0,N2) = jgrid(L0,M0,N2) + g5*fac3
             
             ngrid(L2,M0,N2) = ngrid(L2,M0,N2) + g6*fac1
             gnrm(L2,M0,N2) = gnrm(L2,M0,N2) + g6*fac2
             jgrid(L2,M0,N2) = jgrid(L2,M0,N2) + g6*fac3
             
             ngrid(L0,M2,N2) = ngrid(L0,M2,N2) + g7*fac1
             gnrm(L0,M2,N2) = gnrm(L0,M2,N2) + g7*fac2
             jgrid(L0,M2,N2) = jgrid(L0,M2,N2) + g7*fac3
             
             ngrid(L2,M2,N2) = ngrid(L2,M2,N2) + g8*fac1
             gnrm(L2,M2,N2) = gnrm(L2,M2,N2) + g8*fac2
             jgrid(L2,M2,N2) = jgrid(L2,M2,N2) + g8*fac3
             
             if (numerical_denom) then
                F0_num(N0) = F0_num(N0) + (zn1-zgc)*fac2*delta_x*delta_y
                F0_num(N2) = F0_num(N2) + (zgc-zn0)*fac2*delta_x*delta_y
             end if
          end do

          call reduce3(ngrid)
          call reduce3(jgrid)
          call reduce3(gnrm)
          if (numerical_denom) call sum_allreduce(F0_num)
          
          ! Normalize the MC integration and integrate over discrete mu
          do iz = 0,Nz-1
             if (gnrm_on) then
                where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                   ngrid(:,:,iz) = mcNorm(iz,i)* ngrid(:,:,iz) /gnrm(:,:,iz)
                   jgrid(:,:,iz) = mcNorm(iz,i)* jgrid(:,:,iz) /gnrm(:,:,iz)
                end where 
             else
                where( abs(gnrm(:,:,iz)) .GT. epsilon(0.0) )
                   ngrid(:,:,iz) = mcNorm(iz,i)* ngrid(:,:,iz) /real(Ncell*Nx*Ny)
                   jgrid(:,:,iz) = mcNorm(iz,i)* jgrid(:,:,iz) /real(Ncell*Nx*Ny)
                end where 
             end if
          end do

          if (numerical_denom) F0_num = F0_num / real(Ncell)
 
!          call fill_grid_3D_real(Nx,Ny,Nz,ngrid) 
!          call fill_grid_3D_real(Nx,Ny,Nz,jgrid) 
 
          do iz = 0,Nz-1
             if (make_poisson_well_posed) then
                ngrid(:,:,iz) = ngrid(:,:,iz) - sum(ngrid(:,:,iz))/real(Nx*Ny)
                jgrid(:,:,iz) = jgrid(:,:,iz) - sum(jgrid(:,:,iz))/real(Nx*Ny)
             end if
            call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, ngrid(:,:,iz), rho_k(:,:))
            call rfftwnd_f77_one_real_to_complex (plan_real2compl_2d, jgrid(:,:,iz), jpar_k(:,:))

             do iky = 0,Ny-1
                do ikx = 0,Nx/2
                   !
                   phi(ikx,iky,iz) = phi(ikx,iky,iz) + mu_fac_phi* J0(ikx,iky,iz,i,is)*rho_k(ikx,iky)
                   Apar(ikx,iky,iz) = Apar(ikx,iky,iz) + mu_fac_Apar*J0(ikx,iky,iz,i,is)*jpar_k(ikx,iky) 

                   if (numerical_denom) then
                      phiDenom(ikx,iky,iz) = phiDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/temperature(is))*(1.0 - J0(ikx,iky,iz,i,is)**2)*dmu(i)*mcNorm(iz,i)*F0_num(iz)
                      AparDenom(ikx,iky,iz) = AparDenom(ikx,iky,iz) + (Zs(is)**2*density(is)/mass(is))*J0(ikx,iky,iz,i,is)**2*dmu(i)*mcNorm(iz,i)*F0_num(iz)
                   end if

                end do
             end do ! End kx/ky loop
          end do    ! End z loop
       end do ! End mu loop
    end do ! End species loop

    if (phi_diagnostics_on .AND. proc0 .AND. (mod(step,2) .EQ. 0)) call phi_diagnostics(phi)

    where ( abs(phiDenom) .GT. epsilon(0.0))
       phi = phi/(phiDenom )   
    else where
       phi = 0.0
    end where

    where ( abs(AparDenom) .GT. epsilon(0.0))
       Apar = Apar/(AparDenom )   
    else where
       Apar = 0.0
    end where

    if (k0_method .EQ. 0) then
       phi(0,0,:) = 0.0
       Apar(0,0,:) = 0.0
    else if (k0_method .EQ. 1) then
       do iz = 0,Nz-1
          phi(:,:,iz) = phi(:,:,iz) - phi(0,0,iz)
          Apar(:,:,iz) = Apar(:,:,iz) - Apar(0,0,iz)
       end do
    end if

    ! Single out one k if the option is chosen
    ! Not implemented for Apar yet
!    if (spectrum_diagnostics_on) then
!       if (init_kx .GT. -1000) then 
!          phi(0:init_kx-1,:,:) = 0.0
!          phi(init_kx+1:Nx/2,:,:) = 0.0
!       end if
!       if (init_ky .GT. -1000) then 
!          phi(:,0:init_ky-1,:) = 0.0
!          phi(:,init_ky+1:Ny-init_ky-1,:) = 0.0
!          phi(:,Ny-init_ky+1 : Ny-1, :) = 0.0
!       end if
!    end if

    if (beginning) beginning = .false.

    deallocate(rho_k,jpar_k, ngrid, jgrid,gnrm)
    if (numerical_denom) deallocate(F0_num)
    !
  end subroutine calculate_phiApar

 subroutine calculate_electric_field(phi,x_gc, y_gc, z_gc, vx_gc, vy_gc,Epar_gc)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    !
    ! Compute gyroaveraged electric field with the full J0 function in k-space.
    ! Convert back to real space and find gyroaveraged velocity. See Sec. 2.3.2 in Broemstrup thesis.
    !
    integer :: idx, izm1, izp1
    integer*8, save :: plan_compl2real_2d,plan_real2compl_3d,plan_compl2real_3d
    integer*8, save::plan_compl2compl_1d
    integer*8, save::plan_compl2complInv_1d
    logical, save :: first=.true.
    !    
    complex, dimension(0:,0:,0:) :: phi
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc
    real, dimension(1:,1:), intent (out) :: vx_gc, vy_gc, Epar_gc
    !    
    complex, dimension(:), allocatable:: J0_phi_k, J0_phi_z
    complex, dimension(:,:,:), allocatable :: J0gradxPhi_k,J0gradyPhi_k,J0gradzPhi_k
    real, dimension(:,:,:), allocatable :: gradxPhi_x,gradyPhi_x,gradzPhi_x, phiGyav_x
    !
    real :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, mu_temp, z_temp, B0local, prefac, df_correction, Gamma_0
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2,J0idx,imu
    !    
    allocate(J0gradxPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradyPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradzPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    !    
    allocate(gradxPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradyPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradzPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))

    allocate(J0_phi_k(0:Nz-1))
    allocate(J0_phi_z(0:Nz-1))
    !    
    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       call rfftwnd_f77_create_plan (plan_compl2real_3d,3,Msize,fftw_backward,0)
       call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0)
       first=.false.
    end if

    do j=1,num_species
       idx = 0
       prefac = Zs(j)/temperature(j)
       do imu=1,Nmu  ! for each vperp value...
          mu_temp = mu_grid(imu)
          do iky=0,Ny-1
             do ikx=0,Nx/2
                do iz = 0,Nz-1
                   J0_phi_z(iz) = J0(ikx,iky,iz,imu,j)*phi(ikx,iky,iz)/(real(Nx*Ny*Nz)*B0_grid(iz))
                end do
                ! If mu-averaging, be sure to multiply by Gamma_0
                if ( (Nmu .LE. 1) .AND. .NOT.drift_kinetic) then
                   call gamma_n(k2_perp_shaped(ikx,iky)*temperature(is)*mass(is)/(Zs(is)*B0_grid(iz))**2, Gamma_0)
                   J0_phi_z(iz) = J0_phi_z(iz)*Gamma_0
                end if

                call fftwnd_f77_one (plan_compl2compl_1d, J0_phi_z(:), J0_phi_k(:))

                do ikz = 0,Nz-1

                   J0gradxPhi_k(ikx,iky,ikz) = zi*kx(ikx)*k_shape_x(ikx)*J0_phi_k(ikz)
                   J0gradyPhi_k(ikx,iky,ikz) = zi*ky(iky)*k_shape_y(iky)*J0_phi_k(ikz)
                   J0gradzPhi_k(ikx,iky,ikz) = zi*kz(ikz)*k_shape_z(ikz)*J0_phi_k(ikz)

                   if ( use_mask .AND. ( ( abs(kx(ikx)) .GT. (2.0/3.0)*kx(Nx/2) ) .OR. &
                   ( abs(ky(iky)) .GT. (2.0/3.0)*ky(Ny/2)) .OR. ( abs(kz(ikz)) .GT. (2.0/3.0)*kz(Nz/2)) ) ) then
                      J0gradxPhi_k(ikx,iky,ikz) = 0.0
                      J0gradyPhi_k(ikx,iky,ikz) = 0.0
                      J0gradzPhi_k(ikx,iky,ikz) = 0.0
                  
                   end if

                end do
             end do
          end do

          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradxPhi_k(:,:,:), gradxPhi_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradyPhi_k(:,:,:), gradyPhi_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradzPhi_k(:,:,:), gradzPhi_x(:,:,:))
!             if (use_actual_df) then
!                call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0_phi_z(:,:,iz), phiGyav_x(:,:,iz))
!             end if

          do iz=0,Nz-1
             gradzPhi_x(:,:,iz) = gradzPhi_x(:,:,iz)*B0_grid(iz)            
          end do

          NE = NE_mu(imu)
          do k=1,NE            
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,j)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,j)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,j)/delta_z),Nz)  ;  N1 = N0 + 1
             !             
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,j)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,j)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,j)
             !             
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny)  ;  N2 = mod(N1, Nz)
             !             
             vx_gc(idx,j)  =  -efield(gradyPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             vy_gc(idx,j)  = efield(gradxPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) ! minus b/c of cross product
             Epar_gc(idx,j) = -efield(gradzPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)

             if (use_actual_df) then
                write(*,*) "ERROR: use_actual_df option not working with electromagnetic"
                stop
!                df_correction = efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!                df_correction = df_correction - vz_gc(idx,j)*efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!
                p(wgt_df,idx,j) = p(wgt,idx,j) + prefac*df_correction
             end if
          end do
       end do
    end do
    !
    deallocate(J0gradxPhi_k,J0gradyPhi_k,J0gradzPhi_k,gradxPhi_x,gradyPhi_x,gradzPhi_x, J0_phi_k,J0_phi_z)
    if (use_actual_df) deallocate(phiGyav_x)
    !
  end subroutine calculate_electric_field


 subroutine calculate_EM_field(phi,Apar, x_gc, y_gc, z_gc, vz_gc, vx_gc, vy_gc,Epar_gc)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    !
    ! Compute gyroaveraged electric field with the full J0 function in k-space.
    ! Convert back to real space and find gyroaveraged velocity. See Sec. 2.3.2 in Broemstrup thesis.
    !
    integer :: idx, izm1, izp1
    integer*8, save :: plan_compl2real_2d,plan_real2compl_3d,plan_compl2real_3d
    integer*8, save::plan_compl2compl_1d
    integer*8, save::plan_compl2complInv_1d
    logical, save :: first=.true.
    !    
    complex, dimension(0:,0:,0:) :: phi, Apar
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc, vz_gc
    real, dimension(1:,1:), intent (out) :: vx_gc, vy_gc, Epar_gc
    !    
    complex, dimension(:), allocatable:: J0_phi_k, J0_Apar_k, J0_phi_z, J0_Apar_z 
    complex, dimension(:,:,:), allocatable :: J0gradxPhi_k,J0gradyPhi_k,J0gradzPhi_k,J0gradxApar_k,J0gradyApar_k,J0gradzApar_k
    real, dimension(:,:,:), allocatable :: gradxPhi_x,gradyPhi_x,gradzPhi_x,gradxApar_x,gradyApar_x,gradzApar_x, phiGyav_x, AparGyav_x
    !
    real :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, mu_temp, z_temp, B0local, prefac, df_correction
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2,J0idx,imu
    !    
    allocate(J0gradxPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradyPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradzPhi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradxApar_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradyApar_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0gradzApar_k(0:Nx/2,0:Ny-1,0:Nz-1))
    !    
    allocate(gradxPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradyPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradzPhi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradxApar_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradyApar_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(gradzApar_x(0:Nx-1,0:Ny-1,0:Nz-1))

    allocate(J0_phi_k(0:Nz-1))
    allocate(J0_Apar_k(0:Nz-1))

    allocate(J0_phi_z(0:Nz-1))
    allocate(J0_Apar_z(0:Nz-1))
    !    
!    if (use_actual_df) then
!       allocate(phiGyav_x(0:Nx-1,0:Ny-1,0:Nz-1))
!       allocate(AparGyav_x(0:Nx/2,0:Ny-1,0:Nz-1))
!    end if
    !
    if (first) then
       ! call rfftwnd_f77_create_plan (plan_k2real,3,Msize,fftw_complex_to_real,fftw_estimate)
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       call rfftwnd_f77_create_plan (plan_compl2real_3d,3,Msize,fftw_backward,0)
       call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0)
       first=.false.
    end if
    !    
    do j=1,num_species
       idx = 0
       prefac = Zs(j)/temperature(j)
       do imu=1,Nmu  ! for each vperp value...
          mu_temp = mu_grid(imu)
          do iky=0,Ny-1
             do ikx=0,Nx/2
                do iz = 0,Nz-1
                   J0_phi_z(iz) = J0(ikx,iky,iz,imu,j)*phi(ikx,iky,iz)/(real(Nx*Ny*Nz)*B0_grid(iz))
                   J0_Apar_z(iz) = J0(ikx,iky,iz,imu,j)*Apar(ikx,iky,iz)/(real(Nx*Ny*Nz)*B0_grid(iz))
                end do

                call fftwnd_f77_one (plan_compl2compl_1d, J0_phi_z(:), J0_phi_k(:))
                call fftwnd_f77_one (plan_compl2compl_1d, J0_Apar_z(:), J0_Apar_k(:))

                do ikz = 0,Nz-1

                   J0gradxPhi_k(ikx,iky,ikz) = zi*kx(ikx)*k_shape_x(ikx)*J0_phi_k(ikz)
                   J0gradyPhi_k(ikx,iky,ikz) = zi*ky(iky)*k_shape_y(iky)*J0_phi_k(ikz)
                   J0gradzPhi_k(ikx,iky,ikz) = zi*kz(ikz)*k_shape_z(ikz)*J0_phi_k(ikz)
                   
                   J0gradxApar_k(ikx,iky,ikz) = zi*kx(ikx)*k_shape_x(ikx)*J0_Apar_k(ikz)
                   J0gradyApar_k(ikx,iky,ikz) = zi*ky(iky)*k_shape_y(iky)*J0_Apar_k(ikz)
                   J0gradzApar_k(ikx,iky,ikz) = zi*kz(ikz)*k_shape_z(ikz)*J0_Apar_k(ikz)

                   if ( use_mask .AND. ( ( abs(kx(ikx)) .GT. (2.0/3.0)*kx(Nx/2) ) .OR. &
                   ( abs(ky(iky)) .GT. (2.0/3.0)*ky(Ny/2)) .OR. ( abs(kz(ikz)) .GT. (2.0/3.0)*kz(Nz/2)) ) ) then
                      J0gradxPhi_k(ikx,iky,ikz) = 0.0
                      J0gradyPhi_k(ikx,iky,ikz) = 0.0
                      J0gradzPhi_k(ikx,iky,ikz) = 0.0
                  
                      J0gradxApar_k(ikx,iky,ikz) = 0.0
                      J0gradyApar_k(ikx,iky,ikz) = 0.0
                      J0gradzApar_k(ikx,iky,ikz) = 0.0
                   end if

                end do
             end do
          end do

          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradxPhi_k(:,:,:), gradxPhi_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradyPhi_k(:,:,:), gradyPhi_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradzPhi_k(:,:,:), gradzPhi_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradxApar_k(:,:,:), gradxApar_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradyApar_k(:,:,:), gradyApar_x(:,:,:))
          call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, J0gradzApar_k(:,:,:), gradzApar_x(:,:,:))
!             if (use_actual_df) then
!                call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0phi_k(:,:,iz), phiGyav_x(:,:,iz))
!                call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0Apar_k(:,:,iz), AparGyav_x(:,:,iz))
!             end if

          do iz=0,Nz-1
             gradzPhi_x(:,:,iz) = gradzPhi_x(:,:,iz)*B0_grid(iz)            
             gradzApar_x(:,:,iz) = gradzApar_x(:,:,iz)*B0_grid(iz)            
          end do

          NE = NE_mu(imu)
          do k=1,NE            
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,j)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,j)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,j)/delta_z),Nz)  ;  N1 = N0 + 1
             !             
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,j)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,j)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,j)
             !             
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny)  ;  N2 = mod(N1, Nz)
             !             
             vx_gc(idx,j)  =  -efield(gradyPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             vy_gc(idx,j)  = efield(gradxPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) ! minus b/c of cross product
             Epar_gc(idx,j) = -efield(gradzPhi_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)

             vx_gc(idx,j) = vx_gc(idx,j) + vts(j)*vz_gc(idx,j)*efield(gradyApar_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             vy_gc(idx,j) = vy_gc(idx,j) - vts(j)*vz_gc(idx,j)*efield(gradxApar_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 
             Epar_gc(idx,j) = Epar_gc(idx,j) + vts(j)*vz_gc(idx,j)*efield(gradzApar_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale) 

             if (use_actual_df) then
                write(*,*) "ERROR: use_actual_df option not working with electromagnetic"
                stop
!                df_correction = efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!                df_correction = df_correction - vz_gc(idx,j)*efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!
!                p(wgt_df,idx,j) = p(wgt,idx,j) + prefac*df_correction
             end if
          end do
       end do
    end do
    !
    deallocate(J0gradxPhi_k,J0gradyPhi_k,J0gradzPhi_k,J0gradxApar_k,J0gradyApar_k,J0gradzApar_k,gradxPhi_x,gradyPhi_x,gradzPhi_x, &
              gradxApar_x,gradyApar_x,gradzApar_x, J0_phi_k,J0_Apar_k,J0_phi_z, J0_Apar_z)
!    if (use_actual_df) deallocate(phiGyav_x,AparGyav_x)
    !
  end subroutine calculate_EM_field

   subroutine spectrum_apar(apar_in)
    !
    ! Diagnostic output for apar spectrum.
    !
    use mp, only: proc0
    use fft_work, only: fftw_forward, fftw_backward
    !
    implicit none
    integer*8,save:: fftw_plan_z, fftw_plan_invz
    complex, dimension(0:,0:,0:), intent(inout) :: apar_in
    complex,dimension(:),allocatable:: apar_temp
    real, dimension(:), allocatable :: apar_kx, apar_ky, apar_kz, apar2_k
    real:: apar2_tot
    real, dimension(:),save, allocatable :: apar_kx_m1, apar_kx_m2, apar_ky_m1, apar_ky_m2, apar_kz_m1, apar_kz_m2
    real,save:: apar2_tot_m1, apar2_tot_m2
    real, dimension(:,:), allocatable :: apar_k
    real :: tmp, apar_sq, apar_sq_m1, apar_sq_m2
    integer, dimension(:), allocatable :: number
    logical,save:: first = .true.
    integer :: ikx, iky, ikz, ik
    complex:: omega_temp
    real,save:: omega_apar2_tot, gamma_apar2_tot
    real, dimension(:),save, allocatable:: omega_apar_kz, omega_apar_ky, omega_apar_kx
    real, dimension(:),save, allocatable:: gamma_apar_kz, gamma_apar_ky, gamma_apar_kx
    !

    allocate(apar_kx(0:Nx/2))
    allocate(apar_ky(0:Ny-1))
    allocate(apar_kz(0:Nz-1))
    allocate(apar_k(0:Nx/2,0:Ny-1))
    allocate(apar2_k(1:Nx/2))
    allocate(apar_temp(0:Nz-1))
    allocate(number(1:Nx/2))

    if (first) then
       allocate(apar_kx_m1(0:Nx/2))
       allocate(apar_kx_m2(0:Nx/2))
       allocate(apar_ky_m1(0:Ny-1))
       allocate(apar_ky_m2(0:Ny-1))
       allocate(apar_kz_m1(0:Nz-1))
       allocate(apar_kz_m2(0:Nz-1))
       allocate(omega_apar_kx(0:Nx/2))
       allocate(omega_apar_ky(0:Ny-1))
       allocate(omega_apar_kz(0:Nz-1))
       allocate(gamma_apar_kx(0:Nx/2))
       allocate(gamma_apar_ky(0:Ny-1))
       allocate(gamma_apar_kz(0:Nz-1))
       write(spectrum_k2_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, apar:",(ik*2.*pi/Lx,", ",ik=1,Nx/2)
       write(spectrum_kx_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, apar:",(ik*2.*pi/Lx,", ",ik=0,Nx/2) 
       write(spectrum_ky_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, apar:", (ik*2.*pi/Ly,ik=0,Ny/2),( (-Ny+ik)*(2.*pi/Ly),", ",ik=Ny/2+1,Ny-1)
       write(spectrum_kz_unit,'(1p,A30,1000g15.7)') "# time, omega, gamma, apar:", (ik*2.*pi/Lz,ik=0,Nz/2),( (-Nz+ik)*(2.*pi/Lz),", ",ik=Nz/2+1,Nz-1)
       omega_apar_kx = 0.0
       omega_apar_ky = 0.0
       omega_apar_kz = 0.0
       gamma_apar_kx = 0.0
       gamma_apar_ky = 0.0
       gamma_apar_kz = 0.0
       apar_kx_m1    = 0.0
       apar_ky_m1    = 0.0
       apar_kz_m1    = 0.0
       apar_kx_m2    = 0.0
       apar_ky_m2    = 0.0
       apar_kz_m2    = 0.0

       call fftwnd_f77_create_plan (fftw_plan_z,1,Msize(3),fftw_forward,0) 
       call fftwnd_f77_create_plan (fftw_plan_invz,1,Msize(3),fftw_backward,0) 

       first = .false.
    end if
    !
    number    = 0 
    apar_k     = 0.0
    apar2_k    = 0.0
    apar_kx    = 0.0
    apar_ky    = 0.0
    apar_kz    = 0.0

    
    !
    do ikz=0,Nz-1
       do iky=0,Ny-1
          do ikx=0,Nx/2
             apar_k(ikx,iky) = apar_k(ikx,iky) + real(apar_in(ikx,iky,ikz)*conjg(apar_in(ikx,iky,ikz)))
             apar_kx(ikx)    = apar_kx(ikx) + real(apar_in(ikx,iky,ikz)*conjg(apar_in(ikx,iky,ikz)))

             apar_ky(iky)    = apar_ky(iky) + real(apar_in(ikx,iky,ikz)*conjg(apar_in(ikx,iky,ikz)))
             apar_kz(ikz)    = apar_kz(ikz) + real(apar_in(ikx,iky,ikz)*conjg(apar_in(ikx,iky,ikz)))
          end do
       end do
    end do

    do iky = 0,Ny-1
       if ( (apar_ky_m1(iky) .NE. 0.0) .AND. ( (2.0 - (apar_ky(iky)/apar_ky_m1(iky)) - (apar_ky_m2(iky)/apar_ky_m1(iky))) .GT. 0.0 ) ) then
          omega_apar_ky(iky) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (apar_ky(iky)/apar_ky_m1(iky)) - (apar_ky_m2(iky)/apar_ky_m1(iky))) + (1.0-alpha_ewa)*omega_apar_ky(iky)
       else
          omega_apar_ky(iky) = 0.0 + (1.0 - alpha_ewa)*omega_apar_ky(iky)
       end if
        
       if (apar_ky_m1(iky) .NE. apar_ky(iky)) then
          gamma_apar_ky(iky) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(apar_ky(iky)-apar_ky_m1(iky))/(apar_ky(iky)+apar_ky_m1(iky)) + (1.0 - alpha_ewa)*gamma_apar_ky(iky)
       else
          gamma_apar_ky(iky) = 0.0
       end if
    end do

    do ikx = 0,Nx/2
       if ( (apar_ky_m1(ikx) .NE. 0.0) .AND. ( (2.0 - (apar_kx(ikx)/apar_kx_m1(ikx)) - (apar_kx_m2(ikx)/apar_kx_m1(ikx))) .GT. 0.0 ) ) then
          omega_apar_kx(ikx) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (apar_kx(ikx)/apar_kx_m1(ikx)) - (apar_kx_m2(ikx)/apar_kx_m1(ikx))) + (1.0-alpha_ewa)*omega_apar_kx(ikx)
       else
          omega_apar_kx(ikx) = 0.0 + (1.0 - alpha_ewa)*omega_apar_kx(ikx)
       end if
       if (apar_kx_m1(ikx) .NE. apar_kx(ikx)) then
          gamma_apar_kx(ikx) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(apar_kx(ikx)-apar_kx_m1(ikx))/(apar_kx(ikx)+apar_kx_m1(ikx)) + (1.0 - alpha_ewa)*gamma_apar_kx(ikx)
       else
          gamma_apar_kx(ikx) = 0.0 + (1.0 - alpha_ewa)*gamma_apar_kx(ikx)
       end if
    end do

    do ikz = 0,Nz-1
       if ( (apar_kz_m1(ikz) .NE. 0.0) .AND. ( (2.0 - (apar_kz(ikz)/apar_kz_m1(ikz)) - (apar_kz_m2(ikz)/apar_kz_m1(ikz))) .GT. 0.0 ) ) then
          omega_apar_kz(ikz) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (apar_kz(ikz)/apar_kz_m1(ikz)) - (apar_kz_m2(ikz)/apar_kz_m1(ikz))) + (1.0-alpha_ewa)*omega_apar_kz(ikz)
       else
          omega_apar_kz(ikz) = 0.0 + (1.0 - alpha_ewa)*omega_apar_kz(ikz)
       end if
       if (apar_kz_m1(ikz) .NE. apar_kz(ikz)) then
          gamma_apar_kz(ikz) = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(apar_kz(ikz)-apar_kz_m1(ikz))/(apar_kz(ikz)+apar_kz_m1(ikz)) + (1.0 - alpha_ewa)*gamma_apar_kz(ikz)
       else
          gamma_apar_kz(ikz) = 0.0 + (1.0 - alpha_ewa)*gamma_apar_kz(ikz)
       end if
    end do

    !
    apar2_tot = 0.0
    do ikz = 0,Nz-1
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             apar2_tot = apar2_tot + cabs(apar_in(ikx,iky,ikz))**2
          end do
       end do
    end do

    if ( (apar2_tot_m1 .NE. 0.0) .AND. ( (2.0 - (apar2_tot/apar2_tot_m1) - (apar2_tot_m2/apar2_tot_m1)) .GT. 0.0 ) ) then
       omega_apar2_tot = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*sqrt(2.0 - (apar2_tot/apar2_tot_m1) - (apar2_tot_m2/apar2_tot_m1)) + (1.0-alpha_ewa)*omega_apar2_tot
    else
       omega_apar2_tot = 0.0 + (1.0 - alpha_ewa)*omega_apar2_tot
    end if
    if (apar2_tot_m1 .NE. apar2_tot) then
       gamma_apar2_tot = alpha_ewa*(1.0/(real(nout_spectrum)*delta_t))*(apar2_tot-apar2_tot_m1)/(apar2_tot+apar2_tot_m1) + (1.0 - alpha_ewa)*gamma_apar2_tot
    else
       gamma_apar2_tot = 0.0 + (1.0 - alpha_ewa)*gamma_apar2_tot
    end if

    write(apar_unit,'(1p,1000g15.7)') time,(omega_apar_kz(ik) ,gamma_apar_kz(ik) ,apar_kz(ik),ik = Nz/2,0,-1),(omega_apar_kz(ik), gamma_apar_kz(ik) ,apar_kz(ik),ik = Nz/2+1,Nz-1)      

    close(apar_unit)
    open(unit=apar_unit, file=apar_str, status='old', access='append')
 
    apar2_tot_m2 = apar2_tot_m1
    apar_kz_m2 = apar_kz_m1
    apar_ky_m2 = apar_ky_m1
    apar_kx_m2 = apar_kx_m1

    apar2_tot_m1 = apar2_tot
    apar_kz_m1 = apar_kz
    apar_ky_m1 = apar_ky
    apar_kx_m1 = apar_kx

    do iky = 0,Ny-1
       do ikx = 0,Nx/2
          call fftwnd_f77_one (fftw_plan_invz, apar_in(ikx,iky,:), apar_temp(:))
          apar_in(ikx,iky,:) = apar_temp(:)/real(Nz)
       end do
    end do
    !
    deallocate(apar_k, apar2_k, apar_kx, apar_ky, apar_kz,apar_temp, number)
    !
  end subroutine spectrum_apar

 subroutine calculate_local_slope_EM_field(phi,apar, x_gc, y_gc, z_gc, vz_gc, vx_gc, vy_gc,Epar_gc)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    !
    integer :: idx, izm1, izp1
    integer*8, save :: plan_compl2real_2d,plan_real2compl_3d,plan_compl2real_3d
    integer*8, save::plan_compl2compl_1d
    integer*8, save::plan_compl2complInv_1d
    logical, save :: first=.true.
    !    
    complex, dimension(0:,0:,0:) :: phi, apar
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc, vz_gc
    real, dimension(1:,1:), intent (out) :: vx_gc, vy_gc, Epar_gc
    !    
    complex, dimension(:,:,:), allocatable:: J0_phi_k, J0_apar_k
    real, dimension(:,:,:), allocatable:: J0_phi_x, J0_apar_x
    complex, dimension(:,:,:), allocatable :: J0gradxPhi_k,J0gradyPhi_k,J0gradzPhi_k,J0gradxapar_k,J0gradyapar_k,J0gradzapar_k
    !
    real :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, mu_temp, z_temp, B0local, prefac, df_correction
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2,J0idx,imu
    real:: chi_000,chi_100,chi_010,chi_110,chi_001,chi_101,chi_011,chi_111,vpar, chi_z2, chi_z1, chi_y2, chi_y1, chi_x2, chi_x1
    !    
    allocate(J0_phi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    allocate(J0_apar_k(0:Nx/2,0:Ny-1,0:Nz-1))

    allocate(J0_phi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(J0_apar_x(0:Nx-1,0:Ny-1,0:Nz-1))
    !    
    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       call rfftwnd_f77_create_plan (plan_compl2real_3d,3,Msize,fftw_backward,0)
       call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0)
       first=.false.
    end if
    !    
    do j=1,num_species
       idx = 0
       prefac = Zs(j)/temperature(j)
       do imu=1,Nmu  ! for each vperp value...
          mu_temp = mu_grid(imu)
          do iky=0,Ny-1
             do ikx=0,Nx/2
                do iz = 0,Nz-1
                   J0_phi_k(ikx,iky,iz) = J0(ikx,iky,iz,imu,j)*phi(ikx,iky,iz)/(B0_grid(iz)*real(Nx*Ny))
                   J0_apar_k(ikx,iky,iz) = J0(ikx,iky,iz,imu,j)*apar(ikx,iky,iz)/(B0_grid(iz) *real(Nx*Ny))
                end do
             end do
          end do

          do iz = 0,Nz-1
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0_phi_k(:,:,iz), J0_phi_x(:,:,iz))
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0_apar_k(:,:,iz), J0_apar_x(:,:,iz))
          end do


          NE = NE_mu(imu)
          do k=1,NE            
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,j)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,j)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,j)/delta_z),Nz)  ;  N1 = N0 + 1

             !             
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,j)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,j)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,j)
             !             
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny)  ;  N2 = mod(N1, Nz)
             !             
             vpar = vts(j)*vz_gc(idx,j)

             chi_000 = J0_phi_x(L0,M0,N0) - vpar*J0_apar_x(L0,M0,N0)
             chi_100 = J0_phi_x(L2,M0,N0) - vpar*J0_apar_x(L2,M0,N0)
             chi_010 = J0_phi_x(L0,M2,N0) - vpar*J0_apar_x(L0,M2,N0)
             chi_110 = J0_phi_x(L2,M2,N0) - vpar*J0_apar_x(L2,M2,N0)
             chi_001 = J0_phi_x(L0,M0,N2) - vpar*J0_apar_x(L0,M0,N2)
             chi_101 = J0_phi_x(L2,M0,N2) - vpar*J0_apar_x(L2,M0,N2)
             chi_011 = J0_phi_x(L0,M2,N2) - vpar*J0_apar_x(L0,M2,N2)
             chi_111 = J0_phi_x(L2,M2,N2) - vpar*J0_apar_x(L2,M2,N2)
             !             
             chi_z2 =          (xgc - xl0)*(ygc - ym0)*Volume_scale*chi_111*B0_grid(N2)
             chi_z2 = chi_z2 + (xl1 - xgc)*(ygc - ym0)*Volume_scale*chi_011*B0_grid(N2)
             chi_z2 = chi_z2 + (xgc - xl0)*(ym1 - ygc)*Volume_scale*chi_101*B0_grid(N2)
             chi_z2 = chi_z2 + (xl1 - xgc)*(ym1 - ygc)*Volume_scale*chi_001*B0_grid(N2)

             chi_z1 =          (xgc - xl0)*(ygc - ym0)*Volume_scale*chi_110*B0_grid(N0)
             chi_z1 = chi_z1 + (xl1 - xgc)*(ygc - ym0)*Volume_scale*chi_010*B0_grid(N0)
             chi_z1 = chi_z1 + (xgc - xl0)*(ym1 - ygc)*Volume_scale*chi_100*B0_grid(N0)
             chi_z1 = chi_z1 + (xl1 - xgc)*(ym1 - ygc)*Volume_scale*chi_000*B0_grid(N0)

             chi_y2 =          (xgc - xl0)*(zgc - zn0)*Volume_scale*chi_111
             chi_y2 = chi_y2 + (xl1 - xgc)*(zgc - zn0)*Volume_scale*chi_011
             chi_y2 = chi_y2 + (xgc - xl0)*(zn1 - zgc)*Volume_scale*chi_110
             chi_y2 = chi_y2 + (xl1 - xgc)*(zn1 - zgc)*Volume_scale*chi_010

             chi_y1 =          (xgc - xl0)*(zgc - zn0)*Volume_scale*chi_101
             chi_y1 = chi_y1 + (xl1 - xgc)*(zgc - zn0)*Volume_scale*chi_001
             chi_y1 = chi_y1 + (xgc - xl0)*(zn1 - zgc)*Volume_scale*chi_100
             chi_y1 = chi_y1 + (xl1 - xgc)*(zn1 - zgc)*Volume_scale*chi_000

             chi_x2 =          (ygc - ym0)*(zgc - zn0)*Volume_scale*chi_111
             chi_x2 = chi_x2 + (ym1 - ygc)*(zgc - zn0)*Volume_scale*chi_101
             chi_x2 = chi_x2 + (ygc - ym0)*(zn1 - zgc)*Volume_scale*chi_110
             chi_x2 = chi_x2 + (ym1 - ygc)*(zn1 - zgc)*Volume_scale*chi_100

             chi_x1 =          (ygc - ym0)*(zgc - zn0)*Volume_scale*chi_011
             chi_x1 = chi_x1 + (ym1 - ygc)*(zgc - zn0)*Volume_scale*chi_001
             chi_x1 = chi_x1 + (ygc - ym0)*(zn1 - zgc)*Volume_scale*chi_010
             chi_x1 = chi_x1 + (ym1 - ygc)*(zn1 - zgc)*Volume_scale*chi_000

             vx_gc(idx,j) = -(chi_y2 - chi_y1)
             vy_gc(idx,j) = (chi_x2 - chi_x1)
             Epar_gc(idx,j) = -(chi_z2 - chi_z1)

             if (use_actual_df) then
                write(*,*) "ERROR: use_actual_df option not working with electromagnetic"
                stop
!                df_correction = efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!                df_correction = df_correction - vz_gc(idx,j)*efield(phiGyav_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
!
!                p(wgt_df,idx,j) = p(wgt,idx,j) + prefac*df_correction
             end if
          end do
       end do
    end do
    !
    deallocate( J0_phi_k,J0_apar_k,J0_phi_x, J0_apar_x)
!    if (use_actual_df) deallocate(phiGyav_x,aparGyav_x)
    !
  end subroutine calculate_local_slope_EM_field

  subroutine calculate_local_slope_electric_field(phi, x_gc, y_gc, z_gc, vx_gc, vy_gc, Epar_gc)
    use fft_work, only: fftw_forward, fftw_backward
    implicit none
    !
    ! Compute gyroaveraged electric field with the full J0 function in k-space.
    ! Convert back to real space and find gyroaveraged velocity. See Sec. 2.3.2 in Broemstrup thesis.
    !
    integer :: idx, izm1, izp1
    integer*8, save :: plan_compl2real_2d,plan_real2compl_3d,plan_compl2real_3d
    integer*8, save::plan_compl2compl_1d
    integer*8, save::plan_compl2complInv_1d
    logical, save :: first=.true.
    !    
    complex, dimension(0:,0:,0:) :: phi
    real, dimension(1:,1:), intent (in) :: x_gc, y_gc, z_gc
    real, dimension(1:,1:), intent (out) :: vx_gc, vy_gc, Epar_gc
    !    
    complex, dimension(:,:,:), allocatable :: J0_Ex_k, J0_Ey_k, J0_Ez_k
    real   , dimension(:,:,:), allocatable :: Ex_x, Ey_x, Ez_x
    real   , dimension(:,:,:), allocatable :: df_corr_x
    complex, dimension(:,:,:), allocatable :: df_corr_k
    complex, dimension(:), allocatable:: df_corr_z
    real, dimension(:,:,:), allocatable:: J0_phi_x
    complex, dimension(:,:,:), allocatable:: J0_phi_k
    complex:: J0_phi
    real:: phi_000,phi_100,phi_010,phi_110,phi_001,phi_101,phi_011,phi_111, phi_x2, phi_x1, phi_y2, phi_y1, phi_z2, phi_z1
    !
    real :: xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, mu_temp, z_temp, B0local, prefac, df_correction
    integer :: L0, L1, L2, M0, M1, M2, N0, N1, N2,J0idx,imu
    !    
    allocate(J0_phi_x(0:Nx-1,0:Ny-1,0:Nz-1))
    allocate(J0_phi_k(0:Nx/2,0:Ny-1,0:Nz-1))
    !
    if (use_actual_df) then
       allocate(df_corr_z(0:Nz-1))
       allocate(df_corr_x(0:Nx-1,0:Ny-1,0:Nz-1))
       allocate(df_corr_k(0:Nx/2,0:Ny-1,0:Nz-1))
    end if
    !
    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       call rfftwnd_f77_create_plan (plan_compl2real_3d,3,Msize,fftw_backward,0)
       call rfftwnd_f77_create_plan (plan_compl2real_2d,2,Msize(1:2),fftw_backward,0)
       first=.false.
    end if

    do j=1,num_species
       idx = 0
       prefac = Zs(j)/temperature(j)
       do imu=1,Nmu  ! for each vperp value...
          mu_temp = mu_grid(imu)
          do iky=0,Ny-1
             do ikx=0,Nx/2
                do iz = 0,Nz-1

                   J0_phi_k(ikx,iky,iz) = J0(ikx,iky,iz,imu,j)*phi(ikx,iky,iz) /(B0_grid(iz)*real(Nx*Ny))

                   if (use_actual_df) df_corr_z(iz) = (J0_phi_k(ikx,iky,iz)- phi(ikx,iky,iz))/(B0_grid(iz)*real(Nx*Ny))

 
                end do

                if (use_actual_df) call fftwnd_f77_one (plan_compl2compl_1d, df_corr_z(:), df_corr_k(ikx,iky,:))

             end do
          end do
  
          do iz = 0,Nz-1
             call rfftwnd_f77_one_complex_to_real (plan_compl2real_2d, J0_phi_k(:,:,iz), J0_phi_x(:,:,iz))
          end do

          if (use_actual_df) call rfftwnd_f77_one_complex_to_real (plan_compl2real_3d, df_corr_k(:,:,:), df_corr_x(:,:,:))


          NE = NE_mu(imu)
          do k=1,NE            
             idx = idx + 1
             !
             L0 = mod(int(x_gc(idx,j)/delta_x),Nx)  ;  L1 = L0 + 1
             M0 = mod(int(y_gc(idx,j)/delta_y),Ny)  ;  M1 = M0 + 1
             N0 = mod(int(z_gc(idx,j)/delta_z),Nz)  ;  N1 = N0 + 1
             !             
             xl0 = x_grid(L0)  ;  xl1 = x_grid(L1)  ;  xgc = x_gc(idx,j)
             ym0 = y_grid(M0)  ;  ym1 = y_grid(M1)  ;  ygc = y_gc(idx,j)
             zn0 = z_grid(N0)  ;  zn1 = z_grid(N1)  ;  zgc = z_gc(idx,j)
             !             
             L2 = mod(L1, Nx)  ;  M2 = mod(M1, Ny)  ;  N2 = mod(N1, Nz)

             phi_000 = J0_phi_x(L0,M0,N0)
             phi_100 = J0_phi_x(L2,M0,N0)
             phi_010 = J0_phi_x(L0,M2,N0)
             phi_110 = J0_phi_x(L2,M2,N0)
             phi_001 = J0_phi_x(L0,M0,N2)
             phi_101 = J0_phi_x(L2,M0,N2)
             phi_011 = J0_phi_x(L0,M2,N2)
             phi_111 = J0_phi_x(L2,M2,N2)
             !             
             phi_z2 =          (xgc - xl0)*(ygc - ym0)*Volume_scale*phi_111*B0_grid(N2)
             phi_z2 = phi_z2 + (xl1 - xgc)*(ygc - ym0)*Volume_scale*phi_011*B0_grid(N2)
             phi_z2 = phi_z2 + (xgc - xl0)*(ym1 - ygc)*Volume_scale*phi_101*B0_grid(N2)
             phi_z2 = phi_z2 + (xl1 - xgc)*(ym1 - ygc)*Volume_scale*phi_001*B0_grid(N2)

             phi_z1 =          (xgc - xl0)*(ygc - ym0)*Volume_scale*phi_110*B0_grid(N0)
             phi_z1 = phi_z1 + (xl1 - xgc)*(ygc - ym0)*Volume_scale*phi_010*B0_grid(N0)
             phi_z1 = phi_z1 + (xgc - xl0)*(ym1 - ygc)*Volume_scale*phi_100*B0_grid(N0)
             phi_z1 = phi_z1 + (xl1 - xgc)*(ym1 - ygc)*Volume_scale*phi_000*B0_grid(N0)

             phi_y2 =          (xgc - xl0)*(zgc - zn0)*Volume_scale*phi_111
             phi_y2 = phi_y2 + (xl1 - xgc)*(zgc - zn0)*Volume_scale*phi_011
             phi_y2 = phi_y2 + (xgc - xl0)*(zn1 - zgc)*Volume_scale*phi_110
             phi_y2 = phi_y2 + (xl1 - xgc)*(zn1 - zgc)*Volume_scale*phi_010

             phi_y1 =          (xgc - xl0)*(zgc - zn0)*Volume_scale*phi_101
             phi_y1 = phi_y1 + (xl1 - xgc)*(zgc - zn0)*Volume_scale*phi_001
             phi_y1 = phi_y1 + (xgc - xl0)*(zn1 - zgc)*Volume_scale*phi_100
             phi_y1 = phi_y1 + (xl1 - xgc)*(zn1 - zgc)*Volume_scale*phi_000

             phi_x2 =          (ygc - ym0)*(zgc - zn0)*Volume_scale*phi_111
             phi_x2 = phi_x2 + (ym1 - ygc)*(zgc - zn0)*Volume_scale*phi_101
             phi_x2 = phi_x2 + (ygc - ym0)*(zn1 - zgc)*Volume_scale*phi_110
             phi_x2 = phi_x2 + (ym1 - ygc)*(zn1 - zgc)*Volume_scale*phi_100

             phi_x1 =          (ygc - ym0)*(zgc - zn0)*Volume_scale*phi_011
             phi_x1 = phi_x1 + (ym1 - ygc)*(zgc - zn0)*Volume_scale*phi_001
             phi_x1 = phi_x1 + (ygc - ym0)*(zn1 - zgc)*Volume_scale*phi_010
             phi_x1 = phi_x1 + (ym1 - ygc)*(zn1 - zgc)*Volume_scale*phi_000

             vx_gc(idx,j) = - (phi_y2 - phi_y1)
             vy_gc(idx,j) = (phi_x2 - phi_x1)
             Epar_gc(idx,j) = - (phi_z2 - phi_z1)

             if (use_actual_df) then
                df_correction = efield(df_corr_x(:,:,:), xl0, xl1, xgc, ym0, ym1, ygc, zn0, zn1, zgc, L0, L2, M0, M2, N0, N2, Volume_scale)
                p(wgt_df,idx,j) = p(wgt,idx,j) + prefac*df_correction
             end if
          end do
       end do
    end do
    !
    deallocate(J0_phi_x, J0_phi_k)
    if (use_actual_df) deallocate( df_corr_k, df_corr_x, df_corr_z)
    !
  end subroutine calculate_local_slope_electric_field
 
  subroutine phi2_diagnostics(phi_in)
  implicit none
    complex,dimension(0:,0:,0:),intent(in)::phi_in
    real:: phi2
    real,save:: phi2_prev
    complex,dimension(:),allocatable,save:: omega_phi2
    complex:: omega_phi2_avg
    logical, save:: first = .true.

    if (first) then
       allocate(omega_phi2(1:navg_spectrum))
       omega_phi2 = 1.0
       phi2_prev = 0.0
       first = .false.
    end if

    ! Total |phi|^2
    phi2 = 0.0
    do iz = 0,Nz-1
       do iky = 0,Ny-1
          do ikx = 0,Nx/2
             phi2 = phi2 + cabs(phi_in(ikx,iky,iz) )**2
          end do
       end do
    end do
    if ( phi2_prev .GT. epsilon(0.0)) then
       omega_phi2(1) = (zi/(real(nout_spectrum)*delta_t))*log(phi2/phi2_prev)
    else
       omega_phi2(1) = 0.0
    end if

    ! Calculate moving average
    omega_phi2_avg = sum(omega_phi2)/real(navg_spectrum) 

    ! Output results  

    write(phi2_unit, '(1p,1000g15.7)') time,real(omega_phi2_avg),aimag(omega_phi2_avg),phi2
    close(phi2_unit)
    open(unit=phi2_unit, file=phi2_str, status='old', access='append')
             
    ! Shift array elements to prepare for next timestep
    phi2_prev = phi2
    do i=navg_spectrum,2,-1
       omega_phi2(i) = omega_phi2(i-1) 
    end do

  end subroutine phi2_diagnostics
  
  subroutine phikx_diagnostics(phi_in)
  use fft_work, only: fftw_forward, fftw_backward
  implicit none
    complex,dimension(0:,0:,0:),intent(in)::phi_in
    complex,dimension(:),allocatable:: phi_kx
    real,dimension(:),allocatable:: phi2
    real,dimension(:),allocatable,save:: phi2_prev
    complex,dimension(:,:),allocatable,save:: omega_phikx
    complex,dimension(:),allocatable:: omega_phikx_avg
    logical, save:: first = .true.
    integer*8,save:: plan_compl2compl_1d

    allocate(phi2(0:Nx-1))
    allocate(phi_kx(0:Nx-1))
    allocate(omega_phikx_avg(0:Nx-1))


    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       allocate(omega_phikx(0:Nx-1,1:navg_spectrum))
       allocate(phi2_prev(0:Nx-1))
       omega_phikx = 1.0
       phi2_prev = 0.0
       first = .false.
    end if

    phi2 = 0.0
    do iky = 0,Ny/2
       do ikz = 0,Nz-1
          call fftwnd_f77_one (plan_compl2compl_1d, phi_in(:,iky,ikz), phi_kx(:))
          do ikx = 0,Nx-1
             phi2(ikx) = phi2(ikx) + cabs(phi_kx(ikx) )**2/real(Nx**2)
          end do
       end do
    end do

    do ikx = 0,Nx-1
       if ( phi2_prev(ikx) .GT. epsilon(0.0)) then
          omega_phikx(ikx,1) = (zi/(real(nout_spectrum)*delta_t))*log(phi2(ikx)/phi2_prev(ikx))
       else
          omega_phikx(ikx,1) = 0.0
        end if
        ! Calculate moving average
        omega_phikx_avg(ikx) = sum(omega_phikx(ikx,:))/real(navg_spectrum) 
    end do


    ! Output results  
    write(phikx_unit, '(1p,1000g15.7)') time,(real(omega_phikx_avg(ikx)),aimag(omega_phikx_avg(ikx)),phi2(ikx),ikx=0,Nx-1)
    close(phikx_unit)
    open(unit=phikx_unit, file=phikx_str, status='old', access='append')
             
    ! Shift array elements to prepare for next timestep
    phi2_prev = phi2
    do i=navg_spectrum,2,-1
       omega_phikx(:,i) = omega_phikx(:,i-1) 
    end do

    deallocate(phi2,omega_phikx_avg, phi_kx)

  end subroutine phikx_diagnostics
 
 
  subroutine phikz_diagnostics(phi_in)
  use fft_work, only: fftw_forward, fftw_backward
  implicit none
    complex,dimension(0:,0:,0:),intent(in)::phi_in
    complex,dimension(:),allocatable:: phi_kz
    real,dimension(:),allocatable:: phi2
    real,dimension(:),allocatable,save:: phi2_prev
    complex,dimension(:,:),allocatable,save:: omega_phikz
    complex,dimension(:),allocatable:: omega_phikz_avg
    logical, save:: first = .true.
    integer*8,save:: plan_compl2compl_1d

    allocate(phi2(0:Nz-1))
    allocate(phi_kz(0:Nz-1))
    allocate(omega_phikz_avg(0:Nz-1))


    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       allocate(omega_phikz(0:Nz-1,1:navg_spectrum))
       allocate(phi2_prev(0:Nz-1))
       omega_phikz = 1.0
       phi2_prev = 0.0
       first = .false.
    end if

    phi2 = 0.0
    do ikx = 0,Nx/2
       do iky = 0,Ny-1
          call fftwnd_f77_one (plan_compl2compl_1d, phi_in(ikx,iky,:), phi_kz(:))
          do ikz = 0,Nz-1
             phi2(ikz) = phi2(ikz) + cabs(phi_kz(ikz) )**2/real(Nz**2)
          end do
       end do
    end do

    do ikz = 0,Nz-1
       if ( phi2_prev(ikz) .GT. epsilon(0.0)) then
          omega_phikz(ikz,1) = (zi/(real(nout_spectrum)*delta_t))*log(phi2(ikz)/phi2_prev(ikz))
       else
          omega_phikz(ikz,1) = 0.0
        end if
        ! Calculate moving average
        omega_phikz_avg(ikz) = sum(omega_phikz(ikz,:))/real(navg_spectrum) 
    end do


    ! Output results  
    write(phikz_unit, '(1p,1000g15.7)') time,(real(omega_phikz_avg(ikz)),aimag(omega_phikz_avg(ikz)),phi2(ikz),ikz=0,Nz-1)
    close(phikz_unit)
    open(unit=phikz_unit, file=phikz_str, status='old', access='append')
             
    ! Shift array elements to prepare for next timestep
    phi2_prev = phi2
    do i=navg_spectrum,2,-1
       omega_phikz(:,i) = omega_phikz(:,i-1) 
    end do

    deallocate(phi2,omega_phikz_avg, phi_kz)

  end subroutine phikz_diagnostics
 
  subroutine aparkz_diagnostics(apar_in)
  use fft_work, only: fftw_forward, fftw_backward
  implicit none
    complex,dimension(0:,0:,0:),intent(in)::apar_in
    complex,dimension(:),allocatable:: apar_kz
    real,dimension(:),allocatable:: apar2
    real,dimension(:),allocatable,save:: apar2_prev
    complex,dimension(:,:),allocatable,save:: omega_aparkz
    complex,dimension(:),allocatable:: omega_aparkz_avg
    logical, save:: first = .true.
    integer*8,save:: plan_compl2compl_1d

    allocate(apar2(0:Nz-1))
    allocate(apar_kz(0:Nz-1))
    allocate(omega_aparkz_avg(0:Nz-1))

    if (first) then
       call fftwnd_f77_create_plan (plan_compl2compl_1d,1,Msize(3),fftw_forward,0) 
       allocate(omega_aparkz(0:Nz-1,1:navg_spectrum))
       allocate(apar2_prev(0:Nz-1))
       omega_aparkz = 1.0
       apar2_prev = 0.0
       first = .false.
    end if

    apar2 = 0.0
    do ikx = 0,Nx/2
       do iky = 0,Ny-1
          call fftwnd_f77_one (plan_compl2compl_1d, apar_in(ikx,iky,:), apar_kz(:))
          do ikz = 0,Nz-1
             apar2(ikz) = apar2(ikz) + cabs(apar_kz(ikz) )**2/real(Nz**2)
          end do
       end do
    end do

    do ikz = 0,Nz-1
       if ( apar2_prev(ikz) .GT. epsilon(0.0)) then
          omega_aparkz(ikz,1) = (zi/(real(nout_spectrum)*delta_t))*log(apar2(ikz)/apar2_prev(ikz))
       else
          omega_aparkz(ikz,1) = 0.0
        end if
        ! Calculate moving average
        omega_aparkz_avg(ikz) = sum(omega_aparkz(ikz,:))/real(navg_spectrum) 
    end do


    ! Output results  
    write(aparkz_unit, '(1p,1000g15.7)') time,(real(omega_aparkz_avg(ikz)),aimag(omega_aparkz_avg(ikz)),apar2(ikz),ikz=0,Nz-1)
    close(aparkz_unit)
    open(unit=aparkz_unit, file=aparkz_str, status='old', access='append')
             
    ! Shift array elements to prepare for next timestep
    apar2_prev = apar2
    do i=navg_spectrum,2,-1
       omega_aparkz(:,i) = omega_aparkz(:,i-1) 
    end do

    deallocate(apar2,omega_aparkz_avg,apar_kz)

  end subroutine aparkz_diagnostics

  subroutine manage_diagnostics
  use mp, only: iproc,nproc,proc0
  implicit none
    integer:: jproc = 0
    
    if (phi2_diagnostics_on .AND. (mod(step-1,nout_spectrum).EQ. 0)) then 
       if (jproc .EQ. iproc) call phi2_diagnostics(phi) 
       jproc = mod(jproc+1,nproc)
    end if

    if (phikz_diagnostics_on  .AND. (mod(step-1,nout_spectrum).EQ. 0)) then 
       if (jproc .EQ. iproc) call phikz_diagnostics(phi) 
       jproc = mod(jproc+1,nproc)
    end if

    if (phikx_diagnostics_on  .AND. (mod(step-1,nout_spectrum).EQ. 0)) then 
       if (jproc .EQ. iproc) call phikx_diagnostics(phi) 
       jproc = mod(jproc+1,nproc)
    end if
    
    if (aparkz_diagnostics_on  .AND. (mod(step-1,nout_spectrum).EQ. 0) .AND. apar_on ) then 
       if (jproc .EQ. iproc) call aparkz_diagnostics(apar) 
       jproc = mod(jproc+1,nproc)
    end if

    if (particle_diagnostics_on .AND. (step .EQ. part_write_step) ) then 
       if (jproc .EQ. iproc) call particle_diagnostics()
       jproc = mod(jproc+1,nproc)
    end if

     if (z_trajectory_diagnostics_on ) then 
        if (jproc .EQ. iproc) call z_trajectory_diagnostics()
        jproc = mod(jproc+1,nproc)
     end if

     if (nz_diagnostics_on ) then 
        if (jproc .EQ. iproc) call nz_diagnostics(p(z,:,:))
        jproc = mod(jproc+1,nproc)
     end if

     if (wgt_diagnostics_on .AND. ( mod(step-1,nout_wgt).EQ. 0) ) then
        call wgt_diagnostics()
        jproc = mod(jproc+1,nproc)
     end if

     if (phiplot_on .AND. (mod(step-1,nout_phiplot) .EQ. 0) ) then
        if (jproc .EQ. iproc) then 
           if (phiplot_z_on) then 
              call phiplot_z(phi)
           else
              call phiplot(phi)
           end if
        end if
        jproc = mod(jproc+1,nproc)
     end if


  end subroutine manage_diagnostics

  subroutine broadcast_diagnostic_files()
  use mp, only: broadcast
  implicit none
    call broadcast(phi2_unit)
    call broadcast(phikz_unit)
    call broadcast(phikx_unit)
    call broadcast(aparkz_unit)
    call broadcast(phiplot_unit)
    call broadcast(wgt_unit)
    call broadcast(nz_diagnostics_unit)
    call broadcast(particle_diagnostics_unit)
    call broadcast(z_trajectory_diagnostics_unit)

    call broadcast(phi2_str)
    call broadcast(phikz_str)
    call broadcast(phikx_str)
    call broadcast(aparkz_str)
    call broadcast(phiplot_str)
    call broadcast(wgt_str)
    call broadcast(nz_diagnostics_str)
    call broadcast(particle_diagnostics_str)
    call broadcast(z_trajectory_diagnostics_str)
  end subroutine broadcast_diagnostic_files


  end program PIC_GK
