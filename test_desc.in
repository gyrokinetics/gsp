
!**************************************************************

&particle_par
  
! Number of particles of each species per 3D grid cell
  Ncell =4000				

! Maximum particle energy
  Emax =4.5		

! Temperature of the markers
! Negative value interpreted as infinity (particles uniformly distributed in velocity space - as Broemstrup had it)
! If 1.0, markers are Gaussian-distributed according to the temperature of their species
! If any other value, the markers are Gaussian-distributed with an effective temperature equal to T_marker*T_s
! Intended to adequately cover high energies if desired.
  T_marker = -1.0			

! A strange option that generally should not be activated. Turns off particle streaming in z entirely.
  no_streaming = F
/

!**************************************************************

&field_par

! If true, fields are calculated by the local, piecewise constant slope in phi
! If false, fields are calculated by interpolating E as calculated on the grid
  local_slope = F

! Turns on finite-beta calculations
  apar_on = F
  beta = 1.e-2

! If true, Gamma_0 is estimated based on the particle positions and becomes a function of z
  numerical_denom = F

! If true, velocity-space averaged distribution function (for the purpose of Monte Carlo
! integration) is calculated using the (fractional) local number of particles, the way
! Broemstrup did it
! If false, only the average number of particles per cell (Ncell) is used, like the rest 
! of the community does it
  gnrm_on = T

! Use shape-function corresponding to local two-point finite differences for derivatives
! If false, derivatives are calculated spectrally in Fourier space
  shape_fcn_on = F

! Method by which the zero kperp mode is zeroed out
! If 0, simply set phi(0,0,:) = 0.0
! If 1, subtract phi(0,0,:) from all modes
! Should be equivalent?
  k0_method = 0

! If true, ensures that RHS of Poisson's equation integrates to zero, else Poisson's equation is ill-posed
  make_poisson_well_posed = F

! If true, zero out modes with kx, ky, or kz greater than (2/3) the maximum value
  use_mask = F

/

!**************************************************************

&time_par

! The input timestep. Subject to change depending on further options
  delta_t_in = 1.e-4

! Computational time at which to stop the simulation
  time_stop = 1.0
! Maximum number of timesteps
  step_max = -2
! Timestep logic works as follows. Negative values count as "unspecified"
!   If neither step_max or time_stop are specified, abort code
!   If only time_stop is specified, step_max is determined from time_stop/delta_t_in
!   If only step_max is specified, time_stop is determined from step_max*delta_t_in
!   Code will run until one of the above conditions are met
 
! Safety factor for testing CFL condition. If >1, CFL may be violated
  CFL_fac = 0.8
! Number of successive timesteps with sufficient CFL cushion (at least dtrelax_fac*CFL_fac)
! before timestep can be increased again (but never above delta_t_in)
  nrelax_req = 10
  dtrelax_fac = 0.75
/

!**************************************************************

&grid_par
! Resolution in mu
! If negative, code calculates mu-averaged distribution
  Nmu = 18				

! Flag for Guassian quadrature in mu. Only works for Nmu=10,18,32,64 for now
  gaussquad_on = T			
 
! Grid resolution
  Nx=1				
  Ny=16
  Nz = 8
 
! Grid size
  Lx=6.28
  Ly=125.66	
  Lz=6.28
/

!**************************************************************

&B0_par

! How the background magnetic field is represented
! If 0, B0 is uniform and equal to B0mag
! If -1, read from a table (must specify B0file as the filename)
! If 1, B0(z) = B0mag / (1 + eps*cos(pi*(2*pi*z/Lz - 1)))
! If 2, B0(z) = B0mag + eps*cos(2*pi*z/Lz)
! NONE OF THESE OPTIONS EXCEPT UNIFORM HAVE BEEN TESTED. 
! Need to fix 3D instability first
  B0mode = 0
  B0mag = 1.0
  eps= 0.2
  B0file = "B0test.dat"
/

!**************************************************************

&dia_par

! Turn on a test of the phi integral as one of several modes 
! and specify analytic function to integrate
  phi_diagnostics_on = F
  phi_diag_mode =1
  phi_diag_func = 3
  Nrun_phi_diag = 40

! Output Sum of |phik|^2 to a file
  phi2_diagnostics_on = T

! Output |phi_k|^2 at each z point to a file
  phikz_diagnostics_on = F

! Output |phi_k|^2 at each z point to a file
  phikx_diagnostics_on = F
! Output |Apar_k|^2 at each z point to a file
  aparkz_diagnostics_on = T

! How often to output spectrum data
  nout_spectrum = 1
! How many timesteps to average over to determine moving average 
! of frequency and growth rate
  navg_spectrum = 10

! Turns on diagnostics that count how many particles are near a 
!z grid point as a function of time. Largely obsolete.
  nz_diagnostics_on = F

! Turns on a diagnostic that outputs z(t) for every particle
  z_trajectory_diagnostics_on = F
! Which species to output
  ispec_traj =1
! Approximately how many particles to track for this option
  Nparticle_traj = 50

! Turns on diagnostic that outputs all saved particle information 
! to a file at a specified timestep
  particle_diagnostics_on = F
  part_write_step = 1

! Turns on diagnostic that outputs quantities that are a simple sum
! over all particles. Things like sum of wgt^2, simple particle, heat flux, etc.
  wgt_diagnostics_on = F
  nout_wgt = 1
! Generally deactivated option that ensures that df is summed over instead of g = <df>
! for these diagnostics. Not to be trusted until further revision.
  use_actual_df = F

! An attempt to conserve particle properties by introducing an artificial particle 
! whose weight, momentum, and energy are chosen to as to conserve macro properties
! Not to be trusted 
  superparticle_conserve = F
! Flag that, if true, attempt to continue the simulation even if the conserving "superparticle"
! wanders beyond Emax
  superparticle_fail_cont = F

! Diagnostic that outputs phi as a function of spatial variables
  phiplot_on = F
  nout_phiplot = 1000
! If true, output phi(kz) instead of phi(z)
  phiplot_z_on = F
! If true, plot phi(x,y,z) instead of phi(kx,ky,kz)
  phiplot_real = F

! Diagnostics that compares Gamma_0 to local estimate thereof
  numerical_denom_diagnostics_on = F

/

!**************************************************************

&init_par

! Magnitude (or spread) of the initial particle weights
  init_wgt_mag = 0.1

! If true, only one mode (the inialized mode) is calculated, all others are zeroed out
! for the field. For example, if init_ky = 2, then 2 is the *index* of the ky mode kept, along 
! with -ky. It is uncertain how well this works if multiple dimensions (x,y,z) are specified.
  one_k_mode = F
! Leave unspecified if not in use:
!  init_kx = 0
!  init_ky = 1
!  init_kz = 0
!  exclude_kx = 1
!  exclude_ky = 1
!  exclude_kz = 1

! Initial condition in space
! If 0, initial condition is noise not zero
! If 1, initial condition is a single mode in space (determined by init_kx etc. above)
  init_wgt_mode_r = 0

! Initial condition in velocity space.
! If 0, noise
! If 1, delta-like function. Must specify: init_xi0, init_v0,init_v_width, to determine location and width of the spike
  init_wgt_mode_v = 0

! Write a save file for particle data?
  save_for_restart = F
! How often to write a savefile
  nsave = 10

! Use as initial conditions a previous save file
  init_restart = F
/

!**************************************************************

! Species parameters.
! Largely self-explanatory
&spec_par
  num_species = 2
  Rinv=1.0
  temperature_1=1.0
  temperature_2=1.0
  mass_1 = 1.0
  mass_2 = 5.4e-4
  density_1 = 1.0
  density_2 = 1.0
  charge_1 = 1.0
  charge_2 = -1.0
  Ln_1=1.e28
  Ln_2=1.e28
  LT_1=1.e28
  LT_2=1.e28

! Turns on ExB drift
  nonlinear_on = F

! Turns on curvature drift according to the curvature Rinv defined above
  curvature_on =F

! If true, keep only leading-order finite-Larmor-radius corrections. J0 = 1, Gamma_0s = 1 - kperp^2 rho_s^2
  drift_kinetic = F
/

!**************************************************************
 
&collision_par

! If true, use a collision operator
  collision_on = F

! If true, turn on pitch-angle scattering term of collision operator
  pitch_angle_coll_on = T
! If true, turn on energy scattering term of collision operator
  energy_diff_coll_on = T

! If true, apply collision operator for each sub-timestep of 2nd order Runge-Kutta. 
! Otherwise, it is applied only once per timestep
  halfstep_coll_on = F

! Resolution along v for collision operator grid
  Nv_coll = 8
! Resolution along xi=vz/v for collision operator grid
  Nxi_coll= 8

! Coarse-graining parameter as defined by Broemstrup. If 1.0, collision operator is only coarse graining. 
  gamma = 1.0

! How often to apply collision operator. If only coarse-graining, setting collision_time to 1/gamma should
! be roughly equivalent
  collision_time = 1

! Collision frequency parameter by species
! If negative, species parameters above are used to scale collision frequency appropriately compared to species 1
  nu_coll_1 = 0.001
  nu_coll_2 = -1.0

! If true, use particle-by-particle krook operator
  krook_operator_on = F
  nu_krook = 0.01

! The following options may have decayed significantly and probably do not work

! If true, use Monte Carlo integration to calculate integrals that enforce conservation properties of collision operator
  conserve_coll_mc_on = F

! If true, use Sherman-Morrison formulism to calculate conservation integrals by finite-difference. Currently unstable.
  conserve_coll_sm_on = F

! Turn on electron-ion collision term
  coll_ei_on = F

! If true, find h from g via phi calculated on the collision operator grid (integrate velocity space on the grid)
  h_convert_im = F

! If true, find h from g via on a particle-by-particle basis (based on local phi), then interpolate onto the grid
  h_convert_mc = F

! Turns on gyrodiffusion term
  gyrodiff_on = F

! If true, output to a file the time-evolution of obstensibly conserved properties 
  conserve_diagnostics_on = F
/

! Block activated when conserve_diagnostics_on = T
&vel_dia_par
  v_dia_x = 0
  v_dia_y = 0
  v_dia_z = 0
  is_dia = 1
  vspace_coll_diagnostics_time = 1
  vspace_coll_diagnostics_on = T
/

! Old diagnostics from Broemstrup and Gustafson whose infrastructure may 
! or may not behave anymore, but I want to keep in case I want to use them
&dia_old_par
  energy_diagnostics_on = F
  diagnostics_new_on = F
  diagnostics_1_on = F
  diagnostics_2_on = F
  diagnostics_3_on = F
  diagnostics_4_on = F
  nwrite = 1
/

!**************************************************************

! Depreciated diagnostic block
&dia_traj
  diagnostics_traj_on =.FALSE.
  Ndisp = 1000
  traj_dia_step = 50
/
